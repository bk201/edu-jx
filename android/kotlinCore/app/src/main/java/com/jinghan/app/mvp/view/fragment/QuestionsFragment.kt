package com.jinghan.app.mvp.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.RadioGroup
import com.amap.api.location.AMapLocation
import com.amap.api.location.AMapLocationListener
import com.jinghan.app.AppContext
import com.jinghan.app.global.Constant
import com.jinghan.app.mvp.view.adapter.BaseFragmentAdapter
import com.jinghan.app.mvp.view.popup.FilterPopup
import com.jinghan.core.R
import com.jinghan.core.databinding.FrgQuestionsBinding
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import com.jinghan.core.helper.LocationHelper
import com.jinghan.core.mvp.view.fragment.BaseFragment
import java.util.ArrayList
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/7 11:53
 * @mail lzr319@163.com
 */
@ActivityScoped
class QuestionsFragment @Inject constructor() : BaseFragment<FrgQuestionsBinding>() {

    private lateinit var adapter:BaseFragmentAdapter<Fragment>

    private var helper: LocationHelper = LocationHelper.getInstance(AppContext.instance)

    override val layoutId: Int
        get() = R.layout.frg_questions

    override fun initViews() {
        super.initViews()

        binding.rgHq.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                when(checkedId){
                    R.id.rbHq->{
                        binding.viewPager.currentItem = 0
                    }
                    R.id.rbZx->{
                        binding.viewPager.currentItem = 1
                    }

                }
            }
        })


        binding.ivFilter.setOnClickListener{
            var popup:FilterPopup = FilterPopup(context)
            popup.showAsDropDown(binding.ivFilter)
        }

        helper.setListener { p0 -> run { binding.tvAddress.text = p0?.city?.trimEnd('市') } }
        helper.start()
    }

    override fun initData() {
        var world = QuestionListFragment()
        var worldBundle = Bundle()
        worldBundle.putInt(QuestionListFragment.TYPE,QuestionListFragment.WORLD)
        world.arguments = worldBundle


        var city = QuestionListFragment()
        var cityBundle = Bundle()
        cityBundle.putInt(QuestionListFragment.TYPE,QuestionListFragment.CITY)
        world.arguments = cityBundle

        var fragments = ArrayList<Fragment>()
        fragments.add(world)
        fragments.add(city)

        adapter = BaseFragmentAdapter(fragmentManager)
        binding.viewPager.adapter = adapter
        binding.viewPager.setScanScroll(false)
        adapter.setFragments(fragments)
    }

    override fun initPresenter() {
    }
}