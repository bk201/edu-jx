package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.CourseInfo

/**
 * @author liuzeren
 * @time 2018/3/14 13:36
 * @mail lzr319@163.com
 */
class CourseDetailResponse constructor(var value:CourseInfo):BaseResponse()