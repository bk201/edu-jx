package com.jinghan.app.mvp.model.bean

import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/11    下午2:55
 * @mail lzr319@163.com
 */
data class AskInfo(var addTime:String,
var answerAddTime:String,
var answerDescription:String,
var answerPictures:ArrayList<String>,
var answerUploadTime:String,
var answerUserId:Int,
var answerUserName:String,
var canCancel:Boolean,
var canCancelRefund:Boolean,
var canContact:Boolean,
var canEvaluate:Boolean,
var canFinish:Boolean,
var canPay:Boolean,
var canPlatform:Boolean,
var canRefund:Boolean,
var description:String,
var evaluateContent:String,
var evaluateStatus:Int,
var money:Int,
var orderStatus:Int,
var pictures:ArrayList<String>?,
var questionNo:String,
var statusText:String,
var type:String,
var userId:Int){
    companion object {
        /**全部*/
        val ALL = 0

        /**未解答*/
        val IN_REPLAY = 1

        /**已解答*/
        val IS_REPLAY = 2

        /**评价*/
        val EVALUATE = 3

        /**退款*/
        val REFUND =4
    }

    /*addTime (string, optional): 发布问题时间 ,
description (string, optional): 问题的文字描述 ,
money (number): 问题的钱数 ,
pictures (Array[string]): 图片列表 ,
questionNo (string): 问题单号 ,
type (string): 问题的科目*/
}