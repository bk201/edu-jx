package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.TeacherListInfo
import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/3/5    下午9:38
 * @mail lzr319@163.com
 */
interface ITeacherListFragmentView : BaseView{

    fun stopRefresh()

    fun onResult(isRefresh:Boolean,questions: ArrayList<TeacherListInfo>)

}