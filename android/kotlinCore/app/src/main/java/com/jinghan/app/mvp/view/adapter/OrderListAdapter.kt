package com.jinghan.app.mvp.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jinghan.app.mvp.model.bean.EvaluateInfo
import com.jinghan.app.mvp.model.bean.OrderInfo
import com.jinghan.app.mvp.model.bean.OrderShowInfo
import com.jinghan.app.mvp.view.activity.EvaluateActivity
import com.jinghan.app.mvp.view.activity.RefundActivity
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/11    下午4:07
 * @mail lzr319@163.com
 */
class OrderListAdapter : BaseAdapter<OrderListAdapter.OrderListViewHolder, OrderShowInfo>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListViewHolder {
        return OrderListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.frg_order_list_item,parent,false))
    }

    inner class OrderListViewHolder constructor(itemView: View) : BaseViewHolder<OrderShowInfo>(itemView){

        private var tvOrderId: TextView

        private var tvSubjectType:TextView

        private var tvSubjectContent:TextView

        private var tvMoney:TextView

        private var tvSubjectTime:TextView

        /**立即评价*/
        private var tvComment:TextView

        /**完成订单*/
        private var tvFinishOrder:TextView

        /**取消订单*/
        private var tvCancelOrder:TextView

        /**取消退款*/
        private var tvCancelRefund:TextView

        /**平台介入*/
        private var tvPlant:TextView

        /**立即退款*/
        private var tvPay:TextView

        /**申请退款*/
        private var tvTk:TextView

        /**联系老师*/
        private var tvPhone:TextView

        init {
            tvOrderId = itemView.findViewById(R.id.tvOrderId)
            tvSubjectType = itemView.findViewById(R.id.tvSubjectType)
            tvSubjectContent = itemView.findViewById(R.id.tvSubjectContent)
            tvMoney = itemView.findViewById(R.id.tvMoney)
            tvSubjectTime = itemView.findViewById(R.id.tvSubjectTime)
            tvComment = itemView.findViewById(R.id.tvComment)
            tvFinishOrder = itemView.findViewById(R.id.tvFinishOrder)
            tvCancelOrder = itemView.findViewById(R.id.tvCancelOrder)
            tvTk = itemView.findViewById(R.id.tvTk)
            tvPay = itemView.findViewById(R.id.tvPay)
            tvPhone = itemView.findViewById(R.id.tvPhone)
            tvCancelRefund = itemView.findViewById(R.id.tvCancelRefund)
            tvPlant = itemView.findViewById(R.id.tvPlant)
        }


        override fun update(t: OrderShowInfo) {
            tvOrderId.text = itemView.context.getString(R.string.service_order_no,t.orderNo)

            if(null != t.items && t.items.size > 0)
            tvSubjectType.text = t.items[0].courseName
            tvSubjectContent.text = itemView.context.getString(R.string.course_content,t.items[0].introduce)
            tvSubjectTime.text = itemView.context.getString(R.string.course_time,t.items[0].courseTime)
            tvMoney.text = itemView.context.getString(R.string.money,t.items[0].price)

            if(t.canEvaluate) {
                tvComment.visibility = View.VISIBLE
            }else{
                tvComment.visibility = View.GONE
            }

            if(t.canFinish){
                tvFinishOrder.visibility = View.VISIBLE
            }else{
                tvFinishOrder.visibility = View.GONE
            }

            if(t.canCancel){
                tvCancelOrder.visibility = View.VISIBLE
            }else{
                tvCancelOrder.visibility = View.GONE
            }

            if(t.canCancelRefund){
                tvCancelRefund.visibility = View.VISIBLE
            }else{
                tvCancelRefund.visibility = View.GONE
            }

            if(t.canContact){
                tvPhone.visibility = View.VISIBLE
            }else{
                tvPhone.visibility = View.GONE
            }

            if(t.canPay){
                tvPay.visibility = View.VISIBLE
            }else{
                tvPay.visibility = View.GONE
            }

            if(t.canPlatform){
                tvPlant.visibility = View.VISIBLE
            }else{
                tvPlant.visibility = View.GONE
            }

            if(t.canRefund){
                tvTk.visibility = View.VISIBLE
            }else{
                tvTk.visibility = View.GONE
            }

            tvComment.setOnClickListener {
                // 评论
                var orderInfo = t.items[0]

                EvaluateActivity.toEvaluate(itemView.context,t.orderNo,0,t.userId,orderInfo.teacher,orderInfo.courseTime,orderInfo.courseName,t.items[0].pics,orderInfo.price)
            }

            tvFinishOrder.setOnClickListener{
                //完成订单
            }

            tvCancelOrder.setOnClickListener{
                //取消订单
            }

            tvTk.setOnClickListener{
                //申请退款
                RefundActivity.toRefund(itemView.context,t.orderNo,t.items[0].price)
            }

            tvPay.setOnClickListener{
                //退款
            }

            tvPhone.setOnClickListener{
                //联系老师
            }

            tvCancelRefund.setOnClickListener{
                //取消退款
            }

            tvPlant.setOnClickListener{
                //平台介入
            }
        }
    }
}