package com.jinghan.app.mvp.view.fragment

import android.support.v7.widget.LinearLayoutManager
import com.jinghan.app.mvp.model.bean.AskInfo
import com.jinghan.app.mvp.model.bean.OrderInfo
import com.jinghan.app.mvp.model.bean.OrderShowInfo
import com.jinghan.app.mvp.model.bean.ReplayInfo
import com.jinghan.app.mvp.presenter.IOrderListFragmentPresenter
import com.jinghan.app.mvp.view.adapter.AskListAdapter
import com.jinghan.app.mvp.view.adapter.OrderListAdapter
import com.jinghan.app.mvp.view.adapter.ReplayListAdapter
import com.jinghan.app.mvp.view.impl.view.IOrderListFragmentView
import com.jinghan.core.R
import com.jinghan.core.databinding.FrgOrderListBinding
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import com.jinghan.core.mvp.view.fragment.BaseRefreshFragment
import java.util.ArrayList
import javax.inject.Inject
import dagger.Lazy

/**
 * @author liuzeren
 * @time 2018/2/11    下午2:49
 * @mail lzr319@163.com
 */
@FragmentScoped
class OrderListFragment @Inject constructor() : BaseRefreshFragment<FrgOrderListBinding>(), IOrderListFragmentView {

    private var mOrderListAdapter:OrderListAdapter?=null
    private var mReplayListAdapter:ReplayListAdapter?=null
    private var mAskListAdapter:AskListAdapter?=null

    override fun onRefresh() {
        when(type){
            TYPE_ORDERS->presenter.orderList(status,true, mOrderListAdapter?.itemCount!! >0)
            TYPE_REPLAY->presenter.replayList(status,true,mReplayListAdapter?.itemCount!!>0)
            TYPE_ASK->presenter.askList(status,true,mAskListAdapter?.itemCount!!>0)
        }
    }

    override fun onLoadMore() {
        super.onLoadMore()

        when(type){
            TYPE_ORDERS->presenter.orderList(status,false,mOrderListAdapter?.itemCount!!>0)
            TYPE_REPLAY->presenter.replayList(status,false,mReplayListAdapter?.itemCount!!>0)
            TYPE_ASK->presenter.askList(status,false,mAskListAdapter?.itemCount!!>0)
        }
    }

    @Inject
    protected lateinit var presenter:IOrderListFragmentPresenter

    override fun orderList(isRefresh: Boolean, data: ArrayList<OrderShowInfo>) {
        stopRefresh()
        if(isRefresh){
            mOrderListAdapter?.setDataSource(data)
        }else{
            mOrderListAdapter?.appendData(data)
        }
    }

    override fun replayList(isRefresh: Boolean, data: ArrayList<ReplayInfo>) {
        stopRefresh()
        if(isRefresh){
            mReplayListAdapter?.setDataSource(data)
        }else{
            mReplayListAdapter?.appendData(data)
        }
    }

    override fun askList(isRefresh: Boolean, data: ArrayList<AskInfo>) {
        stopRefresh()
        if(isRefresh){
            mAskListAdapter?.setDataSource(data)
        }else{
            mAskListAdapter?.appendData(data)
        }
    }

    companion object {
        val STATUS = "list_status"

        val TYPE = "list_type"

        val TYPE_ORDERS = 1
        val TYPE_REPLAY = 2
        val TYPE_ASK = 3
    }

    /**状态*/
    private var status = OrderInfo.ALL

    /**类型*/
    private var type = TYPE_ORDERS

    override val layoutId: Int
        get() = R.layout.frg_order_list

    override fun initViews() {
        super.initViews()

        swipeToLoadLayout = binding.swipeToLoadLayout
        swipeToLoadLayout?.setOnRefreshListener(this)
        swipeToLoadLayout?.setOnLoadMoreListener(this)

        var args = arguments

        if(args?.containsKey(TYPE) == true) {
            type = args.getInt(TYPE, TYPE_ORDERS)
        }

        binding.swipeTarget.layoutManager = LinearLayoutManager(context)

        if(args?.containsKey(STATUS) == true){
            when(type){
                TYPE_ORDERS->{
                    mOrderListAdapter = OrderListAdapter()

                    status = args.getInt(STATUS,OrderInfo.ALL)
                    binding.swipeTarget.adapter = mOrderListAdapter
                }
                TYPE_REPLAY->{
                    mReplayListAdapter = ReplayListAdapter()

                    status = args.getInt(STATUS,ReplayInfo.ALL)
                    binding.swipeTarget.adapter = mReplayListAdapter
                }
                TYPE_ASK->{
                    mAskListAdapter = AskListAdapter()

                    status = args.getInt(STATUS,AskInfo.ALL)
                    binding.swipeTarget.adapter = mAskListAdapter
                }
            }
        }
    }

    override fun initData() {
        when(type){
            TYPE_ORDERS->presenter.orderList(status,true,false)
            TYPE_REPLAY->presenter.replayList(status,true,false)
            TYPE_ASK->presenter.askList(status,true,false)
        }
    }

    override fun initPresenter() {
        presenter.lifecycle(lifecycleSubject)
        presenter.takeView(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
    }
}