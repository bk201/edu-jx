package com.jinghan.app.mvp.model.response

/**
 * 发布问题响应
 * 返回单号
 * @author liuzeren
 * @time 2018/1/26    下午7:51
 * @mail lzr319@163.com
 */
class PublishQuestionResponse(var value:String) : BaseResponse()