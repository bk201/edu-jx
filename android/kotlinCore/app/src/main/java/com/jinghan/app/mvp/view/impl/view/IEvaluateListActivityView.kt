package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/3/17 11:27
 * @mail lzr319@163.com
 */
interface IEvaluateListActivityView : BaseView