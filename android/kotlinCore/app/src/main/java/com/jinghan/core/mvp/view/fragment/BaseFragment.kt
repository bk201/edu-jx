package com.jinghan.core.mvp.view.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.CheckResult
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jinghan.core.helper.PermissionUtils
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.mvp.view.impl.view.BaseView
import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.FragmentEvent
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import dagger.android.support.DaggerFragment
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.view_toolbar.view.*

/**
 * @author liuzeren
 * @time 2017/11/7    下午6:18
 * @mail lzr319@163.com
 */
abstract class BaseFragment<B:ViewDataBinding> : DaggerFragment(), LifecycleProvider<FragmentEvent>
        , BaseView, PermissionUtils.OnPermissionListener{
    protected val lifecycleSubject = BehaviorSubject.create<FragmentEvent>()

    protected lateinit var binding:B

    protected var mActivity: BaseActivity<out ViewDataBinding>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,layoutId,container,false)
        return binding.root
    }

    protected abstract val layoutId:Int

    protected fun setTitle(title:String){
        binding.root.toolBar.tvTitle.text = title
    }

    @CheckResult
    override fun lifecycle(): Observable<FragmentEvent> {
        return lifecycleSubject.hide()
    }

    @CheckResult
    override fun <T> bindUntilEvent(event: FragmentEvent): LifecycleTransformer<T> {
        return RxLifecycle.bindUntilEvent<T, FragmentEvent>(lifecycleSubject, event)
    }

    @CheckResult
    override fun <T> bindToLifecycle(): LifecycleTransformer<T> {
        return RxLifecycleAndroid.bindFragment<T>(lifecycleSubject)
    }

    @CallSuper
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        lifecycleSubject.onNext(FragmentEvent.ATTACH)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)

        mActivity = activity as? BaseActivity<in ViewDataBinding>
    }

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleSubject.onNext(FragmentEvent.CREATE)
    }

    @CallSuper
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        initPresenter()
        initViews()
        initData()

        super.onViewCreated(view, savedInstanceState)
        lifecycleSubject.onNext(FragmentEvent.CREATE_VIEW)
    }

    /**
     * 初始化Views
     */
    open fun initViews() {}

    abstract fun initData()

    abstract fun initPresenter()

    @CallSuper
    override fun onStart() {
        super.onStart()
        lifecycleSubject.onNext(FragmentEvent.START)
    }

    @CallSuper
    override fun onResume() {
        super.onResume()
        lifecycleSubject.onNext(FragmentEvent.RESUME)
    }

    @CallSuper
    override fun onPause() {
        lifecycleSubject.onNext(FragmentEvent.PAUSE)
        super.onPause()
    }

    @CallSuper
    override fun onStop() {
        lifecycleSubject.onNext(FragmentEvent.STOP)
        super.onStop()
    }

    @CallSuper
    override fun onDestroyView() {
        lifecycleSubject.onNext(FragmentEvent.DESTROY_VIEW)
        super.onDestroyView()
    }

    @CallSuper
    override fun onDestroy() {
        lifecycleSubject.onNext(FragmentEvent.DESTROY)
        super.onDestroy()
    }

    @CallSuper
    override fun onDetach() {
        lifecycleSubject.onNext(FragmentEvent.DETACH)
        super.onDetach()
    }

    override fun toast(txtId: Int) {
        toast(getString(txtId))
    }

    override fun toast(text: String) {
        if (TextUtils.isEmpty(text)) return

        mActivity?.toast(text)
    }

    override fun close() {
        mActivity?.close()
    }

    override fun showLoading(msg:String) {
        mActivity?.showLoading(msg)
    }

    override fun hideLoading() {
        mActivity?.hideLoading()
    }

    override fun error(listener: View.OnClickListener) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (PermissionUtils.verifyPermissions(grantResults)) {
            onPermissionGranted(requestCode)
        } else {
            onPermissionDenied()
        }
    }

    /**权限获取失败*/
    override fun onPermissionDenied() {
    }

    /**权限获取成功*/
    override fun onPermissionGranted(reqCode:Int) {
    }
}