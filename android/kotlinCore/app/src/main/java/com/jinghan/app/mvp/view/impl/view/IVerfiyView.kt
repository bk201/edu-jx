package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/2/5    下午9:22
 * @mail lzr319@163.com
 */
interface IVerfiyView : BaseView{

    fun onVerfiyResult()

}