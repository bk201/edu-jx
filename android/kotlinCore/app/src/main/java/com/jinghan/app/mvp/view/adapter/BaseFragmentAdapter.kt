package com.jinghan.app.mvp.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/10    下午8:34
 * @mail lzr319@163.com
 */
open class BaseFragmentAdapter<T : Fragment>(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private var fragments: ArrayList<T>? = null

    fun setFragments(fragments: ArrayList<T>) {
        this.fragments = fragments
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return fragments!![position]
    }

    override fun getCount(): Int {
        return if (null == fragments) 0 else fragments!!.size
    }
}