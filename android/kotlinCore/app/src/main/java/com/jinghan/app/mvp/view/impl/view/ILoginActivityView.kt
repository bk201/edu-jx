package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/1/24    下午7:17
 * @mail lzr319@163.com
 */
interface ILoginActivityView : BaseView{

    fun loginResult(result:Boolean,token:String)

}