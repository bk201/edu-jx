package com.jinghan.app.mvp.view.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import com.jinghan.app.mvp.model.bean.AskInfo
import com.jinghan.app.mvp.model.bean.OrderInfo
import com.jinghan.app.mvp.model.bean.ReplayInfo
import com.jinghan.app.mvp.presenter.IOrderListActivityPresenter
import com.jinghan.app.mvp.view.fragment.OrderListFragment
import com.jinghan.app.mvp.view.impl.view.IOrderListActivityView
import com.jinghan.core.databinding.AtyOrderListBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseTabFragmentAdapter
import kotlinx.android.synthetic.main.view_toolbar.view.*
import java.util.ArrayList
import javax.inject.Inject

/**
 * 我的订单,我的解答,我的提问
 * @author liuzeren
 * @time 2018/1/25    下午10:07
 * @mail lzr319@163.com
 */
class OrderListActivity(override val layoutId: Int = R.layout.aty_order_list) : BaseActivity<AtyOrderListBinding>(), IOrderListActivityView {

    private var type = TYPE_ORDERS
    private lateinit var adapter : BaseTabFragmentAdapter<Fragment,String>

    companion object {
        val TYPE = "list_type"

        val TYPE_ORDERS = 1
        val TYPE_REPLAY = 2
        val TYPE_ASK = 3
    }

    @Inject
    protected lateinit var presenter : IOrderListActivityPresenter

    override fun initViewsAndListener() {

        type = intent.getIntExtra(TYPE, TYPE_ORDERS)

        when(type){
            TYPE_ORDERS->mViewBinding.root.toolBar.tvTitle.text = "我的订单"
            TYPE_REPLAY->mViewBinding.root.toolBar.tvTitle.text = "我的解答"
            TYPE_ASK->mViewBinding.root.toolBar.tvTitle.text = "我的提问"
        }

        adapter = BaseTabFragmentAdapter(supportFragmentManager)
        mViewBinding.viewPager.adapter = adapter
        mViewBinding.tabLayout.setupWithViewPager(mViewBinding.viewPager)
    }

    override fun initData() {
        var fragments = ArrayList<Fragment>()
        var arrs:Array<String>? = null

        when(type){
            TYPE_ORDERS->{
                arrs = resources.getStringArray(R.array.orders)

                var all = OrderListFragment()
                var arg = Bundle()
                arg.putInt(OrderListFragment.STATUS,OrderInfo.ALL)
                arg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ORDERS)
                all.arguments = arg
                fragments.add(all)

                var inConsume = OrderListFragment()
                var inConsumeArg = Bundle()
                inConsumeArg.putInt(OrderListFragment.STATUS,OrderInfo.IN_CONSUME)
                inConsumeArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ORDERS)
                inConsume.arguments = inConsumeArg
                fragments.add(inConsume)

                var isConsume = OrderListFragment()
                var isConsumeArg = Bundle()
                isConsumeArg.putInt(OrderListFragment.STATUS,OrderInfo.IS_CONSUME)
                isConsumeArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ORDERS)
                isConsume.arguments = isConsumeArg
                fragments.add(isConsume)

                var evaluate = OrderListFragment()
                var evaluateArg = Bundle()
                evaluateArg.putInt(OrderListFragment.STATUS,OrderInfo.EVALUATE)
                evaluateArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ORDERS)
                evaluate.arguments = evaluateArg
                fragments.add(evaluate)

                var refund = OrderListFragment()
                var refundArg = Bundle()
                refundArg.putInt(OrderListFragment.STATUS,OrderInfo.REFUND)
                refundArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ORDERS)
                refund.arguments = refundArg
                fragments.add(refund)
            }
            TYPE_REPLAY->{
                arrs = resources.getStringArray(R.array.replays)

                var all = OrderListFragment()
                var arg = Bundle()
                arg.putInt(OrderListFragment.STATUS,ReplayInfo.ALL)
                arg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_REPLAY)
                all.arguments = arg
                fragments.add(all)

                var inConsume = OrderListFragment()
                var inConsumeArg = Bundle()
                inConsumeArg.putInt(OrderListFragment.STATUS,ReplayInfo.IN_REPLAY)
                inConsumeArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_REPLAY)
                inConsume.arguments = inConsumeArg
                fragments.add(inConsume)

                var isConsume = OrderListFragment()
                var isConsumeArg = Bundle()
                isConsumeArg.putInt(OrderListFragment.STATUS,ReplayInfo.IS_REPLAY)
                isConsumeArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_REPLAY)
                isConsume.arguments = isConsumeArg
                fragments.add(isConsume)

                var evaluate = OrderListFragment()
                var evaluateArg = Bundle()
                evaluateArg.putInt(OrderListFragment.STATUS,ReplayInfo.EVALUATE)
                evaluateArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_REPLAY)
                evaluate.arguments = evaluateArg
                fragments.add(evaluate)

                var refund = OrderListFragment()
                var refundArg = Bundle()
                refundArg.putInt(OrderListFragment.STATUS,ReplayInfo.REFUND)
                refundArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_REPLAY)
                refund.arguments = refundArg
                fragments.add(refund)
            }
            TYPE_ASK->{
                arrs = resources.getStringArray(R.array.questions)

                var all = OrderListFragment()
                var arg = Bundle()
                arg.putInt(OrderListFragment.STATUS,AskInfo.ALL)
                arg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ASK)
                all.arguments = arg
                fragments.add(all)

                var inConsume = OrderListFragment()
                var inConsumeArg = Bundle()
                inConsumeArg.putInt(OrderListFragment.STATUS,AskInfo.IN_REPLAY)
                inConsumeArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ASK)
                inConsume.arguments = inConsumeArg
                fragments.add(inConsume)

                var isConsume = OrderListFragment()
                var isConsumeArg = Bundle()
                isConsumeArg.putInt(OrderListFragment.STATUS,AskInfo.IS_REPLAY)
                isConsumeArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ASK)
                isConsume.arguments = isConsumeArg
                fragments.add(isConsume)

                var evaluate = OrderListFragment()
                var evaluateArg = Bundle()
                evaluateArg.putInt(OrderListFragment.STATUS,AskInfo.EVALUATE)
                evaluateArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ASK)
                evaluate.arguments = evaluateArg
                fragments.add(evaluate)

                var refund = OrderListFragment()
                var refundArg = Bundle()
                refundArg.putInt(OrderListFragment.STATUS,AskInfo.REFUND)
                refundArg.putInt(OrderListFragment.TYPE,OrderListFragment.TYPE_ASK)
                refund.arguments = refundArg
                fragments.add(refund)
            }
        }

        adapter.setFragments(arrs,fragments)
    }

    override fun initPresenter() {
    }
}