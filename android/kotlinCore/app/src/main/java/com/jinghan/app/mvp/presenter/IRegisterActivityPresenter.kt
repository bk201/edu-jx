package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IRegisterActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/1/22    下午9:49
 * @mail lzr319@163.com
 */
abstract class IRegisterActivityPresenter : BaseLifecyclePresenter<IRegisterActivityView,ActivityEvent>(){

    interface OnCountDownTimerListenter{
        fun onTick(time:Long)

        fun onFinish()
    }

    /**倒计时*/
    abstract fun timeDown(listener:OnCountDownTimerListenter)

    /**注册*/
    abstract fun register(userPhone:String, userPwd:String,verfiyCode:String)

    /**忘记密码*/
    abstract fun forget(userPhone:String, userPwd:String,verfiyCode:String)

    /**获取短信验证码*/
    abstract fun getVerfiyCode(userPhone:String)

    /**密码密码的短信验证码*/
    abstract fun getForgetPsdVerfiyCode(userPhone:String)

}