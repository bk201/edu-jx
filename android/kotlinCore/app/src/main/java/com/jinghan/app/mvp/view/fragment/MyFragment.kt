package com.jinghan.app.mvp.view.fragment

import android.content.Intent
import android.databinding.DataBindingUtil
import android.view.View
import com.bumptech.glide.Glide
import com.jinghan.app.helper.LoginHelper
import com.jinghan.app.mvp.model.bean.UserInfo
import com.jinghan.app.mvp.view.activity.EditInfoActivity
import com.jinghan.app.mvp.view.activity.MyAccountActivity
import com.jinghan.app.mvp.view.activity.OrderListActivity
import com.jinghan.app.mvp.view.activity.UserInfoActivity
import com.jinghan.app.mvp.view.activity.verfiy.CompanyActivity
import com.jinghan.app.mvp.view.activity.verfiy.IdCardActivity
import com.jinghan.core.R
import com.jinghan.core.databinding.FgMyBinding
import com.jinghan.core.databinding.FrgMyCompanyBinding
import com.jinghan.core.databinding.FrgMyStudentBinding
import com.jinghan.core.databinding.FrgMyTeacherBinding
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import com.jinghan.core.mvp.view.fragment.BaseFragment
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/1/25    下午8:27
 * @mail lzr319@163.com
 */
@ActivityScoped
class MyFragment @Inject constructor() : BaseFragment<FgMyBinding>(), View.OnClickListener {

    @Inject
    protected lateinit var helper: LoginHelper

    override fun initViews() {
        super.initViews()

        if (helper.isLogin()) {
            when (helper.userInfo?.type) {
                UserInfo.student -> {
                    binding.viewStudent.setOnInflateListener { stub, inflated ->
                        var viewBing = DataBindingUtil.bind<FrgMyStudentBinding>(inflated)
                        viewBing.onClick = this@MyFragment

                        if (helper.userInfo?.canApplyTeacher == false) {
                            //审核中
                            viewBing.tvTeacherStatus.text = "审核中"
                        }

                        if (helper.userInfo?.canApplyInstitution == false) {
                            //审核中
                            viewBing.tvCompanyStatus.text = "审核中"
                        }

                        if (helper.userInfo?.canApplyTeacher == false || helper.userInfo?.canApplyInstitution == false) {
                            //两个都不能点
                            viewBing.tvToTeacher.isEnabled = false
                            viewBing.tvToCompany.isEnabled = false
                        }
                    }
                    binding.viewStudent.viewStub.inflate()
                }
                UserInfo.teacher -> {
                    binding.viewTeacher.setOnInflateListener { stub, inflated ->
                        var viewBing = DataBindingUtil.bind<FrgMyTeacherBinding>(inflated)
                        viewBing.onClick = this@MyFragment
                    }
                    binding.viewTeacher.viewStub.inflate()
                }
                UserInfo.company -> {
                    binding.viewCompany.setOnInflateListener { stub, inflated ->
                        var viewBing = DataBindingUtil.bind<FrgMyCompanyBinding>(inflated)
                        viewBing.onClick = this@MyFragment
                    }
                    binding.viewCompany.viewStub.inflate()
                }
            }
        } else {
            mActivity?.finish()
            helper.login(false, null)
        }

        binding.onClick = this
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvAccount -> myAccount()
            R.id.tvOrder -> myOrder()
            R.id.tvAsk -> myQuestion()
            R.id.tvAttention -> myAttention()
            R.id.tvComment -> myComment()
            R.id.tvSysMsg -> systemMsg()
            R.id.tvFeedback -> feedback()
            R.id.tvService -> service()
            R.id.tvMail -> eMail()
            R.id.tvAnswer -> myAnswer()
            R.id.tvEdit -> editUserInfo()
            R.id.tvToTeacher -> toTeacher()
            R.id.tvToCompany -> toCompany()
            R.id.btnLogout -> {
                helper.logout()
                mActivity?.close()
            }
            else -> viewUserInfo()
        }
    }

    /**升级为机构*/
    private fun toCompany() {
        var intent = Intent(context, CompanyActivity::class.java)
        intent.putExtra(IdCardActivity.VERFIY_TYPE, IdCardActivity.TO_COMPANY)
        startActivity(intent)
    }

    /**成为老师*/
    private fun toTeacher() {
        var intent = Intent(context, IdCardActivity::class.java)
        intent.putExtra(IdCardActivity.VERFIY_TYPE, IdCardActivity.TO_TEACHER)

        startActivity(intent)
    }

    private fun viewUserInfo() {
        startActivity(Intent(context, UserInfoActivity::class.java))
    }

    /**平台邮箱*/
    private fun eMail() {}

    /**联系客服*/
    private fun service() {}

    /**意见反馈*/
    private fun feedback() {}

    /**编辑用户信息*/
    private fun editUserInfo() {
        startActivity(Intent(context, EditInfoActivity::class.java))
    }

    /**我的余额*/
    private fun myAccount() {
        startActivity(Intent(context, MyAccountActivity::class.java))
    }

    /**我的订单*/
    private fun myOrder() {
        startActivity(Intent(context, OrderListActivity::class.java))
    }

    /**我的提问*/
    private fun myQuestion() {
        var intent = Intent(context, OrderListActivity::class.java)
        intent.putExtra(OrderListActivity.TYPE, OrderListActivity.TYPE_ASK)
        startActivity(intent)
    }

    /**我的解答*/
    private fun myAnswer() {
        var intent = Intent(context, OrderListActivity::class.java)
        intent.putExtra(OrderListActivity.TYPE, OrderListActivity.TYPE_REPLAY)
        startActivity(intent)
    }

    /**我的关注*/
    private fun myAttention() {}

    /**我的评论*/
    private fun myComment() {}

    /**系统消息*/
    private fun systemMsg() {}

    override val layoutId: Int
        get() = R.layout.fg_my

    override fun initData() {
        if (helper.isLogin()) {
            binding.userInfo = helper.userInfo
            Glide.with(context).load(helper.userInfo?.avatar).into(binding.ivHead)
        }
    }

    override fun initPresenter() {
    }
}