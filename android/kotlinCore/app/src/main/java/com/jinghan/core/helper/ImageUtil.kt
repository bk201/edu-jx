package com.jinghan.core.helper

import android.annotation.TargetApi
import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import com.orhanobut.logger.Logger
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import com.jinghan.app.global.Constant
import java.io.FileInputStream
import android.graphics.drawable.Drawable



/**
 * @author liuzeren
 * @time 2018/1/26    下午9:28
 * @mail lzr319@163.com
 */
object ImageUtil{

    /**
     * 根据分辨率压缩图片比例
     *
     * @param imgPath
     * @param w
     * @param h
     * @return
     */
    private fun compressByResolution(imgPath: String, w: Int, h: Int): Bitmap {
        val opts = BitmapFactory.Options()
        opts.inJustDecodeBounds = true
        BitmapFactory.decodeFile(imgPath, opts)

        val width = opts.outWidth
        val height = opts.outHeight
        val widthScale = width / w
        val heightScale = height / h

        var scale: Int
        if (widthScale < heightScale) { //保留压缩比例小的
            scale = widthScale
        } else {
            scale = heightScale
        }

        if (scale < 1) {
            scale = 1
        }
        Logger.d("图片分辨率压缩比例：" + scale)

        opts.inSampleSize = scale

        opts.inJustDecodeBounds = false

        return BitmapFactory.decodeFile(imgPath, opts)
    }

    /**
     * 根据图片的大小设置压缩的比例，提高速度
     *
     * @param imageMB
     * @return
     */
    private fun setSubstractSize(imageMB: Int): Int {

        return if (imageMB > 1000) {
            60
        } else if (imageMB > 750) {
            40
        } else if (imageMB > 500) {
            20
        } else {
            10
        }

    }


    fun compressBitmap(srcPath: String?, ImageSize: Int, savePath: String): Boolean {

        var file = File(savePath)
        if(!file.parentFile.exists()){
            file.parentFile.mkdirs()
        }

        var subtract: Int
        Logger.d("图片处理开始..")

        var bitmap:Bitmap? = null

        srcPath?.let {
            bitmap = compressByResolution(it, 1024, 720) //分辨率压缩
        }

        if (bitmap == null) {
            Logger.d( "bitmap 为空")
            return false
        }

        val baos = ByteArrayOutputStream()
        var options = 100
        bitmap?.compress(Bitmap.CompressFormat.JPEG, options, baos)//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        Logger.d("图片分辨率压缩后：" + baos.toByteArray().size / 1024 + "KB")


        while (baos.toByteArray().size > Constant.UPLOAD_IMAGE_SIZE_LIMIT) { //循环判断如果压缩后图片是否大于ImageSize 2Mb,大于继续压缩
            subtract = setSubstractSize(baos.toByteArray().size / 1024)
            baos.reset()//重置baos即清空baos
            options -= subtract//每次都减少10
            bitmap!!.compress(Bitmap.CompressFormat.JPEG, options, baos)//这里压缩options%，把压缩后的数据存放到baos中
            Logger.d("图片压缩后：" + baos.toByteArray().size / 1024 + "KB")
        }
        Logger.d("图片处理完成!" + baos.toByteArray().size / 1024 + "KB")
        try {
            val fos = FileOutputStream(File(savePath))//将压缩后的图片保存的本地上指定路径中
            fos.write(baos.toByteArray())
            fos.flush()
            fos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (bitmap != null) {
            bitmap!!.recycle()
        }

        return true //压缩成功返回ture
    }

    /**
     * 获取指定文件大小(单位：字节)
     *
     * @param file
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    fun getFileSize(file: File?): Int {
        if (file == null) {
            return 0
        }

        if (file.exists()) {
            var fis = FileInputStream(file)
            return fis.available()
        }
        return 0
    }

    fun parseUriToPath(context: Context, uri: Uri?) : String?{

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            return getImageAbsolutePath(context,uri)
        }

        return getRealFilePath(context,uri)
    }

    //此方法 只能用于4.4以下的版本
    fun getRealFilePath(context: Context, uri: Uri?): String? {
        if (null == uri) return null
        val scheme = uri.scheme
        var data: String? = null
        if (scheme == null)
            data = uri.path
        else if (ContentResolver.SCHEME_FILE == scheme) {
            data = uri.path
        } else if (ContentResolver.SCHEME_CONTENT == scheme) {
            val projection = arrayOf(MediaStore.Images.ImageColumns.DATA)
            val cursor = context.contentResolver.query(uri, projection, null, null, null)

            //            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                    if (index > -1) {
                        data = cursor.getString(index)
                    }
                }
                cursor.close()
            }
        }
        return data
    }


    /**
     * 根据Uri获取图片绝对路径，解决Android4.4以上版本Uri转换
     *
     * @param context
     * @param imageUri
     * @author yaoxing
     * @date 2014-10-12
     */
    @TargetApi(19)
    fun getImageAbsolutePath(context: Context?, imageUri: Uri?): String? {
        if (context == null || imageUri == null)
            return null
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, imageUri)) {
            if (isExternalStorageDocument(imageUri)) {
                val docId = DocumentsContract.getDocumentId(imageUri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
            } else if (isDownloadsDocument(imageUri)) {
                val id = DocumentsContract.getDocumentId(imageUri)
                val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)!!)
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(imageUri)) {
                val docId = DocumentsContract.getDocumentId(imageUri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = MediaStore.Images.Media._ID + "=?"
                val selectionArgs = arrayOf(split[1])
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } // MediaStore (and general)
        else if ("content".equals(imageUri.scheme, ignoreCase = true)) {
            // Return the remote address
            return if (isGooglePhotosUri(imageUri)) imageUri.lastPathSegment else getDataColumn(context, imageUri, null, null)
        } else if ("file".equals(imageUri.scheme, ignoreCase = true)) {
            return imageUri.path
        }// File
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = MediaStore.Images.Media.DATA
        val projection = arrayOf(column)
        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            if (cursor != null)
                cursor.close()
        }
        return null
    }

    /**
     * drawable转bitmap
     *
     * @param drawable drawable对象
     * @return bitmap对象
     */
    @JvmStatic fun drawable2Bitmap(drawable:Drawable) : Bitmap {
        // 取 drawable 的长宽
        var w = drawable.getIntrinsicWidth()
        var h = drawable.getIntrinsicHeight()

        // 取 drawable 的颜色格式
        var config:Bitmap.Config

        if(drawable.getOpacity() != PixelFormat.OPAQUE){
            config = Bitmap.Config.ARGB_8888
        }else{
            config = Bitmap.Config.RGB_565
        }

        // 建立对应 bitmap
        var bitmap:Bitmap = Bitmap.createBitmap(w, h, config)
        // 建立对应 bitmap 的画布
        var canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, w, h)
        // 把 drawable 内容画到画布中
        drawable.draw(canvas)
        return bitmap
    }
}