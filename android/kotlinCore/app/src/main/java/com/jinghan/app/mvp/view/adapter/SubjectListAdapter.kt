package com.jinghan.app.mvp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/2    下午8:59
 * @mail lzr319@163.com
 */
class SubjectListAdapter constructor(var listener:SubjectItemClickListener? = null) : BaseAdapter<SubjectListAdapter.SubjectListViewHolder, String>(){

    interface SubjectItemClickListener{
        fun onItemClick(value:String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectListViewHolder {
        return SubjectListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.aty_subject_list_item,parent,false))
    }


    inner class SubjectListViewHolder(itemView:View) : BaseViewHolder<String>(itemView){

        private var tvSubject:TextView

        init {
            tvSubject = itemView.findViewById(R.id.tvSubject)
        }

        override fun update(t: String) {
            tvSubject.text = t

            tvSubject.setOnClickListener {
                listener?.onItemClick(t)
            }
        }
    }

}