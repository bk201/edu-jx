package com.jinghan.app.mvp.view.activity

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.bumptech.glide.Glide
import com.jinghan.app.global.Constant
import com.jinghan.app.mvp.presenter.IEvaluateActivityPresenter
import com.jinghan.app.mvp.presenter.IFileUploadActivityPresenter
import com.jinghan.app.mvp.view.adapter.EvaluateAadpter
import com.jinghan.app.mvp.view.adapter.decoration.HorizontalDecoration
import com.jinghan.app.mvp.view.adapter.decoration.RightPaddingDecoration
import com.jinghan.app.mvp.view.impl.view.IEvaluateActivityView
import com.jinghan.app.mvp.view.impl.view.IFileUploadView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyEvaluateBinding
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import com.jinghan.core.helper.AndroidUtils
import com.jinghan.core.helper.ImageUtil
import com.jinghan.core.mvp.dialog.ChooseImageDialog
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.view_toolbar.view.*
import java.util.*
import javax.inject.Inject

/**
 * 评价
 * @author liuzeren
 * @time 2018/3/16 13:56
 * @mail lzr319@163.com
 */
class EvaluateActivity : BaseActivity<AtyEvaluateBinding>(),View.OnClickListener,IEvaluateActivityView,IFileUploadView{
    override fun onUploadResult(imgList: ArrayList<String>) {

    }

    override fun onUploadResult(result: Boolean, image: String) {
        if(result){
            adapter.appendData(image)
        }
    }

    private var questionNo:String=""
    private var teacherId:Int=0
    private var studentId:Int=0
    private var pics:String=""
    private var price:Int=0

    var dialog : ChooseImageDialog?=null

    private lateinit var adapter: EvaluateAadpter

    @Inject
    protected lateinit var presenter:IEvaluateActivityPresenter

    @Inject
    protected lateinit var filePresenter:IFileUploadActivityPresenter

    companion object {
        val QUESTION_NO = "QUESTION_NO"
        val TEACHER_ID = "TEACHER_ID"
        val STUDENT_ID = "STUDENT_ID"
        val NAME = "NAME"
        val TIME = "TIME"
        val SUBJECT = "SUBJECT"
        val PICS = "PICS"
        val PRICE = "PRICE"

        fun toEvaluate(context:Context,questionNo:String,teacherId:Int,studentId:Int,name:String?,time:String?,subject:String?,pics:String?,price:Int){
            var intent = Intent(context,EvaluateActivity::class.java)
            intent.putExtra(QUESTION_NO,questionNo)
            intent.putExtra(TEACHER_ID,teacherId)
            intent.putExtra(STUDENT_ID,studentId)
            intent.putExtra(NAME,name)
            intent.putExtra(TIME,time)
            intent.putExtra(SUBJECT,subject)
            intent.putExtra(PICS,pics)
            intent.putExtra(PRICE,price)

            context.startActivity(intent)
        }
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.tvEvaluate1->{
                mViewBinding.etContent.setText(mViewBinding.etContent.text.toString()+"【"+mViewBinding.tvEvaluate1.text+"】")
            }
            R.id.tvEvaluate2->{
                mViewBinding.etContent.setText(mViewBinding.etContent.text.toString()+"【"+mViewBinding.tvEvaluate2.text+"】")
            }
            R.id.tvEvaluate3->{
                mViewBinding.etContent.setText(mViewBinding.etContent.text.toString()+"【"+mViewBinding.tvEvaluate3.text+"】")
            }
            R.id.tvEvaluate4->{
                mViewBinding.etContent.setText(mViewBinding.etContent.text.toString()+"【"+mViewBinding.tvEvaluate4.text+"】")
            }
            R.id.tvEvaluate5->{
                mViewBinding.etContent.setText(mViewBinding.etContent.text.toString()+"【"+mViewBinding.tvEvaluate5.text+"】")
            }
            R.id.btnEvaluate->{
                presenter.evaluate(mViewBinding.etContent.text.toString(),adapter.data,questionNo)
            }
            R.id.viewTeacher->{
                var intent = Intent(this,QuestionInfoActivity::class.java)
                intent.putExtra(QuestionInfoActivity.QUESTION_NO,questionNo)
                startActivity(intent)
            }
        }
    }

    override val layoutId: Int
        get() = R.layout.aty_evaluate

    private fun check(){
        mViewBinding.btnEvaluate.isEnabled = !TextUtils.isEmpty(mViewBinding.etContent.text) && !TextUtils.isEmpty(questionNo)
    }

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
        mViewBinding.root.toolBar.tvTitle.text = "评价"

        mViewBinding.etContent.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                check()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        if(intent.hasExtra(QUESTION_NO)){
            questionNo = intent.getStringExtra(QUESTION_NO)
        }

        if(intent.hasExtra(TEACHER_ID)){
            teacherId = intent.getIntExtra(TEACHER_ID,0)
        }

        if(intent.hasExtra(STUDENT_ID)){
            studentId = intent.getIntExtra(STUDENT_ID,0)
        }

        if(intent.hasExtra(NAME)){
            mViewBinding.tvName.text = intent.getStringExtra(NAME)
        }

        if(intent.hasExtra(TIME)){
            mViewBinding.tvTime.text = intent.getStringExtra(TIME)
        }

        if(intent.hasExtra(SUBJECT)){
            mViewBinding.tvCourse.text = "课程："+intent.getStringExtra(SUBJECT)
        }

        if(intent.hasExtra(PICS)){
            pics = intent.getStringExtra(PICS)
            Glide.with(this).load(pics).centerCrop().into(mViewBinding.ivCover)
        }

        if(intent.hasExtra(PRICE)){
            price = intent.getIntExtra(PRICE,0)
        }
        mViewBinding.tvPrice.text = getString(R.string.money,price)

        mViewBinding.recyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        mViewBinding.recyclerView.addItemDecoration(RightPaddingDecoration(this,AndroidUtils.dip2px(this,5)))
        adapter = EvaluateAadpter(object : EvaluateAadpter.OnItemClickListener{
            override fun onAdd() {
                dialog = ChooseImageDialog(this@EvaluateActivity,true, DialogInterface.OnCancelListener { })
                dialog?.show()
            }

            override fun onItemClick(index: Int) {
                ImageViewerActivity.startViewer(this@EvaluateActivity,adapter.data,index)
            }
        })
        mViewBinding.recyclerView.adapter = adapter
    }

    override fun initData() {
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)

        filePresenter.takeView(this)
        filePresenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
        filePresenter.dropView()
    }

    override fun onPermissionGranted(reqCode: Int) {
        super.onPermissionGranted(reqCode)

        dialog?.onPermissionGranted(reqCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            ChooseImageDialog.GET_IMAGE_FROM_PHONE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                        if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(this, it.data), 2, path)) {
//                            adapter.appendData(path)
                            filePresenter.uploadImage(path,object :UploadProgressListener{
                                override fun onProgress(bytesWritten: Long, contentLength: Long) {
                                }
                            })
                        }
                    }
                }
            }
            ChooseImageDialog.GET_IMAGE_BY_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                    if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(this, ChooseImageDialog.imageUriFromCamera), 2, path)) {
//                        adapter.appendData(path)
                        filePresenter.uploadImage(path,object :UploadProgressListener{
                            override fun onProgress(bytesWritten: Long, contentLength: Long) {
                            }
                        })
                    }
                }
            }
        }
    }
}