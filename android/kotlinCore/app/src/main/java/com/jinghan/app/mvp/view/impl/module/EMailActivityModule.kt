package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IVerfiyPresenter
import com.jinghan.app.mvp.presenter.impl.VerfiyPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/2/5    下午9:26
 * @mail lzr319@163.com
 */
@Module
abstract class EMailActivityModule{

    @ActivityScoped
    @Binds
    abstract fun verfiyPresenter(mVerfiyPresenter: VerfiyPresenter) : IVerfiyPresenter


}