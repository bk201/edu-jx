package com.jinghan.core.mvp.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.ViewGroup;

import com.jinghan.app.mvp.view.adapter.BaseFragmentAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuzeren
 * @time 2018/1/22 13:45
 * @mail lzr319@163.com
 */
public class BaseTabFragmentAdapter<T extends Fragment,F extends CharSequence> extends BaseFragmentAdapter<T> {

    private List<T> fragments = new ArrayList<>();
    private F[] titles = null;

    public BaseTabFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setFragments(F[] titles,List<T> fragments){
        this.fragments = fragments;
        this.titles = titles;

        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }

    @Override
    public T getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return null == titles ? 0:titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}