package com.jinghan.app.mvp.model.response

/**
 * @author liuzeren
 * @time 2018/1/24    下午7:24
 * @mail lzr319@163.com
 */
class LoginResponse(var value:String) : BaseResponse()