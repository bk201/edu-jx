package com.jinghan.app.mvp.presenter.impl;

import com.jinghan.app.helper.LoginHelper;
import com.jinghan.app.mvp.model.response.ImageUploadResponse;
import com.jinghan.app.mvp.presenter.IFileUploadActivityPresenter;
import com.jinghan.core.dependencies.http.model.UploadProgressListener;
import com.jinghan.core.helper.MIMEUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * @author liuzeren
 * @time 2018/1/31    下午9:47
 * @mail lzr319@163.com
 */
public class FileUploadActivityPresenter extends IFileUploadActivityPresenter {

    @Inject
    public FileUploadActivityPresenter(){}

    @Inject
    protected LoginHelper helper;

    private Observable<String> pathToObservable(String path) {
        File file = new File(path);

        MultipartBody.Part part = MultipartBody.Part.createFormData("file"
                , file.getName(), RequestBody.create(MIMEUtils.getMIMEType(file.getName()), file));


        Observable<String> observable = mOkHttp.getFileUploadService(null)
                .uploadQuestionPic(part,helper.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(v->v.getValue());

        return observable;
    }

    @Override
    public void uploadImage(@NotNull UploadProgressListener listener, @NotNull ArrayList<String> paths) {

        getMView().showLoading("正在上传文件");

        Observable<String> arr[] = new Observable[paths.size()];
        for(int i=0;i<paths.size();i++){
            arr[i] = pathToObservable(paths.get(i));
        }


        Observable.zipArray(objects -> {
            ArrayList<String> arrayList = new ArrayList<>();
            for (Object object : objects) {
                arrayList.add(object.toString());
            }
            return arrayList;
        }, true, 1, arr)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DefaultObserver<ArrayList<String>>() {

                    private ArrayList<String> imgs;

                    @Override
                    public void onNext(ArrayList<String> strings) {
                        imgs = strings;
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(null != getMView()){
                            getMView().toast("图片上传失败，请重试〜");
                            getMView().hideLoading();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if(null != getMView()) {
                            getMView().onUploadResult(imgs);
                            getMView().hideLoading();
                        }

                    }
                });

    }

    @Override
    public void uploadImage(@NotNull String imagePath, @NotNull UploadProgressListener mUploadListener) {
        if(!helper.isLogin()){
            helper.login(false, isSuccess -> {
                if(isSuccess){
                    uploadImage(imagePath,mUploadListener);
                }
            });

            return;
        }

        File file = new File(imagePath);

        if(!file.exists()){
            getMView().onUploadResult(false,"");
        }else{
            getMView().showLoading("正在上传文件...");

            MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MIMEUtils.getMIMEType(file.getName()), file));
            mOkHttp.getFileUploadService(mUploadListener)
                    .uploadQuestionPic(part,helper.getToken())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DefaultObserver<ImageUploadResponse>() {
                        @Override
                        public void onNext(ImageUploadResponse response) {
                            if(response.isSuccess()){
                                getMView().onUploadResult(true,response.getValue());
                            }else{
                                getMView().onUploadResult(false,"");
                                getMView().toast(response.getMsg());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            getMView().hideLoading();
                            getMView().onUploadResult(false,"");
                        }

                        @Override
                        public void onComplete() {
                            getMView().hideLoading();
                        }
            });

        }
    }

}
