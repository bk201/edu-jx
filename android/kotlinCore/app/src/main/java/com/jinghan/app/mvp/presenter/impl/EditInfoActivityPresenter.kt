package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.bean.CompanyInfo
import com.jinghan.app.mvp.model.bean.StudentInfo
import com.jinghan.app.mvp.model.bean.TeacherInfo
import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.model.response.CompanyInfoResponse
import com.jinghan.app.mvp.model.response.EditUserInfoResponse
import com.jinghan.app.mvp.model.response.TeacherInfoResponse
import com.jinghan.app.mvp.presenter.IEditInfoActivityPresenter
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/12    下午2:01
 * @mail lzr319@163.com
 */
class EditInfoActivityPresenter @Inject constructor(): IEditInfoActivityPresenter(){
    override fun getTeacherInfo() {
        mView?.let{it.showLoading("正在获取您的信息...")}

        mOkHttp.retrofit.builder(UserService::class.java).getTeacherInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<TeacherInfoResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : TeacherInfoResponse) {
                        if(response.isSuccess){
                            mView?.let{
                                it.onTeacherInfo(response.value)
                            }
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

    override fun getCompanyInfo() {
        mView?.let{it.showLoading("正在获取您的信息...")}

        mOkHttp.retrofit.builder(UserService::class.java).getCompanyInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<CompanyInfoResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : CompanyInfoResponse) {
                        if(response.isSuccess){
                            mView?.let{
                                it.onCompanyInfo(response.value)
                            }
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

    override fun updateTeacherInfo(info: TeacherInfo?) {
        mView?.let{it.showLoading("正在提交您的信息...")}

        mOkHttp.retrofit.builder(UserService::class.java).updateTeacherInfo(info)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{
                                it.toast("信息提交成功")
                                it.close()
                            }
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

    override fun updateCompanyInfo(info: CompanyInfo?) {
        mView?.let{it.showLoading("正在提交您的信息...")}

        mOkHttp.retrofit.builder(UserService::class.java).updateCompanyInfo(info)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{
                                it.toast("信息提交成功")
                                it.close()
                            }
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

    override fun getEditUserInfo() {
        mView?.let{it.showLoading("正在获取您的信息...")}

        mOkHttp.retrofit.builder(UserService::class.java).getStudentInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<EditUserInfoResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : EditUserInfoResponse) {
                        if(response.isSuccess){
                            mView?.let{
                                it.onStudentInfo(response.value)
                            }
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

    override fun editUserInfo(info: StudentInfo?) {

        mView?.let{it.showLoading("正在提交您的信息...")}

        mOkHttp.retrofit.builder(UserService::class.java).editStudentInfo(info)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{
                                it.toast("信息提交成功")
                                it.close()
                            }
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

}