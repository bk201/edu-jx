package com.jinghan.core.mvp.view

import android.support.v7.widget.RecyclerView

/**
 * @author liuzeren
 * @time 2018/1/27    上午11:40
 * @mail lzr319@163.com
 */
abstract class BaseArrayAdapter<T : BaseViewHolder<F>,F> constructor(var data:Array<F>? = null) : RecyclerView.Adapter<T>(){

    fun setDataSource(data:Array<F>?){
        this.data = data
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: T, position: Int) {
        data?.let {
            holder.update(it.get(position))
        }
    }

    override fun getItemCount(): Int {
        return data?.size?:0
    }
}