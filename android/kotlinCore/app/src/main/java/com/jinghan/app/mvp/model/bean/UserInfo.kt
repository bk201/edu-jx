package com.jinghan.app.mvp.model.bean

/**
 *
 * canApplyTeacher: 是否可以申请老师,
 * canApplyInstitution: 是否可以申请机构false:表示正在申请角色,不能重复申请,
 * 此时按钮提示正在申请中相关文字,一旦有一个属性是false,
 * 则两个按钮都不可以操作如果两个值都是false,
 * 则表明用户已经具有相关身份,也不能提交升级申请,
 * 参考 type (0:学生,1:老师,2:机构) 判断身份类型
 * @author liuzeren
 * @time 2018/1/24    下午7:40
 * @mail lzr319@163.com
 */
data class UserInfo(var addTime:String?="",var address:String?=""
,var avatar:String?="",var birthday:String?="",var city:String?=""
,var email:String?="",var emailBind:Int,var id:Long,var mobile:String?="",var money:Int
,var name:String?="",var nickName:String?="",var province:String?="",var sex:Int,var type:Int
,var typeQualified:Int,var canApplyTeacher:Boolean=true,var canApplyInstitution:Boolean=true){
    companion object {
        /**学生*/
        val student = 0

        /**老师*/
        val teacher = 1

        /**机构*/
        val company = 2
    }

    /**0:保密,1:男,2:女*/
    fun getUserSex():String{
        if(1==sex){return "男"}else if(2==sex){return "女"}else{return "保密"}
    }
}