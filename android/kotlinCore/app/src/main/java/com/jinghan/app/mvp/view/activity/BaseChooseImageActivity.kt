package com.jinghan.app.mvp.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.databinding.ViewDataBinding
import com.jinghan.app.global.Constant
import com.jinghan.app.mvp.presenter.IFileUploadActivityPresenter
import com.jinghan.app.mvp.view.impl.view.IFileUploadView
import com.jinghan.core.helper.ImageUtil
import com.jinghan.core.mvp.dialog.ChooseImageDialog
import com.jinghan.core.mvp.view.activity.BaseActivity
import java.util.*
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/4    下午9:01
 * @mail lzr319@163.com
 */
abstract class BaseChooseImageActivity<B: ViewDataBinding> : BaseActivity<B>(), IFileUploadView {

    @Inject
    protected lateinit var presenter: IFileUploadActivityPresenter

    protected abstract fun initPath(path:String)

    var dialog: ChooseImageDialog? = null

    override fun initData() {
        dialog = ChooseImageDialog(this,true,object : DialogInterface.OnCancelListener{
            override fun onCancel(dialog: DialogInterface?) {

            }
        })
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    @SuppressLint("MissingPermission")
    override fun onPermissionGranted(reqCode:Int) {
        super.onPermissionGranted(reqCode)

        if(reqCode == ChooseImageDialog.REQ_CAMERA_AND_STORAGE) {
            dialog?.onPermissionGranted(reqCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            ChooseImageDialog.GET_IMAGE_FROM_PHONE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                        if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(baseContext ,it.data), 2, path)) {
                            initPath(path)
                        }
                    }
                }
            }
            ChooseImageDialog.GET_IMAGE_BY_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                    if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(baseContext, ChooseImageDialog.imageUriFromCamera), 2, path)) {
                        initPath(path)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
    }
}