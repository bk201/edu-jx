package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.CourseInfo

/**
 * @author liuzeren
 * @time 2018/3/14 11:56
 * @mail lzr319@163.com
 */
class CourseInfoListResponse constructor(var values:Array<CourseInfo>) : BasePageResponse()