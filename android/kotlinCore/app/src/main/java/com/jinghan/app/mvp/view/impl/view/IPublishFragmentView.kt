package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/1/26    下午8:38
 * @mail lzr319@163.com
 */
interface IPublishFragmentView : BaseView{

    /**
     * @param orderId 订单ID
     * 问题发布结果
     * */
    fun publishResult(orderId:String)

    /**
     * @param result 图片上传结果
     * @param url 返回的图片url
     * 图片上传结果*/
    fun imageUploadResult(result:Boolean, url:String)

    /**
     * 价格配置
     * */
    fun onMoney(money:Int)

    /**
     * 科目列表
     * */
    fun subjectList(subjects:ArrayList<String>)

}