package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.ReplayInfo

/**
 * @author liuzeren
 * @time 2018/3/17 11:07
 * @mail lzr319@163.com
 */
class ReplayInfoListResponse constructor(var values:ArrayList<ReplayInfo>) : BaseResponse()