package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.ILoginActivityPresenter
import com.jinghan.app.mvp.presenter.impl.LoginActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2017/11/10    下午2:03
 * @mail lzr319@163.com
 */
@Module
abstract class LoginActivityModule {

    @ActivityScoped
    @Binds
    abstract fun loginActivityPresenter(loginActivityPresenter: LoginActivityPresenter) : ILoginActivityPresenter

}