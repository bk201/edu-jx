package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.ILoginActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/1/24    下午7:19
 * @mail lzr319@163.com
 */
abstract class ILoginActivityPresenter : BaseLifecyclePresenter<ILoginActivityView,ActivityEvent>(){

    abstract fun login(phone:String,pwd:String)

}