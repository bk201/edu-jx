package com.jinghan.app.mvp.model.bean

/**
 * 评价信息
 * 评价内容,图片列表,问题单号
 * @author liuzeren
 * @time 2018/3/14 11:29
 * @mail lzr319@163.com
 */
data class EvaluateInfo(var content:String,var pics:ArrayList<String>,var questionNo:String)