package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.presenter.IEvaluateListActivityPresenter

/**
 * @author liuzeren
 * @time 2018/3/17 11:28
 * @mail lzr319@163.com
 */
class EvaluateListActivityPresenter : IEvaluateListActivityPresenter()