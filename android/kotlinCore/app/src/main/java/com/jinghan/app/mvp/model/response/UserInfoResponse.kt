package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.UserInfo

/**
 * @author liuzeren
 * @time 2018/1/24    下午9:09
 * @mail lzr319@163.com
 */
class UserInfoResponse(var value:UserInfo) : BaseResponse(){}