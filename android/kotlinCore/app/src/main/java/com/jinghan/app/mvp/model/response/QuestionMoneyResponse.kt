package com.jinghan.app.mvp.model.response

/**
 * @author liuzeren
 * @time 2018/1/31    下午8:41
 * @mail lzr319@163.com
 */
class QuestionMoneyResponse(var value:Int):BaseResponse()