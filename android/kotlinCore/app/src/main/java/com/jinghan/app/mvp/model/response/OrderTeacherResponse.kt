package com.jinghan.app.mvp.model.response

/**
 * @author liuzeren
 * @time 2018/3/14 13:56
 * @mail lzr319@163.com
 */
class OrderTeacherResponse constructor(var value:String) : BaseResponse()