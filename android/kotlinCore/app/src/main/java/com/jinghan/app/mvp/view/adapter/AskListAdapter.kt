package com.jinghan.app.mvp.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.jh.sdk.dependencies.glide.transformations.RoundedCornersTransformation
import com.jinghan.app.mvp.model.bean.AskInfo
import com.jinghan.app.mvp.view.activity.EvaluateActivity
import com.jinghan.app.mvp.view.activity.QuestionInfoActivity
import com.jinghan.app.mvp.view.activity.RefundActivity
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/11    下午4:07
 * @mail lzr319@163.com
 */
class AskListAdapter : BaseAdapter<AskListAdapter.AskListViewHolder, AskInfo>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AskListViewHolder {
        return AskListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.frg_ask_list_item,parent,false))
    }

    inner class AskListViewHolder constructor(itemView: View) : BaseViewHolder<AskInfo>(itemView){

        var tvOrderId:TextView
        var tvContent:TextView
        var ivCover:ImageView
        var tvMoney:TextView

        /**立即评价*/
        private var tvComment:TextView

        /**完成订单*/
        private var tvFinishOrder:TextView

        /**取消订单*/
        private var tvCancelOrder:TextView

        /**取消退款*/
        private var tvCancelRefund:TextView

        /**平台介入*/
        private var tvPlant:TextView

        /**立即退款*/
        private var tvPay:TextView

        /**申请退款*/
        private var tvTk:TextView

        /**联系老师*/
        private var tvPhone:TextView

        init {
            tvOrderId = itemView.findViewById(R.id.tvOrderId)
            tvContent = itemView.findViewById(R.id.tvContent)
            ivCover = itemView.findViewById(R.id.ivCover)
            tvMoney = itemView.findViewById(R.id.tvMoney)

            tvComment = itemView.findViewById(R.id.tvComment)
            tvFinishOrder = itemView.findViewById(R.id.tvFinishOrder)
            tvCancelOrder = itemView.findViewById(R.id.tvCancelOrder)
            tvTk = itemView.findViewById(R.id.tvTk)
            tvPay = itemView.findViewById(R.id.tvPay)
            tvPhone = itemView.findViewById(R.id.tvPhone)
            tvCancelRefund = itemView.findViewById(R.id.tvCancelRefund)
            tvPlant = itemView.findViewById(R.id.tvPlant)
        }


        override fun update(t: AskInfo) {
            tvContent.text = t.description
            tvMoney.text = itemView.context.getString(R.string.money,t.money)
            tvOrderId.text = itemView.context.getString(R.string.service_order_no,t.questionNo)

            if(null != t.pictures && t.pictures?.size!!>0){
                Glide.with(itemView.context).load(t.pictures?.get(0)).bitmapTransform(RoundedCornersTransformation(itemView.context,5),CenterCrop(itemView.context)).into(ivCover)
            }

            itemView.setOnClickListener{
                var intent = Intent(itemView.context, QuestionInfoActivity::class.java)
                intent.putExtra(QuestionInfoActivity.QUESTION_NO,t.questionNo)

                itemView.context.startActivity(intent)
            }

            if(t.canEvaluate) {
                tvComment.visibility = View.VISIBLE
            }else{
                tvComment.visibility = View.GONE
            }

            if(t.canFinish){
                tvFinishOrder.visibility = View.VISIBLE
            }else{
                tvFinishOrder.visibility = View.GONE
            }

            if(t.canCancel){
                tvCancelOrder.visibility = View.VISIBLE
            }else{
                tvCancelOrder.visibility = View.GONE
            }

            if(t.canCancelRefund){
                tvCancelRefund.visibility = View.VISIBLE
            }else{
                tvCancelRefund.visibility = View.GONE
            }

            if(t.canContact){
                tvPhone.visibility = View.VISIBLE
            }else{
                tvPhone.visibility = View.GONE
            }

            if(t.canPay){
                tvPay.visibility = View.VISIBLE
            }else{
                tvPay.visibility = View.GONE
            }

            if(t.canPlatform){
                tvPlant.visibility = View.VISIBLE
            }else{
                tvPlant.visibility = View.GONE
            }

            if(t.canRefund){
                tvTk.visibility = View.VISIBLE
            }else{
                tvTk.visibility = View.GONE
            }

            tvComment.setOnClickListener {
                // 评论
                var image:String? = null

                if(t.pictures?.size!! >0){
                    image = t.pictures?.get(0)
                }

                EvaluateActivity.toEvaluate(itemView.context,t.questionNo,t.answerUserId,t.userId,t.answerUserName,t.answerAddTime,t.type,image,t.money)
            }

            tvFinishOrder.setOnClickListener{
                //完成订单
            }

            tvCancelOrder.setOnClickListener{
                //取消订单
            }

            tvTk.setOnClickListener{
                //申请退款
                RefundActivity.toRefund(itemView.context,t.questionNo,t.money)
            }

            tvPay.setOnClickListener{
                //退款
            }

            tvPhone.setOnClickListener{
                //联系老师
            }

            tvCancelRefund.setOnClickListener{
                //取消退款
            }

            tvPlant.setOnClickListener{
                //平台介入
            }
        }
    }
}