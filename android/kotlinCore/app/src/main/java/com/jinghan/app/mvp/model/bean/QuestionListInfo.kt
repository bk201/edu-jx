package com.jinghan.app.mvp.model.bean

import android.os.Parcel
import android.os.Parcelable


/**
 * 问题信息
 * @author liuzeren
 * @time 2018/1/26    下午7:40
 * @mail lzr319@163.com
 */
class QuestionListInfo(var addTime:String?="",
                            var address:String?="",
                            var description:String?="",
                            var distance:String?="",
                            var money:Int,
                            var userName:String?="",
                            var status:Int,
                            var pastTime:String?="",
                            var pictures:ArrayList<String>,
                            var questionNo:String?="",
                            var type:String?="") : Parcelable{
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.createStringArrayList(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(addTime)
        parcel.writeString(address)
        parcel.writeString(description)
        parcel.writeString(distance)
        parcel.writeInt(money)
        parcel.writeString(userName)
        parcel.writeInt(status)
        parcel.writeString(pastTime)
        parcel.writeStringList(pictures)
        parcel.writeString(questionNo)
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionListInfo> {
        override fun createFromParcel(parcel: Parcel): QuestionListInfo {
            return QuestionListInfo(parcel)
        }

        override fun newArray(size: Int): Array<QuestionListInfo?> {
            return arrayOfNulls(size)
        }

        /**绿灯,待解答*/
        val NOT_REPLAY = 0

        /**黄灯,有人在解答*/
        val IN_REPLAY = 1

        /**红灯 ,已经解答*/
        val REPLAYIED = 2

        /**代表所有问题*/
        val ALL = -1
    }


}
