package com.jinghan.core.mvp.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.jinghan.core.R;

/**
 * @author liuzeren
 * @time 2018/1/23 14:37
 * @mail lzr319@163.com
 */
public class LoadingDialog extends Dialog {

    private TextView mDetailsText;
    private String mDetailsLabel;

    private RotateAnimation rotateAnimation;
    private ImageView ivLoading;

    public LoadingDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dlg_loading);

        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(layoutParams);

        setCanceledOnTouchOutside(false);

        rotateAnimation = (RotateAnimation) AnimationUtils.loadAnimation(getContext(),R.anim.rotating);
        LinearInterpolator lir = new LinearInterpolator();
        rotateAnimation.setInterpolator(lir);
        initViews();
    }

    private void initViews() {
        ivLoading = findViewById(R.id.ivLoading);
        mDetailsText = findViewById(R.id.details_label);
        setDetailsLabel(mDetailsLabel);
    }

    public void setDetailsLabel(String detailsLabel) {
        mDetailsLabel = detailsLabel;
        if (mDetailsText != null) {
            if (detailsLabel != null) {
                mDetailsText.setText(detailsLabel);
                mDetailsText.setVisibility(View.VISIBLE);
            } else {
                mDetailsText.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void show() {
        super.show();

        ivLoading.startAnimation(rotateAnimation);
    }

    @Override
    public void dismiss() {
        ivLoading.clearAnimation();

        super.dismiss();
    }
}