package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IOrderListFragmentView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.FragmentEvent

/**
 * @author liuzeren
 * @time 2018/2/11    下午3:32
 * @mail lzr319@163.com
 */
abstract class IOrderListFragmentPresenter : BaseLifecyclePresenter<IOrderListFragmentView,FragmentEvent>(){

    abstract fun orderList(status:Int,isRefresh:Boolean,hasData:Boolean)

    abstract fun replayList(status:Int,isRefresh:Boolean,hasData:Boolean)

    /**我的提问*/
    abstract fun askList(status:Int,isRefresh:Boolean,hasData:Boolean)

}