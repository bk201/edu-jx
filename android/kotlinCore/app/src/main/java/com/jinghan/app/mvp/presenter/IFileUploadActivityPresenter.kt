package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IFileUploadView
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/1/31    下午9:44
 * @mail lzr319@163.com
 */
abstract class IFileUploadActivityPresenter : BaseLifecyclePresenter<IFileUploadView,ActivityEvent>(){

    abstract fun uploadImage(listener:UploadProgressListener,path: ArrayList<String>)

    abstract fun uploadImage(imagePath: String, mUploadListener: UploadProgressListener)

}