package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IOrderListActivityPresenter
import com.jinghan.app.mvp.presenter.IOrderListFragmentPresenter
import com.jinghan.app.mvp.presenter.impl.OrderListActivityPresenter
import com.jinghan.app.mvp.presenter.impl.OrderListFragmentPresenter
import com.jinghan.app.mvp.view.fragment.HomeFragment
import com.jinghan.app.mvp.view.fragment.OrderListFragment
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author liuzeren
 * @time 2017/11/10    下午2:03
 * @mail lzr319@163.com
 */
@Module
abstract class OrderListActivityModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun orderListFragment() : OrderListFragment

    @ActivityScoped
    @Binds
    abstract fun orderListActivityPresenter(orderListActivityPresenter: OrderListActivityPresenter) : IOrderListActivityPresenter

    @FragmentScoped
    @Binds
    abstract fun orderListFragmentPresenter(orderListFragmentPresenter: OrderListFragmentPresenter) : IOrderListFragmentPresenter

}