package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.ITeacherInfoActivityPresenter
import com.jinghan.app.mvp.presenter.impl.TeacherInfoActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:11
 * @mail lzr319@163.com
 */
@Module
abstract class TeacherInfoActivityModule{

    @ActivityScoped
    @Binds
    internal abstract fun teacherInfoActivityPresenter(presenter: TeacherInfoActivityPresenter): ITeacherInfoActivityPresenter

}