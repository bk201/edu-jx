package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IAnswerQuestionActivityPresenter
import com.jinghan.app.mvp.presenter.IFileUploadActivityPresenter
import com.jinghan.app.mvp.presenter.impl.AnswerQuestionActivityPresenter
import com.jinghan.app.mvp.presenter.impl.FileUploadActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/3/16 10:18
 * @mail lzr319@163.com
 */
@Module
abstract class AnswerQuestionActivityModule{

    @ActivityScoped
    @Binds
    abstract fun answerQuestionActivityPresenter(mAnswerQuestionActivityPresenter: AnswerQuestionActivityPresenter) : IAnswerQuestionActivityPresenter

    @ActivityScoped
    @Binds
    abstract fun fileUploadActivityPresenter(mFileUploadActivityPresenter: FileUploadActivityPresenter) : IFileUploadActivityPresenter

}