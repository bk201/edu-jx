package com.jinghan.app.mvp.presenter.impl

import android.location.Location
import com.jinghan.app.helper.LoginHelper
import com.jinghan.app.mvp.model.response.*
import com.jinghan.app.mvp.presenter.IPublishFragmentPresenter
import com.jinghan.core.R
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import java.io.File
import javax.inject.Inject
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.core.helper.MIMEUtils
import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.MultipartBody
import java.util.ArrayList


/**
 * @author liuzeren
 * @time 2018/1/26    下午8:39
 * @mail lzr319@163.com
 */
class PublishFragmentPresenter @Inject constructor() : IPublishFragmentPresenter(){
    override fun readMoney() {
        mOkHttp.retrofit.builder(QuestionService::class.java).questionMoney()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object: DefaultObserver<QuestionMoneyResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : QuestionMoneyResponse) {
                        if(response.isSuccess){
                            mView?.let{it.onMoney(response.value)}
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

    override fun questionList() {

        mOkHttp.retrofit.builder(QuestionService::class.java).subjectList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object: DefaultObserver<SubjectListResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : SubjectListResponse) {
                        if(response.isSuccess){
                            mView?.let{it.subjectList(response.values)}
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })

    }

    override fun reqAddress(location: Location) {

    }



    @Inject
    protected lateinit var helper:LoginHelper

    private fun pathToObservable(path:String) : Observable<String>{
        var file = File(path)

        val part = MultipartBody.Part.createFormData("file"
                , file.name, RequestBody.create(MIMEUtils.getMIMEType(file.name), file))


        var observable = mOkHttp.getFileUploadService(null)
                .uploadQuestionPic(part,helper.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map{t->t.value}

        return observable
    }

    override fun uploadImage(vararg paths: String) {

        var arr:Array<Observable<String>> = Array(paths.size, { i -> pathToObservable(paths[i])})


        var list:ArrayList<Observable<String>>
        /*for(path in paths){
            var file = File(path)
            if(!file.exists()){
                continue
            }

            val part = MultipartBody.Part.createFormData("file"
                    , file.name, RequestBody.create(MIMEUtils.getMIMEType(file.name), file))


            var observable = mOkHttp.getFileUploadService(null)
                    .uploadQuestionPic(part,helper.getToken())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            list.add(observable)
        }

        var observables1:Observable<ImageUploadResponse>[] = arrayOfNulls<Observable<ImageUploadResponse>>(list.size)

        if(list.size>0){
            Observable.zipArray(object:Function<Observable<ImageUploadResponse>[],ArrayList<String>> {

            },true,1,observables1)
        }*/

        /*Observable.zipArray(object : Function<Array<Any>,ArrayList<String>>{
            override fun apply(t: Array<Any>): ArrayList<String> {

            }
        },true,1,list.toArray()).observeOn(AndroidSchedulers.mainThread()).subscribe(object : DefaultObserver<Array<String>>() {
            override fun onNext(t: Array<String>) {

            }

            override fun onComplete() {
            }

            override fun onError(e: Throwable) {
            }
        })*/
    }

    override fun uploadImage(imagePath: String, mUploadListener: UploadProgressListener) {

        if(!helper.isLogin()){
            helper.login(false,object : LoginHelper.LoginResultListener{
                override fun loginResult(isSuccess: Boolean) {
                    if(isSuccess){
                        uploadImage(imagePath,mUploadListener)
                    }
                }
            })

            return
        }

        var file = File(imagePath)

        if(!file.exists()){
            mView?.imageUploadResult(false,"")
        }else{
            val part = MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MIMEUtils.getMIMEType(file.name), file))
            mOkHttp.getFileUploadService(mUploadListener)
                    .uploadQuestionPic(part,helper.getToken())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : DefaultObserver<ImageUploadResponse>() {

                        override fun onNext(response: ImageUploadResponse) {
                            if(response.isSuccess){
                                mView?.imageUploadResult(true,response.value)
                            }else{
                                mView?.imageUploadResult(false,"")
                            }
                        }

                        override fun onError(e: Throwable) {
                            mView?.imageUploadResult(false,"")
                        }

                        override fun onComplete() {}
                    })

        }

    }


    data class PublishQuestion(var address:String?="",
                               var city:String?="",
                               var cityCode:String?="",
                               var country:String?="",
                               var description:String,
                               var latitude:Double?=0.0,
                               var longitude:Double?=0.0,
                               var money:Int,
                               var pics:ArrayList<String>,
                               var region:String?="",
                               var regionCode:String?="",
                               var state:String?="",
                               var type:String)


    override fun publishQuestion(address:String?,
                                 city:String?,
                                 cityCode:String?,
                                 country:String?,
                                 description:String,
                                 latitude:Double?,
                                 longitude:Double?,
                                 money:Int,
                                 pics:ArrayList<String>,
                                 region:String?,
                                 regionCode:String?,
                                 state:String?,
                                 type:String) {

        var question = PublishQuestion(address,
                city,
                cityCode,
                country,
                description,
                latitude,
                longitude,
                money,
                pics,
        region,
        regionCode,
        state,
        type)

        mOkHttp.retrofit.builder(QuestionService::class.java).publishQuestion(question)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<PublishQuestionResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        mView?.toast(R.string.request_failure)
                    }

                    override fun onNext(response: PublishQuestionResponse) {
                        if(response.isSuccess){
                            mView?.publishResult(response.value)
                        }else{
                            mView?.toast(response.msg)
                        }
                    }
                })
    }

}