package com.jinghan.app.mvp.view.activity.verfiy

import android.content.Intent
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.jinghan.app.mvp.view.activity.BaseChooseImageActivity
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyIdcardBinding
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import kotlinx.android.synthetic.main.view_toolbar.view.*
import java.util.*

/**
 * 身份证认证
 * @author liuzeren
 * @time 2018/1/28    下午9:04
 * @mail lzr319@163.com
 */
class IdCardActivity constructor() : BaseChooseImageActivity<AtyIdcardBinding>(), View.OnClickListener{
    override fun onUploadResult(imgList: ArrayList<String>) {

    }

    override fun onUploadResult(result: Boolean, image: String) {
        if(result){
            if(TextUtils.isEmpty(frontImageUrl)){
                frontImageUrl = image
                presenter.uploadImage(backImagePath,object : UploadProgressListener{
                    override fun onProgress(bytesWritten: Long, contentLength: Long) {

                    }
                })
            }else{
                backImageUrl = image
                toNextVerfiy()
            }
        }
    }

    private fun toNextVerfiy(){
        var intent : Intent? = null

        if(type == TO_TEACHER){
            intent = Intent(baseContext,EducationActivity::class.java)
        }else{
            intent = Intent(baseContext,CompanyActivity::class.java)
        }

        intent.putExtra(ID_CODE,mViewBinding.etCredit.text.toString())
        intent.putExtra(FRONT_IMAGE,frontImageUrl)
        intent.putExtra(BACK_IMAGE,backImageUrl)
        intent.putExtra(VERFIY_TYPE,type)

        startActivity(intent)
    }

    private var type = TO_TEACHER

    companion object {

        val VERFIY_TYPE = "verfiy_type"

        /**成功老师验证*/
        val TO_TEACHER = 1

        /**成为机构验证*/
        val TO_COMPANY = 2

        /**身份证号*/
        val ID_CODE = "id_code"

        /**前图*/
        val FRONT_IMAGE = "front_image"

        /**背图*/
        val BACK_IMAGE = "back_image"
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnNext -> next()
            R.id.viewIdCardFront -> addImageFront()
            R.id.viewIdCardBack -> addImageBack()
        }
    }

    /**身份证前面图*/
    private lateinit var frontImagePath:String
    /**上传后的身份证前面图*/
    private var frontImageUrl:String = ""

    /**身份证背景图*/
    private lateinit var backImagePath:String
    /**上传后的身份证背面图*/
    private var backImageUrl:String = ""

    /**前面*/
    private var FRONT:Int = 1
    /**背面*/
    private var BACK:Int = 2

    private var clickType:Int = FRONT

    /**添加身份证正面*/
    private fun addImageFront(){
        clickType = FRONT
        dialog?.show()
    }

    /**添加身份证反面*/
    private fun addImageBack(){
        clickType = BACK
        dialog?.show()
    }

    /**下一步*/
    private fun next(){
        if(TextUtils.isEmpty(mViewBinding.etCredit.text.toString())){
            toast(mViewBinding.etCredit.hint.toString())
            return
        }

        if(TextUtils.isEmpty(frontImagePath)){
            toast("身份证正面照不能为空")
            return
        }

        if(TextUtils.isEmpty(backImagePath)){
            toast("身份证背面照不能为空")
            return
        }

        if(TextUtils.isEmpty(frontImageUrl) || TextUtils.isEmpty(backImageUrl)) {
            presenter.uploadImage(frontImagePath, object : UploadProgressListener {
                override fun onProgress(bytesWritten: Long, contentLength: Long) {

                }
            })
        }else{
            toNextVerfiy()
        }
    }

    override val layoutId: Int
        get() = R.layout.aty_idcard

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
        mViewBinding.root.toolBar.tvTitle.text = "身份认证"

        var it = intent
        if(it.hasExtra(VERFIY_TYPE)){
            type = it.getIntExtra(VERFIY_TYPE, TO_TEACHER)
        }


    }

    override fun initPath(path:String){
        if(clickType == FRONT) {
            frontImagePath = path
            frontImageUrl = ""
            Glide.with(this).load(path).centerCrop().into(mViewBinding.ivImageFront)
        }else{
            backImagePath = path
            backImageUrl = ""
            Glide.with(this).load(path).centerCrop().into(mViewBinding.ivImageBack)
        }
    }
}