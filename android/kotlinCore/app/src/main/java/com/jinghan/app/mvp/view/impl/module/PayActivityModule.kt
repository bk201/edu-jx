package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IPayActivityPresenter
import com.jinghan.app.mvp.presenter.impl.PayActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/3/16 20:37
 * @mail lzr319@163.com
 */
@Module
abstract class PayActivityModule{
    @ActivityScoped
    @Binds
    abstract fun payActivityPresenter(mPayActivityPresenter: PayActivityPresenter) : IPayActivityPresenter
}