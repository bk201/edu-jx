package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/1/22    下午9:50
 * @mail lzr319@163.com
 */
interface IRegisterActivityView : BaseView{

    fun verfiyCodeResult(result:Boolean)

}