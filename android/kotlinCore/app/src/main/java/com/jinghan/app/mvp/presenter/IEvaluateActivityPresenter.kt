package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IEvaluateActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/3/16 16:12
 * @mail lzr319@163.com
 */
abstract class IEvaluateActivityPresenter : BaseLifecyclePresenter<IEvaluateActivityView,ActivityEvent>(){

    abstract fun evaluate(content:String,pics:ArrayList<String>,questionNo:String)

}