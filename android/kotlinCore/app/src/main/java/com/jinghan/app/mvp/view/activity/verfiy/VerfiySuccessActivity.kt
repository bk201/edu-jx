package com.jinghan.app.mvp.view.activity.verfiy

import android.content.Intent
import android.view.View
import com.jinghan.app.mvp.view.activity.UserInfoActivity
import com.jinghan.core.databinding.AtyVerfiySuccessBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R

/**
 * @author liuzeren
 * @time 2018/1/29    下午8:33
 * @mail lzr319@163.com
 */
class VerfiySuccessActivity constructor(override val layoutId: Int = R.layout.aty_verfiy_success)
    : BaseActivity<AtyVerfiySuccessBinding>(), View.OnClickListener{
    override fun onClick(v: View?) {
        finish()
        startActivity(Intent(this, UserInfoActivity::class.java))
    }

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
    }

    override fun initData() {
    }

    override fun initPresenter() {
    }

}