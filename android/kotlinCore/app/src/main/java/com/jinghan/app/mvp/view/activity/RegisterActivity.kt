package com.jinghan.app.mvp.view.activity

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.jinghan.app.mvp.presenter.IRegisterActivityPresenter
import com.jinghan.app.mvp.view.impl.view.IRegisterActivityView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyRegisterBinding
import com.jinghan.core.dependencies.aspectj.annotation.SingleClick
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * 注册界面
 * @author liuzeren
 * @time 2018/1/22    下午9:13
 * @mail lzr319@163.com
 */
class RegisterActivity(override val layoutId: Int = R.layout.aty_register) : BaseActivity<AtyRegisterBinding>()
        , View.OnClickListener,IRegisterActivityView {

    private var type = TYPE_REGISTER

    companion object {
        val TYPE = "type"

        /**注册*/
        val TYPE_REGISTER = "register"

        /**忘记密码*/
        val TYPE_FORGET = "forget"
    }

    /**获取短信验证码结果*/
    override fun verfiyCodeResult(result: Boolean) {
        if(result){
            presenter.timeDown(object:IRegisterActivityPresenter.OnCountDownTimerListenter{
                override fun onTick(time: Long) {
                    mViewBinding.tvVerfiyCode.text = getString(R.string.verfiy_time,time.toString())
                }

                override fun onFinish() {
                    mViewBinding.tvVerfiyCode.isEnabled = true
                    mViewBinding.tvVerfiyCode.text = getString(R.string.verfiy_code)
                }
            })
        }else{
            mViewBinding.tvVerfiyCode.isEnabled = true
        }
    }

    @Inject
    protected lateinit var presenter:IRegisterActivityPresenter

    @SingleClick
    override fun onClick(v: View) {
        when(v.id){
            R.id.btnRegister-> {
                when (type) {
                    TYPE_REGISTER -> presenter.register(mViewBinding.etPhone.text.toString()
                            , mViewBinding.etPwd.text.toString(), mViewBinding.etCode.text.toString())
                    else -> presenter.forget(mViewBinding.etPhone.text.toString()
                            , mViewBinding.etPwd.text.toString(), mViewBinding.etCode.text.toString())
                }
            }
            else->{
                mViewBinding.tvVerfiyCode.isEnabled = false

                when(type){
                    TYPE_REGISTER ->presenter.getVerfiyCode(mViewBinding.etPhone.text.toString())
                    else->presenter.getForgetPsdVerfiyCode(mViewBinding.etPhone.text.toString())
                }

            }
        }
    }

    override fun initViewsAndListener() {

        if(intent.hasExtra(TYPE)){
            type = intent.getStringExtra(TYPE)
        }

        when(type){
            TYPE_REGISTER->mViewBinding.btnRegister.text="注册"
            else->mViewBinding.btnRegister.text="找回密码"
        }

        mViewBinding.onClick = this
        mViewBinding.etPhone.addTextChangedListener(object:TextWatcher{

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                mViewBinding.tvVerfiyCode.isEnabled = !TextUtils.isEmpty(mViewBinding.etPhone.text)
                check()
            }
        })

        mViewBinding.etCode.addTextChangedListener(object:TextWatcher{

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                check()
            }
        })

        mViewBinding.etPwd.addTextChangedListener(object:TextWatcher{

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                check()
            }
        })
    }

    private fun check(){
        mViewBinding.btnRegister.isEnabled = !TextUtils.isEmpty(mViewBinding.etPhone.text)
                && !TextUtils.isEmpty(mViewBinding.etCode.text)
                && !TextUtils.isEmpty(mViewBinding.etPwd.text)
    }

    override fun initData() {
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun initToolbar() {
        super.initToolbar()

        mViewBinding.root.toolBar.setBackgroundColor(resources.getColor(R.color.whiteColor))
        mViewBinding.root.toolBar.setNavigationIcon(R.drawable.vc_back)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}