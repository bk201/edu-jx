package com.jinghan.app.mvp.model.bean

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable

/**
 * @author liuzeren
 * @time 2018/2/11    下午2:55
 * @mail lzr319@163.com
 */
@SuppressLint("ParcelCreator")
data class OrderInfo(var courseCount:Int,
                     var courseName:String?,
                     var courseTime:String?,
                     var goodsName:String?,
                     var goodsNum:Int,
                     var id:Int,
                     var introduce:String?,
                     var orderId:Int,
                     var orderNo:String?,
                     var pics:String?,
                     var price:Int,
                     var teacher:String?) : Parcelable{


    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(courseCount)
        parcel.writeString(courseName)
        parcel.writeString(courseTime)
        parcel.writeString(goodsName)
        parcel.writeInt(goodsNum)
        parcel.writeInt(id)
        parcel.writeString(introduce)
        parcel.writeInt(orderId)
        parcel.writeString(orderNo)
        parcel.writeString(pics)
        parcel.writeInt(price)
        parcel.writeString(teacher)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderInfo> {
        override fun createFromParcel(parcel: Parcel): OrderInfo {
            return OrderInfo(parcel)
        }

        override fun newArray(size: Int): Array<OrderInfo?> {
            return arrayOfNulls(size)
        }

        /**全部*/
        val ALL = 0

        /**待消费*/
        val IN_CONSUME = 1

        /**已消费*/
        val IS_CONSUME = 2

        /**评价*/
        val EVALUATE = 3

        /**退款*/
        val REFUND =4
    }
}