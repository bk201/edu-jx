package com.jinghan.app.mvp.view.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/3/16 17:22
 * @mail lzr319@163.com
 */
class EvaluateAadpter constructor(var listener:OnItemClickListener?=null) : BaseAdapter<EvaluateAadpter.EvaluateViewHolder,String>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EvaluateViewHolder {
        return EvaluateViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.aty_evaluate_item,parent,false))
    }

    override fun onBindViewHolder(holder: EvaluateViewHolder, position: Int) {
        if(position == itemCount-1){
            holder.update("",position)
        }else {
            super.onBindViewHolder(holder, position)
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount()+1
    }

    interface OnItemClickListener{

        fun onAdd()

        fun onItemClick(index:Int)
    }

    inner class EvaluateViewHolder constructor(itemView:View) :BaseViewHolder<String>(itemView){

        private var ivCover: ImageView

        init {
            ivCover = itemView.findViewById(R.id.ivCover)
        }

        override fun update(t: String) {
            if(TextUtils.isEmpty(t)){
                ivCover.setImageResource(R.drawable.layer_camera)
            }else {
                Glide.with(itemView.context).load(t).centerCrop().into(ivCover)
            }
        }

        fun update(t:String,index:Int){
            update(t)

            itemView.setOnClickListener {
                if(TextUtils.isEmpty(t)){
                    listener?.onAdd()
                }else{
                    listener?.onItemClick(index)
                }
            }
        }
    }
}

