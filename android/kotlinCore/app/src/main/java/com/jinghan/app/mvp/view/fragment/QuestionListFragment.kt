package com.jinghan.app.mvp.view.fragment

import android.support.v7.widget.LinearLayoutManager
import com.jinghan.app.AppContext
import com.jinghan.app.mvp.model.bean.QuestionListInfo
import com.jinghan.app.mvp.presenter.IQuestionListFragmentPresenter
import com.jinghan.app.mvp.view.adapter.QuestionListAdapter
import com.jinghan.app.mvp.view.impl.view.IQuestionListFragmentView
import com.jinghan.core.R
import com.jinghan.core.databinding.FrgQuestionListBinding
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import com.jinghan.core.mvp.view.fragment.BaseRefreshFragment
import java.util.ArrayList
import javax.inject.Inject

/**
 * 问题列表
 * @author liuzeren
 * @time 2018/2/7 13:57
 * @mail lzr319@163.com
 */
@FragmentScoped
class QuestionListFragment @Inject constructor() : BaseRefreshFragment<FrgQuestionListBinding>(),IQuestionListFragmentView{

    private val status = QuestionListInfo.ALL

    override fun onResult(isRefresh: Boolean, questions: ArrayList<QuestionListInfo>) {
        stopRefresh()
        if(isRefresh){
            adapter.setDataSource(questions)
        }else{
            adapter.appendData(questions)
        }
    }

    /**数据列表类型*/
    private var type:Int = WORLD

    companion object {

        val TYPE = "city_type"

        /**全球*/
        val WORLD = 1

        /**同城*/
        val CITY = 2
    }

    private lateinit var adapter: QuestionListAdapter

    @Inject
    protected lateinit var presenter:IQuestionListFragmentPresenter

    override val layoutId: Int
        get() = R.layout.frg_question_list

    override fun initViews() {
        super.initViews()

        swipeToLoadLayout = binding.swipeToLoadLayout
        swipeToLoadLayout?.setOnRefreshListener(this)
        swipeToLoadLayout?.setOnLoadMoreListener(this)
    }

    override fun initData() {
        binding.swipeTarget.layoutManager = LinearLayoutManager(context)

        if(arguments?.containsKey(TYPE) == true){
            type = arguments.getInt(TYPE, WORLD)
        }

        if(type == WORLD){
            adapter = QuestionListAdapter(1)
        }else {
            adapter = QuestionListAdapter()
        }
        binding.swipeTarget.adapter = adapter

        when(type) {
            WORLD -> presenter.worldQuestionList(status,true,adapter.itemCount>0)
            CITY -> presenter.cityQuestionList(status,true,adapter.itemCount>0
                    ,AppContext.instance.location?.cityCode
                    ,AppContext.instance.location?.latitude
                    ,AppContext.instance.location?.longitude)
        }
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onRefresh() {
        when(type) {
            WORLD -> presenter.worldQuestionList(status, true, adapter.itemCount > 0)
            CITY -> presenter.cityQuestionList(status, true, adapter.itemCount > 0
                    , AppContext.instance.location?.cityCode
                    , AppContext.instance.location?.latitude
                    , AppContext.instance.location?.longitude)
        }
    }

    override fun onLoadMore() {
        super.onLoadMore()

        when(type) {
            WORLD -> presenter.worldQuestionList(status, false, adapter.itemCount > 0)
            CITY -> presenter.cityQuestionList(status, false, adapter.itemCount > 0
                    , AppContext.instance.location?.cityCode
                    , AppContext.instance.location?.latitude
                    , AppContext.instance.location?.longitude)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
    }
}