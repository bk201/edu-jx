package com.jinghan.app.mvp.view.impl.api

import com.jinghan.app.mvp.model.bean.CompanyInfo
import com.jinghan.app.mvp.model.bean.StudentInfo
import com.jinghan.app.mvp.model.bean.TeacherInfo
import com.jinghan.app.mvp.model.response.*
import com.jinghan.app.mvp.presenter.impl.VerfiyPresenter
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import io.reactivex.Observable
import retrofit2.http.Body

/**
 * @author liuzeren
 * @time 2018/1/22    下午10:22
 * @mail lzr319@163.com
 */
interface UserService{

    /**@param type 0注册，1找回密码*/
    @GET("mobile/captcha")
    fun getVerfiyCode(@Query("mobile") mobile:String
                      ,@Query("type") type: Int):Observable<BaseResponse>

    @POST("register")
    fun register(@Query("mobile") mobile:String
                 ,@Query("password") password: String
                 ,@Query("code")code:String):Observable<RegisterResponse>

    @POST("findpassword")
    fun findPassword(@Query("mobile") mobile:String
                 ,@Query("password") password: String
                 ,@Query("code")code:String):Observable<RegisterResponse>

    @POST("login")
    fun login(@Query("mobile") mobile:String,@Query("password") password:String):Observable<LoginResponse>

    @GET("user/get-user-info")
    fun getUserInfo(@Query("token") token:String) : Observable<UserInfoResponse>

    /**申请成为机构*/
    @POST("user/apply-institution")
    fun verfiyCompany(@Body companyInfo: VerfiyPresenter.CompanyVerfiyInfo) : Observable<BaseResponse>

    /**申请成为老师*/
    @POST("user/apply-teacher")
    fun verfiyTeacher(@Body teacherInfo: VerfiyPresenter.TeacherVerfiyInfo) : Observable<BaseResponse>

    @GET("query-local-question")
    fun orderList(@Query("status") status:Int,@Query("pageNo") pageNo:Int
                         ,@Query("pageSize") pageSize: Int):Observable<OrderListResponse>

    @GET("query-local-question")
    fun replayList(@Query("status") status:Int,@Query("pageNo") pageNo:Int
                  ,@Query("pageSize") pageSize: Int):Observable<ReplayListResponse>

    @GET("question/query-question")
    fun askList(@Query("status") status:Int,@Query("pageNo") pageNo:Int
                  ,@Query("pageSize") pageSize: Int):Observable<AskListResponse>

    fun companyLocalList(@Query("keyword") keyword:String
                    ,@Query("latitude") latitude:Double?=0.0
                    ,@Query("longitude") longitude:Double?=0.0
                    ,@Query("cityCode") cityCode:String?="",@Query("pageNo") pageNo:Int
                         ,@Query("pageSize") pageSize: Int):Observable<CompanyListResponse>


    @POST("user/edit-user")
    fun editStudentInfo(@Body studentInfo: StudentInfo?):Observable<BaseResponse>


    @GET("user/get-user")
    fun getStudentInfo():Observable<EditUserInfoResponse>

    @POST("user/edit-teacher")
    fun updateTeacherInfo(@Body teacherInfo : TeacherInfo?):Observable<BaseResponse>


    @GET("user/get-teacher")
    fun getTeacherInfo():Observable<TeacherInfoResponse>

    @POST("user/edit-institution")
    fun updateCompanyInfo(@Body companyInfo: CompanyInfo?):Observable<BaseResponse>


    @GET("user/get-institution")
    fun getCompanyInfo():Observable<CompanyInfoResponse>

    /**机构详情*/
    @GET("look-institution")
    fun getCompanyDetail(@Query("userId") userId:Int):Observable<CompanyDetailResponse>

    /**老师详情*/
    @GET("look-teacher")
    fun getTeacherDetail(@Query("userId") userId:Int):Observable<TeacherDetailResponse>

}