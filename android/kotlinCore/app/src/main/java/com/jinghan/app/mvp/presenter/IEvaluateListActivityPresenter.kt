package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IEvaluateListActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/3/17 11:27
 * @mail lzr319@163.com
 */
abstract class IEvaluateListActivityPresenter : BaseLifecyclePresenter<IEvaluateListActivityView,ActivityEvent>()