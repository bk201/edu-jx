package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.*
import com.jinghan.app.mvp.presenter.impl.*
import com.jinghan.app.mvp.view.fragment.*
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author liuzeren
 * @time 2017/11/10    下午2:03
 * @mail lzr319@163.com
 */
@Module
abstract class HomeActivityModule{

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun homeFragment() : HomeFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun publishFragment() : PublishFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun companyListFragment() : CompanyListFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun myFragment() : MyFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun questionsFragment() : QuestionsFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun questionListFragment() : QuestionListFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun recommendFragment() : RecommendFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun teachersFragment():TeachersFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun teacherListFragment():TeacherListFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun companyFragment():CompanyFragment

    @FragmentScoped
    @Binds
    abstract fun companyListFragmentPresenter(companyListFragmentPresenter:CompanyListFragmentPresenter):ICompanyListFragmentPresenter

    @ActivityScoped
    @Binds
    abstract fun publishFragmentPresenter(publishFragmentPresenter: PublishFragmentPresenter) : IPublishFragmentPresenter

    @FragmentScoped
    @Binds
    abstract fun questionListFragmentPresenter(questionListFragmentPresenter: QuestionListFragmentPresenter): IQuestionListFragmentPresenter

    @ActivityScoped
    @Binds
    abstract fun homeActivityPresenter(homeActivityPresenter: HomeActivityPresenter) : IHomeActivityPresenter

    @ActivityScoped
    @Binds
    abstract fun homeFragmentPresenter(homeFragmentPresenter: HomeFragmentPresenter) : IHomeFragmentPresenter

    @ActivityScoped
    @Binds
    abstract fun fileUploadPresenter(fileUploadPresenter: FileUploadPresenter) : IFileUploadPresenter

    @FragmentScoped
    @Binds
    abstract fun teacherListFragmentPresenter(mTeacherListFragmentPresenter: TeacherListFragmentPresenter): ITeacherListFragmentPresenter

}