package com.jinghan.app.mvp.view.activity.verfiy

import android.content.Intent
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.jinghan.app.mvp.view.activity.BaseChooseImageActivity
import com.jinghan.app.mvp.view.impl.view.IFileUploadView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyEducationBinding
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import kotlinx.android.synthetic.main.view_toolbar.view.*
import java.util.*
import javax.inject.Inject

/**
 * 学位认证
 * @author liuzeren
 * @time 2018/1/28    下午9:04
 * @mail lzr319@163.com
 */
class EducationActivity constructor() : BaseChooseImageActivity<AtyEducationBinding>(), View.OnClickListener, IFileUploadView {
    override fun onUploadResult(imgList: ArrayList<String>) {

    }

    companion object {

        /**学位前图*/
        val EDUCATION_FRONT = "education_front"

        /**学位后图*/
        val EDUCATION_BACK = "education_back"

        /**学位号*/
        val EDUCATION_CODE = "education_code"
    }

    override fun onUploadResult(result: Boolean, image: String) {
        if(result){
            if(TextUtils.isEmpty(frontImageUrl)){
                frontImageUrl = image
                presenter.uploadImage(backImagePath,object : UploadProgressListener {
                    override fun onProgress(bytesWritten: Long, contentLength: Long) {

                    }
                })
            }else{
                backImageUrl = image
                toNextVerfiy()
            }
        }

    }

    private fun toNextVerfiy(){
        var it : Intent? = intent

        it?.putExtra(EDUCATION_FRONT,frontImageUrl)
        it?.putExtra(EDUCATION_BACK,backImageUrl)
        it?.putExtra(EDUCATION_CODE,mViewBinding.etCredit.text.toString())
        it?.setClass(this,AlipayActiviity::class.java)

        startActivity(intent)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnNext -> next()
            R.id.viewIdCardFront -> addImageFront()
            R.id.viewIdCardBack -> addImageBack()
        }
    }

    /**学位证前面图*/
    private lateinit var frontImagePath:String
    /**上传后的学位证前面图*/
    private var frontImageUrl:String = ""

    /**学位证背景图*/
    private lateinit var backImagePath:String
    /**上传后的学位证背面图*/
    private var backImageUrl:String = ""

    /**前面*/
    private var FRONT:Int = 1
    /**背面*/
    private var BACK:Int = 2

    private var clickType:Int = FRONT

    /**添加身份证正面*/
    private fun addImageFront(){
        clickType = FRONT
        dialog?.show()
    }

    /**添加身份证反面*/
    private fun addImageBack(){
        clickType = BACK
        dialog?.show()
    }

    /**下一步*/
    private fun next(){
        if(TextUtils.isEmpty(mViewBinding.etCredit.text.toString())){
            toast(mViewBinding.etCredit.hint.toString())
            return
        }

        if(TextUtils.isEmpty(frontImagePath)){
            toast("学位证正面照不能为空")
            return
        }

        if(TextUtils.isEmpty(backImagePath)){
            toast("学位证背面照不能为空")
            return
        }

        if(TextUtils.isEmpty(frontImageUrl) || TextUtils.isEmpty(backImageUrl)) {
            presenter.uploadImage(frontImagePath, object : UploadProgressListener {
                override fun onProgress(bytesWritten: Long, contentLength: Long) {

                }
            })
        }else{
            toNextVerfiy()
        }
    }

    override val layoutId: Int
        get() = R.layout.aty_education

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
        mViewBinding.root.toolBar.tvTitle.text = "学位认证"

        /*var it = intent
        if(it.hasExtra(VERFIY_TYPE)){
            type = it.getIntExtra(VERFIY_TYPE, TO_TEACHER)
        }*/


    }

    override fun initPath(path:String){
        if(clickType == FRONT) {
            frontImagePath = path
            frontImageUrl = ""
            Glide.with(this).load(path).centerCrop().into(mViewBinding.ivImageFront)
        }else{
            backImagePath = path
            backImageUrl=""
            Glide.with(this).load(path).centerCrop().into(mViewBinding.ivImageBack)
        }
    }
}