package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.ICompanyInfoActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:49
 * @mail lzr319@163.com
 */
abstract class ICompanyInfoActivityPresenter : BaseLifecyclePresenter<ICompanyInfoActivityView,ActivityEvent>(){

    /**请求机构信息*/
    public abstract fun requestCompanyInfo(id:Int)

}