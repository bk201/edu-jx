package com.jinghan.core.mvp.widget.scaleimageview.decoder;

/**
 * @author liuzeren
 * @time 2017/12/26 17:31
 * @mail lzr319@163.com
 */
public interface DecoderFactory<T> {
    /**
     * Produce a new instance of a decoder with type {@link T}.
     *
     * @return a new instance of your decoder.
     */
    T make() throws IllegalAccessException, InstantiationException;
}
