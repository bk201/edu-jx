package com.jinghan.app.mvp.model.response

/**
 * 图片上传响应
 * @author liuzeren
 * @time 2018/1/26    下午7:31
 * @mail lzr319@163.com
 */
class ImageUploadResponse(var value:String) : BaseResponse()