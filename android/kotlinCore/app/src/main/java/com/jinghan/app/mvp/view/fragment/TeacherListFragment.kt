package com.jinghan.app.mvp.view.fragment

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.amap.api.location.AMapLocation
import com.amap.api.location.AMapLocationListener
import com.jinghan.app.AppContext
import com.jinghan.app.mvp.model.bean.TeacherListInfo
import com.jinghan.app.mvp.presenter.ITeacherListFragmentPresenter
import com.jinghan.app.mvp.view.adapter.TeacherListFragmentAdapter
import com.jinghan.app.mvp.view.adapter.decoration.HorizontalDecoration
import com.jinghan.app.mvp.view.impl.view.ITeacherListFragmentView
import com.jinghan.core.R
import com.jinghan.core.databinding.FrgTeacherListBinding
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import com.jinghan.core.mvp.view.fragment.BaseRefreshFragment
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/13    下午2:36
 * @mail lzr319@163.com
 */
@FragmentScoped
class TeacherListFragment @Inject constructor() : BaseRefreshFragment<FrgTeacherListBinding>(),ITeacherListFragmentView,View.OnClickListener{
    override fun onClick(v: View) {
        when(v.id){
            R.id.tvSearch->{initData()}
        }
    }

    /**位置信息*/
    private var location : AMapLocation? = null

    /**定位服务*/
    protected var locationHelper = AppContext.instance.locationHelper

    @Inject
    protected lateinit var presenter:ITeacherListFragmentPresenter

    override fun onResult(isRefresh: Boolean, questions: ArrayList<TeacherListInfo>) {

        stopRefresh()

        if(isRefresh){
            adapter.setDataSource(questions)
        }else{
            adapter.appendData(questions)
        }
    }

    private var type = CITY

    companion object {

        val DEFINE_TYPE = "teacher_type"

        val CITY = 1

        val GLOBAL = 2

    }

    private lateinit var adapter: TeacherListFragmentAdapter

    override val layoutId: Int
        get() = R.layout.frg_teacher_list

    override fun initViews() {
        super.initViews()

        binding.swipeToLoadLayout.setOnRefreshListener(this)
        binding.swipeToLoadLayout.setOnLoadMoreListener(this)

        adapter = TeacherListFragmentAdapter()
        binding.swipeTarget.addItemDecoration(HorizontalDecoration(context,R.color.window_background,resources.getDimensionPixelOffset(R.dimen.right_padding)))
        binding.swipeTarget.layoutManager = LinearLayoutManager(context)
        binding.swipeTarget.adapter = adapter

        type = arguments.getInt(DEFINE_TYPE, CITY)

        locationHelper?.setListener(object : AMapLocationListener {
            override fun onLocationChanged(p0: AMapLocation?) {
                location = p0
            }
        })
        locationHelper?.start()
    }

    override fun initData() {

        when(type){
            CITY -> presenter.cityTeacherList(binding.etKeyword.text.toString(),true,false,location?.cityCode,location?.latitude,location?.longitude)
            else->presenter.worldTeacherList(binding.etKeyword.text.toString(),true,false)
        }
    }

    override fun onRefresh() {
        when(type){
            CITY -> presenter.cityTeacherList(binding.etKeyword.text.toString(),true,adapter.itemCount>0,location?.cityCode,location?.latitude,location?.longitude)
            else->presenter.worldTeacherList(binding.etKeyword.text.toString(),true,adapter.itemCount>0)
        }
    }

    override fun onLoadMore() {
        super.onLoadMore()

        when(type){
            CITY -> presenter.cityTeacherList(binding.etKeyword.text.toString(),false,adapter.itemCount>0,location?.cityCode,location?.latitude,location?.longitude)
            else->presenter.worldTeacherList(binding.etKeyword.text.toString(),false,adapter.itemCount>0)
        }
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
    }

}