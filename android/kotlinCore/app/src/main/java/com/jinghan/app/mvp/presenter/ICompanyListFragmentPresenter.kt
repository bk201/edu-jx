package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.ICompanyListFragmentView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.FragmentEvent

/**
 * @author liuzeren
 * @time 2018/2/11    下午5:24
 * @mail lzr319@163.com
 */
abstract class ICompanyListFragmentPresenter : BaseLifecyclePresenter<ICompanyListFragmentView,FragmentEvent>(){

    /**
     * 全球问题列表
     * @param status 状态 status 字段说明, 0:绿灯,待解答 1:黄灯,有人在解答 2:红灯 ,已经解答 , -1 代表所有问题
     * @param isRefresh 是否为刷新
     * @param hasData 当前列表中是否已经存在数据
     * */
    abstract fun worldCompanyList(keyWord:String,isRefresh:Boolean,hasData:Boolean)

    /**
     * 同城问题列表
     * @param status 状态 status 字段说明, 0:绿灯,待解答 1:黄灯,有人在解答 2:红灯 ,已经解答 , -1 代表所有问题
     * @param isRefresh 是否为刷新
     * @param hasData 当前列表中是否已经存在数据
     * @param cityCode 城市代码
     * @param longitude 经度
     * @param latitude 纬度
     * */
    abstract fun cityCompanyList(keyWord:String,isRefresh:Boolean,hasData:Boolean
                                 ,cityCode:String?="",latitude:Double?=0.0
                                 ,longitude:Double?=0.0)
}