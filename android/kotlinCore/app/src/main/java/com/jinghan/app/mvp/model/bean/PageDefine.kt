package com.jinghan.app.mvp.model.bean

import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import com.jinghan.app.mvp.view.fragment.*
import com.jinghan.core.R
import kotlin.reflect.KClass

/**
 * @author liuzeren
 * @time 2018/1/24    下午9:37
 * @mail lzr319@163.com
 */
enum class PageDefine(var id:String,@StringRes var text:Int,@DrawableRes var img:Int,@LayoutRes var layout:Int,var cls: KClass<out Fragment>){
    HOME("0", R.string.home,R.drawable.st_home,R.layout.fg_home_bottom_bar, QuestionsFragment::class),
    TEACHER("1", R.string.teacher,R.drawable.st_teacher,R.layout.fg_home_bottom_bar,TeachersFragment::class),
    PUBLISH("2", R.string.publish,R.drawable.st_publish,R.layout.fg_home_bottom_bar, PublishFragment::class),
    COMPANY("3", R.string.company,R.drawable.st_company,R.layout.fg_home_bottom_bar,CompanyFragment::class),
    MY("4", R.string.my,R.drawable.st_my,R.layout.fg_home_bottom_bar,MyFragment::class)
}