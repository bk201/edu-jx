package com.jinghan.app.mvp.view.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jinghan.app.mvp.view.adapter.SubjectListAdapter
import com.jinghan.app.mvp.view.adapter.decoration.RightPaddingDecoration
import com.jinghan.core.databinding.AtySubjectListBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import kotlinx.android.synthetic.main.view_toolbar.*
import kotlinx.android.synthetic.main.view_toolbar.view.*

/**
 * @author liuzeren
 * @time 2018/2/2    下午8:56
 * @mail lzr319@163.com
 */
class SubjectListActivity constructor(override val layoutId: Int = R.layout.aty_subject_list): BaseActivity<AtySubjectListBinding>(){

    companion object {
        val SUBJECT = "SUBJECT"
    }

    private lateinit var adapter:SubjectListAdapter

    override fun initViewsAndListener() {
        mViewBinding.root.toolBar.tvTitle.text = "选择科目"

        mViewBinding.recyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        adapter = SubjectListAdapter(object : SubjectListAdapter.SubjectItemClickListener{
            override fun onItemClick(value: String) {
                var intent = Intent()
                intent.putExtra(SUBJECT,value)
                setResult(Activity.RESULT_OK,intent)

                finish()
            }
        })
        mViewBinding.recyclerView.addItemDecoration(RightPaddingDecoration(this,resources.getDimensionPixelOffset(R.dimen.divider)))
        mViewBinding.recyclerView.adapter = adapter
    }

    override fun initData() {

        var intent = getIntent()
        if(intent.hasExtra(SUBJECT)){
            adapter.data = intent.getStringArrayListExtra(SUBJECT)
        }

    }

    override fun initPresenter() {
    }
}