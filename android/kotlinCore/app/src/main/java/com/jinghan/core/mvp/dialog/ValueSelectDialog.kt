package com.jinghan.core.mvp.dialog

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.TextView
import com.jinghan.app.mvp.view.adapter.decoration.HorizontalDecoration
import com.jinghan.app.mvp.view.adapter.decoration.RightPaddingDecoration
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder
import com.jinghan.core.mvp.view.MyRecycleView
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/12    下午3:47
 * @mail lzr319@163.com
 */
class ValueSelectDialog(mContext:Context,var values:ArrayList<String>,var listener:OnItemClickListener) : BaseDialog(mContext,true), View.OnClickListener{

    init {
        initView()
    }

    private lateinit var recyclerView : MyRecycleView
    private lateinit var adapter:ValuesAdapter

    interface OnItemClickListener{
        fun onItemClick(value:String)
    }

    fun initView(){
        setContentView(R.layout.dialog_value_select)
        findViewById<TextView>(R.id.tvCancel).setOnClickListener(this)
        recyclerView = findViewById(R.id.recyclerView)

        adapter = ValuesAdapter(listener)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(HorizontalDecoration(context,R.color.window_background,context.resources.getDimensionPixelOffset(R.dimen.divider)))
        recyclerView.adapter = adapter

        adapter.setDataSource(values)

        layoutParams?.width = android.view.ViewGroup.LayoutParams.MATCH_PARENT
        layoutParams?.gravity = Gravity.BOTTOM
    }

    override fun onClick(v: View) {
        dismiss()
    }

    inner class ValuesAdapter(var listener:OnItemClickListener) : BaseAdapter<ValuesAdapter.ValuesViewHolder,String>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ValuesViewHolder {
            return ValuesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.dlg_value_select_item,parent,false))
        }


        inner class ValuesViewHolder(itemView:View) : BaseViewHolder<String>(itemView){

            private lateinit var tvItem:TextView

            init {
                tvItem = itemView.findViewById(R.id.tvItem)
            }

            override fun update(t: String) {
                tvItem.text = t

                tvItem.setOnClickListener(object:View.OnClickListener{
                    override fun onClick(v: View) {
                        listener?.onItemClick(t)
                        dismiss()
                    }
                })
            }
        }
    }
}