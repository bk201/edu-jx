package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.ITeacherInfoActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:08
 * @mail lzr319@163.com
 */
abstract class ITeacherInfoActivityPresenter : BaseLifecyclePresenter<ITeacherInfoActivityView,ActivityEvent>(){

    public abstract fun requestTeacherInfo(teacherId:Int)

}