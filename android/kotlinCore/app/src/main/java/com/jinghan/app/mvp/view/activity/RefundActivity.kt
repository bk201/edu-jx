package com.jinghan.app.mvp.view.activity

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.jinghan.app.mvp.presenter.IRefundActivityPresenter
import com.jinghan.app.mvp.view.impl.view.IRefundActivityView
import com.jinghan.core.databinding.AtyRefundBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * 退款
 * @author liuzeren
 * @time 2018/1/28    下午8:50
 * @mail lzr319@163.com
 */
class RefundActivity constructor(override val layoutId: Int = R.layout.aty_refund) : BaseActivity<AtyRefundBinding>(),View.OnClickListener,IRefundActivityView{

    private var orderNo:String = ""
    private var price:Int = 0

    companion object {
        val QUESTION_NO = "QUESTION_NO"
        val PRICE = "PRICE"

        fun toRefund(context:Context,orderNo:String,price:Int){
            var intent = Intent(context,RefundActivity::class.java)
            intent.putExtra(QUESTION_NO,orderNo)
            intent.putExtra(PRICE,price)
            context.startActivity(intent)
        }
    }

    @Inject
    protected lateinit var presenter:IRefundActivityPresenter

    override fun onClick(v: View) {
        presenter.refund(null,orderNo,mViewBinding.etReason.text.toString() )
    }

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
        mViewBinding.root.toolBar.tvTitle.text = "订单退款"

        if(intent.hasExtra(QUESTION_NO)){
            orderNo = intent.getStringExtra(QUESTION_NO)
        }

        if(intent.hasExtra(PRICE)){
            price = intent.getIntExtra(PRICE,0)
        }

        mViewBinding.tvMoney.text = getString(R.string.money,price)
        mViewBinding.tvOrderId.text = getString(R.string.service_order_no,orderNo)

        mViewBinding.etReason.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                mViewBinding.btnCommit.isEnabled = !TextUtils.isEmpty(mViewBinding.etReason.text.toString())
            }
        })
    }

    override fun initData() {

    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}