package com.jinghan.app.mvp.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.jh.sdk.dependencies.glide.transformations.RoundedCornersTransformation
import com.jinghan.app.mvp.model.bean.AskInfo
import com.jinghan.app.mvp.model.bean.ReplayInfo
import com.jinghan.app.mvp.view.activity.AnswerQuestionActivity
import com.jinghan.app.mvp.view.activity.EvaluateActivity
import com.jinghan.app.mvp.view.activity.QuestionInfoActivity
import com.jinghan.app.mvp.view.activity.RefundActivity
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/11    下午4:07
 * @mail lzr319@163.com
 */
class ReplayListAdapter : BaseAdapter<ReplayListAdapter.ReplayListViewHolder,ReplayInfo>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReplayListViewHolder {
        return ReplayListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.frg_replay_list_item,parent,false))
    }

    inner class ReplayListViewHolder constructor(itemView: View) : BaseViewHolder<ReplayInfo>(itemView){
        var tvOrderId: TextView
        var tvContent: TextView
        var ivCover: ImageView
        var tvMoney: TextView

        /**立即解答*/
        private var tvAnswer: TextView

        /**联系老师*/
        private var tvContact: TextView

        /**查看评价*/
        private var tvLookEvaluate: TextView

        /**同意退款*/
        private var tvTk: TextView

        /**拒绝退款*/
        private var tvCancelRefund: TextView


        init {
            tvOrderId = itemView.findViewById(R.id.tvOrderId)
            tvContent = itemView.findViewById(R.id.tvContent)
            ivCover = itemView.findViewById(R.id.ivCover)
            tvMoney = itemView.findViewById(R.id.tvMoney)

            tvAnswer = itemView.findViewById(R.id.tvAnswer)
            tvContact = itemView.findViewById(R.id.tvContact)
            tvLookEvaluate = itemView.findViewById(R.id.tvLookEvaluate)
            tvTk = itemView.findViewById(R.id.tvTk)
            tvCancelRefund = itemView.findViewById(R.id.tvCancelRefund)
        }


        override fun update(t: ReplayInfo) {
            tvContent.text = t.description
            tvMoney.text = itemView.context.getString(R.string.money,t.money)
            tvOrderId.text = itemView.context.getString(R.string.service_order_no,t.questionNo)

            if(null != t.pictures && t.pictures?.size!!>0){
                Glide.with(itemView.context).load(t.pictures?.get(0)).bitmapTransform(RoundedCornersTransformation(itemView.context,5), CenterCrop(itemView.context)).into(ivCover)
            }

            itemView.setOnClickListener{
                var intent = Intent(itemView.context, QuestionInfoActivity::class.java)
                intent.putExtra(QuestionInfoActivity.QUESTION_NO,t.questionNo)

                itemView.context.startActivity(intent)
            }

            if(t.canAnswer) {
                tvAnswer.visibility = View.VISIBLE
            }else{
                tvAnswer.visibility = View.GONE
            }

            if(t.canContact){
                tvContact.visibility = View.VISIBLE
            }else{
                tvContact.visibility = View.GONE
            }

            if(t.canLookEvaluate){
                tvLookEvaluate.visibility = View.VISIBLE
            }else{
                tvLookEvaluate.visibility = View.GONE
            }

            if(t.canRefund){
                tvCancelRefund.visibility = View.VISIBLE
                tvTk.visibility = View.VISIBLE
            }else{
                tvCancelRefund.visibility = View.GONE
                tvTk.visibility = View.GONE
            }

            tvAnswer.setOnClickListener {
                //解题
            }

            tvContact.setOnClickListener {
                //联系老师
            }

            tvLookEvaluate.setOnClickListener {
                //查看评价
            }

            tvTk.setOnClickListener {
                //同意退款
            }

            tvCancelRefund.setOnClickListener {
                //拒绝退款
            }
        }
    }
}