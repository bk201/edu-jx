package com.jinghan.app.mvp.presenter

import android.location.Location
import com.jinghan.app.mvp.view.impl.view.IPublishFragmentView
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.FragmentEvent
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/1/26    下午8:37
 * @mail lzr319@163.com
 */
abstract class IPublishFragmentPresenter : BaseLifecyclePresenter<IPublishFragmentView,FragmentEvent>(){

    /**上传图片*/
    abstract fun uploadImage(imagePath:String,mUploadListener: UploadProgressListener)

    abstract fun uploadImage(vararg path:String)

    /**发布问题*/
    abstract fun publishQuestion(address:String?,
    city:String?,
    cityCode:String?,
    country:String?,
    description:String,
    latitude:Double?,
    longitude:Double?,
    money:Int,
    pics:ArrayList<String>,
    region:String?,
    regionCode:String?,
    state:String?,
    type:String)

    /**根据经纬度信息获取详细地址信息*/
    abstract fun reqAddress(location:Location)

    /**读取价格*/
    abstract fun readMoney()

    abstract fun questionList()

}