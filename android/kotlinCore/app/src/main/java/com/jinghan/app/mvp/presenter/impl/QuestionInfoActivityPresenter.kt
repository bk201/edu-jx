package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.model.response.QuestionInfoResponse
import com.jinghan.app.mvp.presenter.IQuestionInfoActivityPresenter
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/28    下午2:48
 * @mail lzr319@163.com
 */
class QuestionInfoActivityPresenter @Inject constructor() : IQuestionInfoActivityPresenter(){
    override fun catchQuestion(questionNo: String) {
        mView?.let { it.showLoading("抢单中~") }

        mOkHttp.retrofit.builder(QuestionService::class.java).catchQuestion(questionNo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let {
                            it.toast(R.string.request_failure)
                            it.catchResult(false)
                        }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{it.catchResult(true)}
                        }else{
                            mView?.let {
                                it.toast(response.msg)
                                it.catchResult(false)
                            }
                        }
                    }
                })
    }

    override fun reqQuestionDetail(questionNo: String) {
        mView?.let { it.showLoading("正在获取问题详情信息~") }

        mOkHttp.retrofit.builder(QuestionService::class.java).questionInfo(questionNo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<QuestionInfoResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let {
                            it.toast(R.string.request_failure)
                            it.close()
                        }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : QuestionInfoResponse) {
                        if(response.isSuccess){
                            mView?.let{it.onDetail(response.value)}
                        }else{
                            mView?.let {
                                it.toast(response.msg)
                                it.close()
                            }
                        }
                    }
                })
    }

}