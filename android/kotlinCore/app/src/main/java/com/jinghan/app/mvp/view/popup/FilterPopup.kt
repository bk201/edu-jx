package com.jinghan.app.mvp.view.popup

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupWindow
import android.widget.Toast
import com.jinghan.app.mvp.model.response.SubjectListResponse
import com.jinghan.app.mvp.view.adapter.SubjectList1Adapter
import com.jinghan.app.mvp.view.adapter.decoration.RightPaddingDecoration
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.core.R
import com.jinghan.core.dependencies.http.OkHttp
import com.jinghan.core.mvp.view.MyRecycleView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers

/**
 * @author liuzeren
 * @time 2018/2/27    上午9:23
 * @mail lzr319@163.com
 */
class FilterPopup constructor(var context:Context) : PopupWindow(){

    lateinit var recyclerView:MyRecycleView
    lateinit var adapter: SubjectList1Adapter

    lateinit var btnOk:Button

    init {
        initView()
        initData()
    }


    private fun initView(){
        contentView = LayoutInflater.from(context).inflate(R.layout.popup_filter,null)
        recyclerView = contentView.findViewById(R.id.recyclerView)

        adapter = SubjectList1Adapter()
        var manager:GridLayoutManager = GridLayoutManager(context,5)
        recyclerView.layoutManager = manager
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(RightPaddingDecoration(context,context.resources.getDimensionPixelOffset(R.dimen.padding)))

        btnOk = contentView.findViewById(R.id.btnOk)
        btnOk.setOnClickListener {
            dismiss()
        }

        width = ViewGroup.LayoutParams.MATCH_PARENT
        height = ViewGroup.LayoutParams.WRAP_CONTENT

        isOutsideTouchable = true
        isTouchable = true
    }

    private fun initData(){
        OkHttp.getInstance(context).retrofit.builder(QuestionService::class.java).subjectList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: DefaultObserver<SubjectListResponse>(){
                    override fun onError(e: Throwable) {
                        Toast.makeText(context,R.string.request_failure,Toast.LENGTH_LONG).show()
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(response : SubjectListResponse) {
                        if(response.isSuccess){
                            adapter.setDataSource(response.values)
                        }else{
                            Toast.makeText(context,response.msg,Toast.LENGTH_LONG).show()
                        }
                    }
                })
    }
}