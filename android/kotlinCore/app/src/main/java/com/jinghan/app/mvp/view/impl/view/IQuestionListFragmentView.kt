package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.QuestionListInfo
import com.jinghan.core.mvp.view.impl.view.BaseView
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/10    下午9:14
 * @mail lzr319@163.com
 */
interface IQuestionListFragmentView : BaseView{

    fun onResult(isRefresh:Boolean,questions:ArrayList<QuestionListInfo>)

}