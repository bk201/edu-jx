package com.jinghan.core.mvp.widget

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * @author liuzeren
 * @time 2018/2/7 13:51
 * @mail lzr319@163.com
 */
class NoScrollViewPager @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null)  : ViewPager(context,attrs){

    private var isCanScroll = true

    /**
     * 设置其是否能滑动换页
     * @param isCanScroll false 不能换页， true 可以滑动换页
     */
    fun setScanScroll(isCanScroll: Boolean) {
        this.isCanScroll = isCanScroll
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return isCanScroll && super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return isCanScroll && super.onTouchEvent(ev)

    }
}