package com.jinghan.app.mvp.view.activity

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.jinghan.app.global.Constant
import com.jinghan.app.mvp.model.bean.QuestionListInfo
import com.jinghan.app.mvp.presenter.IAnswerQuestionActivityPresenter
import com.jinghan.app.mvp.presenter.IFileUploadActivityPresenter
import com.jinghan.app.mvp.view.adapter.ImageListAdapter
import com.jinghan.app.mvp.view.adapter.decoration.RightPaddingDecoration
import com.jinghan.app.mvp.view.impl.view.IAnswerQuestionActivityView
import com.jinghan.app.mvp.view.impl.view.IFileUploadView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyAnswerQuestionBinding
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import com.jinghan.core.helper.ImageUtil
import com.jinghan.core.mvp.dialog.ChooseImageDialog
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.view_toolbar.view.*
import java.util.*
import javax.inject.Inject

/**
 * 解题
 * @author liuzeren
 * @time 2018/3/11    下午9:19
 * @mail lzr319@163.com
 */
class AnswerQuestionActivity : BaseActivity<AtyAnswerQuestionBinding>(),View.OnClickListener, IAnswerQuestionActivityView, IFileUploadView {
    override fun onUploadResult(imgList: ArrayList<String>) {

    }

    override fun onUploadResult(result: Boolean, image: String) {
        if (result) {
            adapter.appendData(image)
            check()
        }
    }

    @Inject
    protected lateinit var presenter: IAnswerQuestionActivityPresenter

    @Inject
    protected lateinit var filePresenter: IFileUploadActivityPresenter

    private lateinit var adapter: ImageListAdapter

    var dialog: ChooseImageDialog? = null

    companion object {
        val QUESTION = "QuestionListInfo"
    }

    override fun submitAnswer() {
        //问题解答成功
        mViewBinding.btnAnswer.isEnabled = false
        mViewBinding.btnAnswerLater.isEnabled = false
        toast("答案提交成功")
        close()
    }

    private fun submit() {
        //解答
        if(adapter.itemCount==0){
            toast("至少提交一张解题照片")
            return
        }

        presenter.answer(mViewBinding.etQuestion.text.toString(), adapter.data, mViewBinding.question!!.questionNo!! )
    }

    override val layoutId: Int
        get() = R.layout.aty_answer_question

    private fun check(){
        mViewBinding.btnAnswer.isEnabled = !TextUtils.isEmpty(mViewBinding.etQuestion.text.toString()) && adapter.itemCount>0
    }


    override fun initViewsAndListener() {

        if(intent.hasExtra(QUESTION)) {
            mViewBinding.question = intent.getParcelableExtra<QuestionListInfo>(QUESTION)
            mViewBinding.etMoney.text = getString(R.string.money,mViewBinding.question?.money)
        }

        mViewBinding.root.toolBar.tvTitle.text = "立即解答"
        mViewBinding.onClick = this
        mViewBinding.etQuestion.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                check()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        adapter = ImageListAdapter(object : ImageListAdapter.OnItemClickListener{
            override fun onClick(url: String) {
                this@AnswerQuestionActivity?.let {
                    dialog = ChooseImageDialog(it,true,object : DialogInterface.OnCancelListener{
                        override fun onCancel(dialog: DialogInterface?) {

                        }
                    })
                    dialog?.show()
                }
            }
        })
        var manager = GridLayoutManager(this,4)
        mViewBinding.recyclerView.layoutManager = manager
        mViewBinding.recyclerView.adapter = adapter
        mViewBinding.recyclerView.addItemDecoration(RightPaddingDecoration(this,this.resources.getDimensionPixelOffset(R.dimen.right_padding)))

        if(null == mViewBinding.question){
            finish()
        }
    }

    override fun initData() {
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)

        filePresenter.takeView(this)
        filePresenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
        filePresenter.dropView()
    }

    override fun onPermissionGranted(reqCode: Int) {
        super.onPermissionGranted(reqCode)

        dialog?.onPermissionGranted(reqCode)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.viewDetail->{
                //查看问题详情
                if(null != mViewBinding.question){
                    var intent = Intent(this,QuestionInfoActivity::class.java)
                    intent.putExtra(QuestionInfoActivity.QUESTION_NO,mViewBinding.question?.questionNo)
                    startActivity(intent)
                }
            }
            R.id.btnAnswer->{
                //立即解答
                submit()
            }
            R.id.btnAnswerLater->{
                //稍后解答
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            ChooseImageDialog.GET_IMAGE_FROM_PHONE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                        if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(this, it.data), 2, path)) {
                            filePresenter.uploadImage(path,object : UploadProgressListener {
                                override fun onProgress(bytesWritten: Long, contentLength: Long) {
                                }
                            })
                        }
                    }
                }
            }
            ChooseImageDialog.GET_IMAGE_BY_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                    if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(this, ChooseImageDialog.imageUriFromCamera), 2, path)) {
                        filePresenter.uploadImage(path,object : UploadProgressListener {
                            override fun onProgress(bytesWritten: Long, contentLength: Long) {
                            }
                        })
                    }
                }
            }
        }
    }
}