package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.bean.EvaluateInfo
import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.model.response.TeacherInfoResponse
import com.jinghan.app.mvp.presenter.IEvaluateActivityPresenter
import com.jinghan.app.mvp.view.impl.api.OrderService
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/3/16 16:13
 * @mail lzr319@163.com
 */
class EvaluateActivityPresenter @Inject constructor():IEvaluateActivityPresenter(){
    override fun evaluate(content: String, pics: ArrayList<String>, questionNo: String) {
        mView?.let{it.showLoading("正在提交评价信息...")}

        var mEvaluateInfo = EvaluateInfo(content,pics ,questionNo)

        mOkHttp.retrofit.builder(OrderService::class.java).evaluate(mEvaluateInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{
                                it.toast("评价成功")
                                it.close()
                            }
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }
}