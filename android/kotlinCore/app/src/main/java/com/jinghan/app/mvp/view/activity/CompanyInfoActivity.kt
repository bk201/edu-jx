package com.jinghan.app.mvp.view.activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jinghan.app.mvp.model.bean.CompanyListInfo
import com.jinghan.app.mvp.model.bean.TeacherInfo
import com.jinghan.app.mvp.presenter.ICompanyInfoActivityPresenter
import com.jinghan.app.mvp.view.impl.view.ICompanyInfoActivityView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyCompanyInfoBinding
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * 机构详情
 * @author liuzeren
 * @time 2018/3/11    上午9:15
 * @mail lzr319@163.com
 */
class CompanyInfoActivity : BaseActivity<AtyCompanyInfoBinding>(), ICompanyInfoActivityView ,View.OnClickListener{
    override fun onClick(v: View) {
        when(v.id){
            R.id.btnCall->{
                //联系我们
            }
            R.id.tvComment->{
                //评论
            }
        }
    }

    companion object {
        val ID = "id"
    }

    /**机构Id*/
    private var companyId = 0

    @Inject
    protected lateinit var presenter:ICompanyInfoActivityPresenter

    override fun onDetail(info: CompanyListInfo) {
        mViewBinding.onClick = this
        mViewBinding.info = info

        var times:String = "0"
        if(info.evaluateTime > 99){
            times = "99+"
        }else{
            times = ""+info.evaluateTime
        }

        mViewBinding.tvComment.text = times
    }

    private lateinit var adapter:SubjectAdapter

    override val layoutId: Int
        get() = R.layout.aty_company_info

    override fun initViewsAndListener() {

    }

    override fun initData() {
        if(intent.hasExtra(ID)){
            companyId = intent.getIntExtra(ID,0)
        }

        presenter.requestCompanyInfo(companyId)
    }

    override fun initToolbar() {
        super.initToolbar()

        mViewBinding.root.toolBar.tvTitle.text = "机构详情"
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
    }

    class SubjectAdapter : BaseAdapter<SubjectViewHolder,TeacherInfo>(){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
            return SubjectViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.aty_company_info_item,parent,false))
        }

    }

    class SubjectViewHolder constructor(itemView: View) : BaseViewHolder<TeacherInfo>(itemView){

        /**寒假班*/
        private var tvType:TextView

        /**上课内容*/
        private var tvContent:TextView

        /**上课时间*/
        private var tvTime:TextView

        /**课程价格*/
        private var tvPrice: TextView

        init {
            tvType = itemView.findViewById(R.id.tvType)
            tvContent = itemView.findViewById(R.id.tvType)
            tvTime = itemView.findViewById(R.id.tvType)
            tvPrice = itemView.findViewById(R.id.tvType)
        }

        override fun update(t: TeacherInfo) {

        }
    }
}