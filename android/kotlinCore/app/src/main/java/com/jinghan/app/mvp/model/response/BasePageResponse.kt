package com.jinghan.app.mvp.model.response

/**
 * @author liuzeren
 * @time 2018/2/10    下午9:21
 * @mail lzr319@163.com
 */
open class BasePageResponse(var pageNumber:Int=0,var pageSize:Int=0,var total:Int=0) : BaseResponse(){

    companion object {
        /**定义分页的个数*/
        val PageSize = 10
    }

}