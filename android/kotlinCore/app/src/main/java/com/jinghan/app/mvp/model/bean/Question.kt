package com.jinghan.app.mvp.model.bean

/**
 * 问题信息
 * @author liuzeren
 * @time 2018/1/26    下午7:40
 * @mail lzr319@163.com
 */
data class Question(var addTime:String
                    ,var addTimeLong:Long,var address:String
                    ,var description:String,var id:Long,var latitude:Float,var longitude:Long
                    ,var money:Float,var payStatus:Int,var picture1:String,var picture2:String
                    ,var picture3:String,var picture4:String,var picture5:String,var questionNo:String
                    ,var status:Int,var userId:Long)