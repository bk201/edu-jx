package com.jinghan.app.mvp.view.activity

import android.content.Intent
import android.media.Image
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.jinghan.app.mvp.model.bean.QuestionListInfo
import com.jinghan.app.mvp.presenter.IQuestionInfoActivityPresenter
import com.jinghan.app.mvp.view.adapter.ImageListAdapter1
import com.jinghan.app.mvp.view.adapter.decoration.RightPaddingDecoration
import com.jinghan.app.mvp.view.impl.view.IQuestionInfoActivityView
import com.jinghan.core.databinding.AtyQuestionInfoBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * 问题详情
 * @author liuzeren
 * @time 2018/1/28    下午8:30
 * @mail lzr319@163.com
 */
class QuestionInfoActivity constructor(override val layoutId: Int = R.layout.aty_question_info)
    : BaseActivity<AtyQuestionInfoBinding>(),IQuestionInfoActivityView, View.OnClickListener{
    override fun catchResult(isSuccess: Boolean) {
        /**抢单成功就不能再抢单了*/
        mViewBinding.btnAccept.isEnabled = !isSuccess

        if(isSuccess) {
            var intent = Intent(this, AnswerQuestionActivity::class.java)
            intent.putExtra(AnswerQuestionActivity.QUESTION, mViewBinding.info)
            startActivity(intent)
        }
    }

    var questionNo:String = ""
    lateinit var adapter: ImageListAdapter1

    companion object {
        val QUESTION_NO = "question_no"
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.ivQuestion->{
                ImageViewerActivity.startViewer(this@QuestionInfoActivity,adapter.data,adapter.getSelectedIndex())
            }
            R.id.btnAccept->{
                presenter.catchQuestion(questionNo)
            }
        }
    }

    override fun onDetail(info: QuestionListInfo) {
        mViewBinding.info = info
        mViewBinding.tvSubject.text = getString(R.string.subject_,info.type)
        mViewBinding.tvMoney.text = getString(R.string.subject_money,info.money)
        adapter.setDataSource(info.pictures)
        adapter.setSelected(0)
        mViewBinding.btnAccept.isEnabled = info.status == QuestionListInfo.NOT_REPLAY
    }

    @Inject
    lateinit var presenter:IQuestionInfoActivityPresenter

    override fun initViewsAndListener() {
        mViewBinding.onClick = this

        adapter = ImageListAdapter1(object:ImageListAdapter1.OnItemClickListener{
            override fun onClick(url: String) {
                Glide.with(this@QuestionInfoActivity).load(url).into(mViewBinding.ivQuestion)
            }
        })

        var manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        mViewBinding.recyclerView.layoutManager = manager
        mViewBinding.recyclerView.addItemDecoration(RightPaddingDecoration(this,10))
        mViewBinding.recyclerView.adapter = adapter

        mViewBinding.root.toolBar.tvTitle.text = "问题详情"
    }

    override fun initData() {
        if(intent.hasExtra(QUESTION_NO)){
            questionNo = intent.getStringExtra(QUESTION_NO)
        }

        if(TextUtils.isEmpty(questionNo)){
            finish()
        }

        presenter.reqQuestionDetail(questionNo)
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}