package com.jinghan.app.mvp.model.bean

/**
 * 学生信息
 * @author liuzeren
 * @time 2018/2/13    上午10:58
 * @mail lzr319@163.com
 */
data class StudentInfo(var address:String="",
                       var birthday:String,
                       var city:String?="",
                       var cityCode:String?="",
                       var country:String?="",
                       var introduce:String,
                       var latitude:Double?=0.0,
                       var longitude:Double?=0.0,
                       var mobile:String?="" ,
                       var name:String?="" ,
                       var phase:String ,
                       var region:String?="" ,
                       var regionCode:String?="",
                       var sex:Int?=0,
                       var age:Int = 0,
                       var state:String?=""){
    /**0:保密,1:男,2:女*/
    fun getUserSex():String{
        if(1==sex){return "男"}else if(2==sex){return "女"}else{return "保密"}
    }
}