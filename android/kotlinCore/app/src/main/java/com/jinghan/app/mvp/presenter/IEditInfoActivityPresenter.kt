package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.model.bean.CompanyInfo
import com.jinghan.app.mvp.model.bean.StudentInfo
import com.jinghan.app.mvp.model.bean.TeacherInfo
import com.jinghan.app.mvp.view.impl.view.IEditInfoActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/2/12    下午2:00
 * @mail lzr319@163.com
 */
abstract class IEditInfoActivityPresenter : BaseLifecyclePresenter<IEditInfoActivityView,ActivityEvent>(){

    abstract fun getEditUserInfo()

    abstract fun getTeacherInfo()

    abstract fun getCompanyInfo()

    abstract fun editUserInfo(info: StudentInfo?)

    abstract fun updateTeacherInfo(info:TeacherInfo?)

    abstract fun updateCompanyInfo(info:CompanyInfo?)
}