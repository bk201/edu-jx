package com.jinghan.app.mvp.view.fragment

import android.os.Bundle
import android.widget.RadioGroup
import com.amap.api.location.AMapLocation
import com.jinghan.app.AppContext
import com.jinghan.app.mvp.view.adapter.BaseFragmentAdapter
import com.jinghan.core.R
import com.jinghan.core.databinding.FrgTeachersBinding
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import com.jinghan.core.mvp.view.fragment.BaseFragment
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/13    下午2:29
 * @mail lzr319@163.com
 */
@FragmentScoped
class CompanyFragment @Inject constructor(): BaseFragment<FrgTeachersBinding>(){

    /**定位服务*/
    protected var locationHelper = AppContext.instance.locationHelper

    private var location: AMapLocation? = null
    private lateinit var adapter:BaseFragmentAdapter<CompanyListFragment>

    override val layoutId: Int
        get() = R.layout.frg_teachers

    override fun initData() {
        locationHelper?.setListener { p0 ->
            location = p0
            binding.tvAddress.text = p0?.city?.trimEnd('市')
        }
        locationHelper?.start()

        adapter = BaseFragmentAdapter(childFragmentManager)

        var fragments : ArrayList<CompanyListFragment> = ArrayList()

        var city = CompanyListFragment()
        var bundle = Bundle()
        bundle.putInt(CompanyListFragment.DEFINE_TYPE,CompanyListFragment.CITY)
        city.arguments = bundle
        fragments.add(city)

        var global = CompanyListFragment()
        var bundle1 = Bundle()
        bundle1.putInt(CompanyListFragment.DEFINE_TYPE,CompanyListFragment.GLOBAL)
        global.arguments = bundle1
        fragments.add(global)

        adapter.setFragments(fragments)
        binding.viewPager.adapter = adapter
        binding.viewPager.setScanScroll(false)

        binding.rgType.setOnCheckedChangeListener(object:RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                when(checkedId){
                    R.id.rbCity->binding.viewPager.currentItem = 0
                    else->binding.viewPager.currentItem = 1
                }
            }
        })
    }

    override fun initPresenter() {
    }
}