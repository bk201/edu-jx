package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.TeacherListInfo

/**
 * @author liuzeren
 * @time 2018/3/11    上午10:19
 * @mail lzr319@163.com
 */
data class TeacherDetailResponse(var value:TeacherListInfo) : BaseResponse()