package com.jinghan.app.mvp.view.adapter

import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jinghan.app.mvp.model.bean.QuestionListInfo
import com.jinghan.app.mvp.view.activity.QuestionInfoActivity
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/7 14:00
 * @mail lzr319@163.com
 */
class QuestionListAdapter constructor(var type:Int = 0) : BaseAdapter<BaseViewHolder<QuestionListInfo>, QuestionListInfo>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<QuestionListInfo> {

        when(viewType){
            R.layout.frg_question_list_item->return QuestionListViewHolder(
                    LayoutInflater.from(parent.context).inflate(viewType,parent,false))
            else->return QuestionListViewHolder1(
                    LayoutInflater.from(parent.context).inflate(viewType,parent,false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        if(type == 0){
            return R.layout.frg_question_list_item
        }else{
            return R.layout.frg_question_list_item1
        }
    }

    class QuestionListViewHolder constructor(itemView: View) : BaseViewHolder<QuestionListInfo>(itemView) {

        /**头像*/
        private lateinit var ivHead: ImageView
        /**时间*/
        private lateinit var tvTime : TextView
        /**用户姓名*/
        private lateinit var tvName : TextView
        /**性别*/
        private lateinit var ivSex : ImageView
        /**状态*/
        private lateinit var ivStatus:ImageView
        /**视频解答*/
        private lateinit var tvVideo:TextView
        /**上门解答*/
        private lateinit var tvReplayHome : TextView
        /**题目的第一张图片*/
        private lateinit var ivImage:ImageView
        /**题目价格*/
        private lateinit var tvMoney:TextView
        /**留言备注*/
        private lateinit var tvMemo:TextView
        /**题目解答*/
        private lateinit var tvReplay:TextView

        init {
            ivHead = itemView.findViewById(R.id.ivHead)
            tvTime = itemView.findViewById(R.id.tvTime)
            tvName = itemView.findViewById(R.id.tvName)
            ivSex = itemView.findViewById(R.id.ivSex)
            ivStatus = itemView.findViewById(R.id.ivStatus)
            tvVideo = itemView.findViewById(R.id.tvVideo)
            tvReplayHome = itemView.findViewById(R.id.tvReplayHome)
            ivImage = itemView.findViewById(R.id.ivImage)
            tvMoney = itemView.findViewById(R.id.tvMoney)
            tvMemo = itemView.findViewById(R.id.tvMemo)
            tvReplay = itemView.findViewById(R.id.tvReplay)
        }

        override fun update(t: QuestionListInfo) {

            if(null != t.pictures && t.pictures.size > 0) {
                Glide.with(itemView.context).load(t.pictures[0]).into(ivImage)
            }

            tvTime.text = t.pastTime
            tvMoney.text = Html.fromHtml(itemView.context.getString(R.string.question_money1,t.money))

            when(t.status){
                QuestionListInfo.NOT_REPLAY->ivStatus.setImageResource(R.drawable.sp_no_replay)
                QuestionListInfo.IN_REPLAY->ivStatus.setImageResource(R.drawable.sp_replaing)
                QuestionListInfo.REPLAYIED->ivStatus.setImageResource(R.drawable.sp_replaied)
                else->ivStatus.setImageResource(R.drawable.sp_no_replay)
            }

            tvName.text = t.userName
//            ivSex.setImageResource(R.mipmap.boy)
//            tvMoney.text = itemView.resources.getString(R.string.question_money_limit,t.)

            itemView.setOnClickListener{
                var intent = Intent(itemView.context,QuestionInfoActivity::class.java)
                intent.putExtra(QuestionInfoActivity.QUESTION_NO,t.questionNo)

                itemView.context.startActivity(intent)
            }
        }
    }

    class QuestionListViewHolder1 constructor(itemView: View) : BaseViewHolder<QuestionListInfo>(itemView) {

        /**头像*/
        private lateinit var ivHead: ImageView
        /**时间*/
        private lateinit var tvTime : TextView
        /**用户姓名*/
        private lateinit var tvName : TextView
        /**性别*/
        private lateinit var ivSex : ImageView
        /**状态*/
        private lateinit var ivStatus:ImageView
        /**视频解答*/
        private lateinit var tvAddress:TextView
        /**题目的第一张图片*/
        private lateinit var ivImage:ImageView
        /**题目价格*/
        private lateinit var tvMoney:TextView
        /**留言备注*/
        private lateinit var tvMemo:TextView
        /**题目解答*/
        private lateinit var tvReplay:TextView

        init {
            ivHead = itemView.findViewById(R.id.ivHead)
            tvTime = itemView.findViewById(R.id.tvTime)
            tvName = itemView.findViewById(R.id.tvName)
            ivSex = itemView.findViewById(R.id.ivSex)
            ivStatus = itemView.findViewById(R.id.ivStatus)
            tvAddress = itemView.findViewById(R.id.tvAddress)
            ivImage = itemView.findViewById(R.id.ivImage)
            tvMoney = itemView.findViewById(R.id.tvMoney)
            tvMemo = itemView.findViewById(R.id.tvMemo)
            tvReplay = itemView.findViewById(R.id.tvReplay)
        }

        override fun update(t: QuestionListInfo) {

            if(null != t.pictures && t.pictures.size > 0) {
                Glide.with(itemView.context).load(t.pictures[0]).into(ivImage)
            }

            tvTime.text = t.pastTime
            tvMoney.text = Html.fromHtml(itemView.context.getString(R.string.question_money1,t.money))

            when(t.status){
                QuestionListInfo.NOT_REPLAY->ivStatus.setImageResource(R.drawable.sp_no_replay)
                QuestionListInfo.IN_REPLAY->ivStatus.setImageResource(R.drawable.sp_replaing)
                QuestionListInfo.REPLAYIED->ivStatus.setImageResource(R.drawable.sp_replaied)
                else->ivStatus.setImageResource(R.drawable.sp_no_replay)
            }

            tvName.text = t.userName
            tvAddress.text = t.address
//            ivSex.setImageResource(R.mipmap.boy)
//            tvMoney.text = itemView.resources.getString(R.string.question_money_limit,t.)

            itemView.setOnClickListener{
                var intent = Intent(itemView.context,QuestionInfoActivity::class.java)
                intent.putExtra(QuestionInfoActivity.QUESTION_NO,t.questionNo)

                itemView.context.startActivity(intent)
            }
        }
    }
}