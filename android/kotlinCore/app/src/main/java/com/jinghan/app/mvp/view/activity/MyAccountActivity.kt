package com.jinghan.app.mvp.view.activity

import android.view.View
import com.jinghan.app.mvp.presenter.IMyAccountActivityPresenter
import com.jinghan.app.mvp.view.impl.view.IMyAccountActivityView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyMyaccountBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * Created by xueliang on 2018/4/13.
 */

class MyAccountActivity(override val layoutId: Int = R.layout.aty_myaccount) : BaseActivity<AtyMyaccountBinding>(), IMyAccountActivityView {

    @Inject
    protected lateinit var presenter : IMyAccountActivityPresenter

    override fun initViewsAndListener() {
        mViewBinding.root.toolBar.tvTitle.text = "余额"
        mViewBinding.root.toolBar.right_txt.text = "明细"
        mViewBinding.root.toolBar.right_txt.visibility = View.VISIBLE;
        mViewBinding.root.toolBar.right_txt.setOnClickListener(View.OnClickListener { })
    }

    override fun initData() {

    }

    override fun initPresenter() {

    }
}
