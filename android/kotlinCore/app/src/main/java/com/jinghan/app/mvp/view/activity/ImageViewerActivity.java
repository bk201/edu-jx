package com.jinghan.app.mvp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jinghan.app.mvp.view.adapter.BaseFragmentAdapter;
import com.jinghan.app.mvp.view.fragment.ImageFragment;
import com.jinghan.core.R;
import com.jinghan.core.databinding.AtyImageViewerBinding;
import com.jinghan.core.mvp.view.activity.BaseActivity;

import java.util.ArrayList;

/**
 * @author liuzeren
 * @time 2018/2/13 16:32
 * @mail lzr319@163.com
 */
public class ImageViewerActivity extends BaseActivity<AtyImageViewerBinding> {

    public static final String PATH = "path";
    public static final String INDEX = "index";

    private ArrayList<String> paths = null;
    private ArrayList<ImageFragment> fragments;
    private BaseFragmentAdapter adapter;

    private int index = 0;

    public static void startViewer(Context context,String image){
        ArrayList<String> images = new ArrayList<>(1);
        images.add(image);

        startViewer(context,images,0);
    }

    public static void startViewer(Context context,ArrayList<String> images){
        startViewer(context,images,0);
    }

    public static void startViewer(Context context,ArrayList<String> images,int index){
        Intent intent = new Intent(context,ImageViewerActivity.class);
        intent.putExtra(INDEX,index);
        intent.putExtra(PATH,images);

        context.startActivity(intent);
    }

    @Override
    public void initViewsAndListener() {
        Intent intent = getIntent();

        if(intent.getExtras().containsKey(PATH)) {
            paths = intent.getExtras().getStringArrayList(PATH);
        }

        if(intent.getExtras().containsKey(INDEX)){
            index = intent.getExtras().getInt(INDEX,0);
        }

        if(null == paths || paths.size() == 0){
            finish();
        }

        fragments = new ArrayList<>(paths.size());
        if(index >= paths.size() || index<0){
            index = 0;
        }

        mViewBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ImageViewerActivity.this.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private int mLastPosition;

    public void onPageSelected(int position) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (mViewBinding.viewGuide.getChildCount() > mLastPosition) {
                mViewBinding.viewGuide.getChildAt(mLastPosition).setActivated(false);
            }

            mViewBinding.viewGuide.getChildAt(position).setActivated(true);
        }

        mLastPosition = position;
    }

    @Override
    protected void initData() {
        initIndicators();
        adapter = new BaseFragmentAdapter(getSupportFragmentManager());
        mViewBinding.viewPager.setAdapter(adapter);
        adapter.setFragments(fragments);
        mViewBinding.viewPager.setCurrentItem(index);
        onPageSelected(index);
    }

    private void initIndicators(){

        mViewBinding.viewGuide.removeAllViews();

        Resources res = getResources();
        int size = res.getDimensionPixelOffset(R.dimen.indicator_size);
        int margin = res.getDimensionPixelOffset(R.dimen.indicator_margin);
        for (String path:paths) {
            ImageView indicator = new ImageView(this);
            indicator.setAlpha(180);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(size, size);
            lp.setMargins(margin, 0, 0, 0);
            lp.gravity = Gravity.CENTER;
            indicator.setLayoutParams(lp);
            Drawable drawable = res.getDrawable(R.drawable.selector_indicator);
            indicator.setImageDrawable(drawable);
            mViewBinding.viewGuide.addView(indicator);

            ImageFragment fragment = new ImageFragment();
            Bundle bundle = new Bundle();
            bundle.putString(PATH,path);
            fragment.setArguments(bundle);

            fragments.add(fragment);
        }
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.aty_image_viewer;
    }
}