package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.OrderShowInfo

/**
 * @author liuzeren
 * @time 2018/3/14 13:50
 * @mail lzr319@163.com
 */
class OrderShowInfoListResponse constructor(var values:ArrayList<OrderShowInfo>) : BasePageResponse()