package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.CompanyDetailResponse
import com.jinghan.app.mvp.model.response.QuestionInfoResponse
import com.jinghan.app.mvp.presenter.ICompanyInfoActivityPresenter
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:50
 * @mail lzr319@163.com
 */
class CompanyInfoActivityPresenter @Inject constructor(): ICompanyInfoActivityPresenter(){

    override fun requestCompanyInfo(userId: Int) {
        mView?.let { it.showLoading("正在获取机构详情信息~") }

        mOkHttp.retrofit.builder(UserService::class.java).getCompanyDetail(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<CompanyDetailResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let {
                            it.toast(R.string.request_failure)
                            it.close()
                        }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : CompanyDetailResponse) {
                        if(response.isSuccess){
                            mView?.let{it.onDetail(response.value)}
                        }else{
                            mView?.let {
                                it.toast(response.msg)
                                it.close()
                            }
                        }
                    }
                })
    }
}