package com.jinghan.app.mvp.model.response

/**
 * @author liuzeren
 * @time 2018/3/14 11:56
 * @mail lzr319@163.com
 */
class AddCourseInfoResponse constructor(var value:Int) : BaseResponse()