package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.presenter.IAnswerQuestionActivityPresenter
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/3/11    下午9:40
 * @mail lzr319@163.com
 */
class AnswerQuestionActivityPresenter @Inject constructor():IAnswerQuestionActivityPresenter(){
    override fun answer(description: String, pics: ArrayList<String>, questionNo: String) {

        mView?.let { it.showLoading("正在提交问题答案~") }

        mOkHttp.retrofit.builder(QuestionService::class.java).submitAnswer(AnswerInfo(description,pics,questionNo))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{it.submitAnswer()}
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

    /**解答问题的实体类*/
    data class AnswerInfo(var description:String,
    var pics :ArrayList<String>,
    var questionNo:String)
}