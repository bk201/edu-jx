package com.jinghan.core.mvp.widget.scaleimageview.decoder;

import android.support.annotation.NonNull;

/**
 * @author liuzeren
 * @time 2017/12/26 17:30
 * @mail lzr319@163.com
 */
public class CompatDecoderFactory<T> implements DecoderFactory<T> {
    private Class<? extends T> clazz;

    public CompatDecoderFactory(@NonNull Class<? extends T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T make() throws IllegalAccessException, InstantiationException {
        return clazz.newInstance();
    }
}
