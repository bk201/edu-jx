package com.jinghan.core.helper.setting

import android.support.annotation.StringRes
import com.jinghan.app.AppContext
import com.jinghan.core.helper.SharedPrefsUtils

/**
 * @author liuzeren
 * @time 2018/1/24    下午8:48
 * @mail lzr319@163.com
 */
abstract class SettingsEntry<T> constructor(@StringRes var keyResId:Int, var defaultValueResId:Int ) : SharedPrefsUtils.Entry<T>{

    override fun getKey(): String {
        return AppContext.instance.getString(keyResId)
    }

    abstract fun getValue() : T

    abstract fun putValue(value:T)

    fun remove() {
        SharedPrefsUtils.remove(this)
    }

}