package com.jinghan.app.mvp.model.bean

/**
 * @author liuzeren
 * @time 2018/2/11    下午2:55
 * @mail lzr319@163.com
 */
data class ReplayInfo(var addTime:String,
var answerAddTime:String,
var answerDescription:String,
var answerPictures:ArrayList<String>,
var answerUploadTime:String,
var answerUserId:Int,
var answerUserName:String,
var canAnswer:Boolean,
var canContact:Boolean,
var canLookEvaluate:Boolean,
var canRefund:Boolean,
var description:String,
var evaluateContent:String,
var evaluateStatus:Int,
var money:Int,
var orderStatus:Int,
var pictures:ArrayList<String>,
var questionNo:String,
var statusText:String,
var type:String,
var userId:Int){
    companion object {
        /**全部*/
        val ALL = 0

        /**未解答*/
        val IN_REPLAY = 1

        /**已解答*/
        val IS_REPLAY = 2

        /**评价*/
        val EVALUATE = 3

        /**退款*/
        val REFUND =4
    }
}