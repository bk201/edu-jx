package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.AskInfo
import com.jinghan.app.mvp.model.bean.OrderInfo
import com.jinghan.app.mvp.model.bean.OrderShowInfo
import com.jinghan.app.mvp.model.bean.ReplayInfo
import com.jinghan.core.mvp.view.impl.view.BaseView
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/11    下午3:31
 * @mail lzr319@163.com
 */
interface IOrderListFragmentView : BaseView{

    fun orderList(isRefresh:Boolean,data:ArrayList<OrderShowInfo>)

    fun replayList(isRefresh:Boolean,data:ArrayList<ReplayInfo>)

    fun askList(isRefresh:Boolean,data:ArrayList<AskInfo>)

}