package com.jinghan.app.mvp.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jinghan.app.mvp.model.bean.TeacherListInfo
import com.jinghan.app.mvp.view.activity.TeacherInfoActivity
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/13    下午2:40
 * @mail lzr319@163.com
 */
class TeacherListFragmentAdapter : BaseAdapter<TeacherListFragmentAdapter.TeacherListFragmentViewHolder, TeacherListInfo>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeacherListFragmentViewHolder {
        return TeacherListFragmentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.frg_teacher_list_item,parent,false))
    }


    inner class TeacherListFragmentViewHolder(itemView: View) : BaseViewHolder<TeacherListInfo>(itemView){

        private var ivCover:ImageView
        private var tvName:TextView
        /**科目*/
        private var tvSubject:TextView
        /**累计教学时间*/
        private var tvCountOfTimes:TextView
        /**累计视频教学时间*/
        private var tvCountOfVideoTimes:TextView
        /**累计现场教学时间*/
        private var tvCountOfInline:TextView
        /**距离*/
        private var tvLocate:TextView

        init {
            ivCover = itemView.findViewById(R.id.ivCover)
            tvName = itemView.findViewById(R.id.tvName)
            tvSubject = itemView.findViewById(R.id.tvSubject)
            tvCountOfTimes = itemView.findViewById(R.id.tvCountOfTimes)
            tvCountOfVideoTimes = itemView.findViewById(R.id.tvCountOfVideoTimes)
            tvCountOfInline = itemView.findViewById(R.id.tvCountOfInline)
            tvLocate = itemView.findViewById(R.id.tvLocate)
        }

        override fun update(t: TeacherListInfo) {
            Glide.with(itemView.context).load(t.avatar).into(ivCover)

            tvName.text = t.teacherName
            tvSubject.text = t.type
            tvCountOfTimes.text = itemView.context.getString(R.string.hours,t.teachTime)
            tvCountOfVideoTimes.text = itemView.context.getString(R.string.hours_money,t.vedioTeachCharge)
            tvCountOfInline.text = itemView.context.getString(R.string.hours_money,t.visitTeachCharge)
            tvLocate.text = t.distance

            itemView.setOnClickListener {
                var intent = Intent(itemView.context, TeacherInfoActivity::class.java)
                intent.putExtra(TeacherInfoActivity.ID,t.userId)
                itemView.context.startActivity(intent)
            }
        }

    }
}