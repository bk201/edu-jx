package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IMyAccountActivityPresenter
import com.jinghan.app.mvp.presenter.impl.MyAccountActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author
 * @time
 * @mail
 */
@Module
abstract class MyAccountActivityModule{
    @ActivityScoped
    @Binds
    abstract fun myAccountPresenter(myAccountActivityPresenter: MyAccountActivityPresenter) : IMyAccountActivityPresenter
}