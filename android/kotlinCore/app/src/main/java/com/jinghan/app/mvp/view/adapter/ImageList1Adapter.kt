package com.jinghan.app.mvp.view.adapter

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/28    下午4:12
 * @mail lzr319@163.com
 */
class ImageListAdapter1 constructor(var listener:OnItemClickListener?) : BaseAdapter<BaseViewHolder<String>, String>(){

    var selectedImg = ""

    interface OnItemClickListener{
        fun onClick(url:String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> {
        var view: View = LayoutInflater.from(parent.context).inflate(R.layout.fg_publish_item_img1,parent,false)
        return ImageViewHolder(view,listener)
    }

    fun setSelected(index:Int){
        if(index>=0 && index<itemCount){
            selectedImg = data.get(index)
            listener?.onClick(selectedImg)

            notifyDataSetChanged()
        }
    }

    fun getSelectedIndex():Int{
        var index = 0

        if(!TextUtils.isEmpty(selectedImg)){
            index = data.indexOf(selectedImg)
        }

        return index
    }

    inner class ImageViewHolder(itemView: View, var listener:OnItemClickListener?) : BaseViewHolder<String>(itemView){

        var ivImage: ImageView

        init {
            ivImage = itemView.findViewById(R.id.ivImage)
        }

        override fun update(t: String) {

            ivImage.isSelected = t.equals(selectedImg)

            itemView.setOnClickListener(object : View.OnClickListener{
                override fun onClick(v: View) {
                    ivImage.isSelected = t.equals(selectedImg)
                    listener?.onClick(t)
                    selectedImg = t
                    notifyDataSetChanged()
                }
            })

            Glide.with(itemView.context).load(t).centerCrop().into(ivImage)
        }
    }

}