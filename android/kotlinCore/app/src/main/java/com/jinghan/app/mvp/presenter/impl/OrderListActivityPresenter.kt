package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.presenter.IOrderListActivityPresenter
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/1/25    下午10:11
 * @mail lzr319@163.com
 */
class OrderListActivityPresenter @Inject constructor() : IOrderListActivityPresenter(){}