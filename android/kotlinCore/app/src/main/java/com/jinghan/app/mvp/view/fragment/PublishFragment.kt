package com.jinghan.app.mvp.view.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.location.LocationManager
import android.support.v7.widget.GridLayoutManager
import android.text.Html
import android.text.TextUtils
import android.view.View
import com.amap.api.location.AMapLocation
import com.amap.api.location.AMapLocationListener
import com.jinghan.app.AppContext
import com.jinghan.app.global.Constant
import com.jinghan.app.mvp.presenter.IPublishFragmentPresenter
import com.jinghan.app.mvp.presenter.impl.FileUploadPresenter
import com.jinghan.app.mvp.view.activity.PayActivity
import com.jinghan.app.mvp.view.activity.SubjectListActivity
import com.jinghan.app.mvp.view.adapter.ImageListAdapter
import com.jinghan.app.mvp.view.adapter.decoration.RightPaddingDecoration
import com.jinghan.app.mvp.view.impl.view.IFileUploadView
import com.jinghan.app.mvp.view.impl.view.IPublishFragmentView
import com.jinghan.core.R
import com.jinghan.core.databinding.FgPublishBinding
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import com.jinghan.core.helper.ImageUtil
import com.jinghan.core.mvp.dialog.ChooseImageDialog
import com.jinghan.core.mvp.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.fg_publish.*
import kotlinx.android.synthetic.main.view_toolbar.view.*
import java.util.*
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/1/25    下午9:15
 * @mail lzr319@163.com
 */
@ActivityScoped
class PublishFragment @Inject constructor() : BaseFragment<FgPublishBinding>(), View.OnClickListener,IPublishFragmentView, IFileUploadView {

    /**位置信息*/
    private var location : AMapLocation? = null

    /**定位服务*/
    protected var locationHelper = AppContext.instance.locationHelper

    override fun onUploadResult(result: Boolean, image: String) {
        if(result){
            adapter.appendData(image)
        }
    }

    /**
     * 界面初始化完成的两个指标分别为：
     * 1、科目列表加载成功
     * 2、价格获取成功
     * 界面是否初始化完成*/
    private fun inited() : Boolean{
        return subjectListInited && moneyInited
    }

    /**科目列表是否加载成功*/
    private var subjectListInited : Boolean = false

    /**价格初始化成功*/
    private var moneyInited : Boolean = false

    /**价格*/
    private var money:Int = 0

    private fun init() : Boolean{
        if(!inited()){
            if(!subjectListInited){
                presenter.questionList()
            }

            if(!moneyInited){
                presenter.readMoney()
            }

            return false
        }

        return true
    }


    override fun onUploadResult(imgList: ArrayList<String>) {

    }

    private fun publish(){
        if(!init()) return

        var m = money
        try{
            m = binding.etMoney.text.toString() as Int
        }catch (e:Exception){}

        if(m < money){
            toast("问题单的价格最低不能少于"+money+"元")
        }

        presenter.publishQuestion(location?.address,location?.city,location?.cityCode,location?.country
                ,binding.etQuestion.text.toString(),location?.latitude,location?.longitude,m,adapter.data,location?.district,location?.adCode,location?.province,binding.tvSubject.text.toString())
    }

    override val layoutId: Int
        get() = R.layout.fg_publish

    override fun onMoney(money: Int) {
        moneyInited = true
        this.money = money

        binding.etMoney.setText(money.toString())
//        if(money == 0){
//            binding.etMoney.text = Html.fromHtml(getString(R.string.question_money_free))
//        }else {
//            binding.etMoney.text = money.toString()
//        }
    }

    override fun subjectList(subjects: ArrayList<String>) {
        subjectListInited = true

        if(subjects?.size > 0){
            binding.subject = subjects.get(0)
            binding.tvSubject.tag = subjects
        }
    }

    /**科目列表*/
    val REQ_SUBJECT = 10002

    private lateinit var adapter: ImageListAdapter
    private lateinit var manager:LocationManager

    override fun publishResult(orderId: String) {

        adapter.clear()
        binding.etQuestion.setText("")

        var m = money
        try{
            m = binding.etMoney.text.toString() as Int
        }catch (e:Exception){}

        PayActivity.pay(context,orderId,m)
    }

    override fun imageUploadResult(result: Boolean, url: String) {
    }

    @Inject
    protected lateinit var presenter:IPublishFragmentPresenter

    @Inject
    protected lateinit var uploadPresenter: FileUploadPresenter

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnPublish->publishQuestion()
            else-> selectSubject()
        }
    }

    /**选择科目*/
    private fun selectSubject(){
        if(!init()) return

        var values:ArrayList<String> = binding.tvSubject.tag as ArrayList<String>

        var intent = Intent(context,SubjectListActivity::class.java)
        intent.putStringArrayListExtra(SubjectListActivity.SUBJECT,values)
        startActivityForResult(intent,REQ_SUBJECT)
    }

    /**发布问题*/
    private fun publishQuestion(){

        if(TextUtils.isEmpty(binding.tvSubject.text.toString())){
            toast("请选择科目")
            return
        }

        if(adapter.itemCount == 0 && TextUtils.isEmpty(binding.etQuestion.text.toString())){
            toast("问题描述和问题图片不能同时为空哦〜")
            return
        }

        if(adapter.itemCount > 1){
            publish()
        }else {
            toast("请至少上传一张问题图片")
        }

    }

    var dialog:ChooseImageDialog? = null

    @SuppressLint("MissingPermission")
    override fun onPermissionGranted(reqCode:Int) {
        super.onPermissionGranted(reqCode)

        if(reqCode == ChooseImageDialog.REQ_CAMERA_AND_STORAGE) {
            dialog?.onPermissionGranted(reqCode)
        }
    }

    override fun initViews() {
        super.initViews()
        locationHelper?.setListener(object : AMapLocationListener{
            override fun onLocationChanged(p0: AMapLocation?) {
                location = p0

                binding.tvLocation.text = location?.address
            }
        })
        locationHelper?.start()

        binding.root.toolBar.setNavigationIcon(android.R.color.transparent)
        setTitle("问题发布")

        binding.onClick = this

        adapter = ImageListAdapter(object : ImageListAdapter.OnItemClickListener{
            override fun onClick(url: String) {
                mActivity?.let {
                    dialog = ChooseImageDialog(it,true,object : DialogInterface.OnCancelListener{
                        override fun onCancel(dialog: DialogInterface?) {

                        }
                    })
                    dialog?.show()
                }

            }
        })
        var manager = GridLayoutManager(context,4)
        binding.recyclerView.layoutManager = manager
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(RightPaddingDecoration(context,context.resources.getDimensionPixelOffset(R.dimen.right_padding)))
    }

    override fun initData() {
        presenter.readMoney()
        presenter.questionList()
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)

        uploadPresenter.takeView(this)
        uploadPresenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            ChooseImageDialog.GET_IMAGE_FROM_PHONE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                        if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(context,it.data), 2, path)) {
                            uploadPresenter.uploadImage(path,object:UploadProgressListener{
                                override fun onProgress(bytesWritten: Long, contentLength: Long) {

                                }
                            })
                        }
                    }
                }
            }
            ChooseImageDialog.GET_IMAGE_BY_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    var path = "${Constant.CAMERA_PATH}${UUID.randomUUID().toString()}.jpg"
                    if (ImageUtil.compressBitmap(ImageUtil.parseUriToPath(context,ChooseImageDialog.imageUriFromCamera), 2, path)) {
                        uploadPresenter.uploadImage(path,object:UploadProgressListener{
                            override fun onProgress(bytesWritten: Long, contentLength: Long) {

                            }
                        })
                    }
                }
            }
            REQ_SUBJECT ->{
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        var value = data.getStringExtra(SubjectListActivity.SUBJECT)
                        binding.tvSubject.text = value
                    }
                }
            }
        }
    }
}