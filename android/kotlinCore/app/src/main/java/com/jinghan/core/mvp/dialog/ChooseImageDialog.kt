package com.jinghan.core.mvp.dialog

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.jinghan.app.AppContext
import com.jinghan.core.R
import com.jinghan.core.helper.PermissionUtils
import com.orhanobut.logger.Logger
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author liuzeren
 * @time 2018/1/27    下午1:12
 * @mail lzr319@163.com
 */
class ChooseImageDialog(var mContext: Activity, cancelable: Boolean
                        , cancelListener: DialogInterface.OnCancelListener)
    : BaseDialog(mContext,cancelable, cancelListener),View.OnClickListener{

    companion object {
        val GET_IMAGE_BY_CAMERA:Int = 5001
        val GET_IMAGE_FROM_PHONE:Int = 5002

        /**照相机权限及存储权限*/
        val REQ_CAMERA_AND_STORAGE = 10003

        var imageUriFromCamera : Uri? = null
    }


    var clickView : View? = null

    override fun onPermissionDenied() {
        super.onPermissionDenied()
    }

    override fun onPermissionGranted(reqCode:Int) {
        super.onPermissionGranted(reqCode)

        when(clickView?.id){
            R.id.tv_camera ->{
                cancel()

                imageUriFromCamera = createImagePathUri()

                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                // MediaStore.EXTRA_OUTPUT参数不设置时,系统会自动生成一个uri,但是只会返回一个缩略图
                // 返回图片在onActivityResult中通过以下代码获取
                // Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUriFromCamera)
                mContext.startActivityForResult(intent, GET_IMAGE_BY_CAMERA)
            }
            R.id.tv_file ->{
                cancel()

                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                mContext.startActivityForResult(intent, GET_IMAGE_FROM_PHONE)
            }
        }
    }

    override fun onClick(v: View) {
        clickView = v
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            onPermissionGranted(REQ_CAMERA_AND_STORAGE)
        }else {
            val ac = AppContext.instance.currentActivity()
            PermissionUtils.requestPermissionsResult(ac, REQ_CAMERA_AND_STORAGE, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE))
        }
    }

    private var mTvCamera: TextView? = null
    private var mTvFile: TextView? = null
    private var mTvCancel: TextView? = null

    init {
        initView()
    }

    private fun initView() {
        setContentView(R.layout.dialog_picker_pictrue)

        mTvCamera = findViewById(R.id.tv_camera)
        mTvFile = findViewById(R.id.tv_file)
        mTvCancel = findViewById(R.id.tv_cancel)
        mTvCancel?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                cancel()
            }
        })
        mTvFile?.setOnClickListener(this)
        mTvCamera?.setOnClickListener(this)

        layoutParams?.gravity = Gravity.BOTTOM
    }

    /**
     * 创建一条图片地址uri,用于保存拍照后的照片
     *
     * @param context
     * @return 图片的uri
     */
    fun createImagePathUri(): Uri {
        val imageFilePath : Uri

        val status = Environment.getExternalStorageState()
        val timeFormatter = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA)
        val time = System.currentTimeMillis()
        val imageName = timeFormatter.format(Date(time))
        // ContentValues是我们希望这条记录被创建时包含的数据信息
        val values = ContentValues(3)
        values.put(MediaStore.Images.Media.DISPLAY_NAME, imageName)
        values.put(MediaStore.Images.Media.DATE_TAKEN, time)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")

        if (status == Environment.MEDIA_MOUNTED) {// 判断是否有SD卡,优先使用SD卡存储,当没有SD卡时使用手机存储
            imageFilePath = context.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        } else {
            imageFilePath = context.contentResolver.insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, values)
        }

        Logger.d( "生成的照片输出路径：" + imageFilePath.toString())
        return imageFilePath
    }

}