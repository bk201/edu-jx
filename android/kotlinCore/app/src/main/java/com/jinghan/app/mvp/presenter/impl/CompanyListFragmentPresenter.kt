package com.jinghan.app.mvp.presenter.impl

import android.view.View
import com.jinghan.app.mvp.model.response.BasePageResponse
import com.jinghan.app.mvp.model.response.CompanyListResponse
import com.jinghan.app.mvp.model.response.TeacherListResponse
import com.jinghan.app.mvp.presenter.ICompanyListFragmentPresenter
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/11    下午5:25
 * @mail lzr319@163.com
 */
class CompanyListFragmentPresenter @Inject constructor() : ICompanyListFragmentPresenter(){
    private var currentPage = 1

    override fun worldCompanyList(keyWord:String, isRefresh: Boolean, hasData: Boolean) {

        if(isRefresh){
            currentPage = 1
        }else{
            if(hasData) {
                currentPage++
            }else{
                currentPage = 1
            }
        }

        mOkHttp.retrofit.builder(QuestionService::class.java).queryGlobalInstitution(keyWord,currentPage, BasePageResponse.PageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<CompanyListResponse>(){
                    override fun onComplete() {
                        mView?.stopRefresh()
                    }

                    override fun onError(e: Throwable) {
                        mView?.stopRefresh()
                        currentPage--
                        if(hasData){
                            mView?.toast(R.string.request_failure)
                        }else {
                            mView?.error(View.OnClickListener { worldCompanyList(keyWord, isRefresh, hasData) })
                        }
                    }

                    override fun onNext(response: CompanyListResponse) {
                        if(response.isSuccess){
                            mView?.onResult(isRefresh,response.values)
                        }else{
                            currentPage--
                            if(hasData){
                                mView?.toast(response.msg)
                            }else {
                                mView?.error(View.OnClickListener { worldCompanyList(keyWord, isRefresh, hasData) })
                            }
                        }
                    }
                })
    }

    override fun cityCompanyList(keyWord:String, isRefresh: Boolean, hasData: Boolean, cityCode: String?, latitude: Double?, longitude: Double?) {
        if(isRefresh){
            currentPage = 1
        }else{
            if(hasData) {
                currentPage++
            }else{
                currentPage = 1
            }
        }

        mOkHttp.retrofit.builder(QuestionService::class.java).queryLocalInstitution(keyWord,currentPage, BasePageResponse.PageSize,latitude,longitude,cityCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<CompanyListResponse>(){
                    override fun onComplete() {
                        mView?.stopRefresh()
                    }

                    override fun onError(e: Throwable) {
                        mView?.stopRefresh()
                        currentPage--
                        if(hasData){
                            mView?.toast(R.string.request_failure)
                        }else {
                            mView?.error(View.OnClickListener { cityCompanyList(keyWord, isRefresh, hasData, cityCode, latitude, longitude) })
                        }
                    }

                    override fun onNext(response: CompanyListResponse) {
                        if(response.isSuccess){
                            mView?.onResult(isRefresh,response.values)
                        }else{
                            currentPage--
                            if(hasData){
                                mView?.toast(response.msg)
                            }else {
                                mView?.error(View.OnClickListener { cityCompanyList(keyWord, isRefresh, hasData, cityCode, latitude, longitude) })
                            }
                        }
                    }
                })
    }

}
