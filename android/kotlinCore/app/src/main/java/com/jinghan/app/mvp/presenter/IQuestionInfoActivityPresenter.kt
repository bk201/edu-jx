package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IQuestionInfoActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/2/28    下午2:45
 * @mail lzr319@163.com
 */
abstract class IQuestionInfoActivityPresenter : BaseLifecyclePresenter<IQuestionInfoActivityView,ActivityEvent>(){

    abstract fun reqQuestionDetail(questionNo:String)

    /**抢单*/
    abstract fun catchQuestion(questionNo: String)

}