package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.QuestionListInfo

/**
 * @author liuzeren
 * @time 2018/2/28    下午2:54
 * @mail lzr319@163.com
 */
class QuestionInfoResponse constructor(var value:QuestionListInfo): BaseResponse(){}