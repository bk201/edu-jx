package com.jinghan.core.mvp.view.activity

import android.databinding.ViewDataBinding
import com.aspsine.swipetoloadlayout.OnLoadMoreListener
import com.aspsine.swipetoloadlayout.OnRefreshListener
import com.aspsine.swipetoloadlayout.SwipeToLoadLayout
import com.jinghan.core.mvp.view.fragment.BaseFragment

/**
 * @author liuzeren
 * @time 2018/2/3    下午2:46
 * @mail lzr319@163.com
 */
abstract class BaseRefreshActivity<B : ViewDataBinding> : BaseFragment<B>(),OnRefreshListener,OnLoadMoreListener{
    protected var swipeToLoadLayout : SwipeToLoadLayout?=null

    fun stopRefresh(){
        if(swipeToLoadLayout?.isRefreshing==true){
            swipeToLoadLayout?.isRefreshing = false
        }

        if(swipeToLoadLayout?.isLoadingMore==true){
            swipeToLoadLayout?.isLoadingMore = false
        }
    }

    override fun onLoadMore() {

    }

    override fun onDestroy() {
        super.onDestroy()

        stopRefresh()
    }
}