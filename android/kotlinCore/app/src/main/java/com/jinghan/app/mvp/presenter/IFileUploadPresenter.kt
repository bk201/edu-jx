package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IFileUploadView
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.FragmentEvent
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/1/31    下午9:44
 * @mail lzr319@163.com
 */
abstract class IFileUploadPresenter : BaseLifecyclePresenter<IFileUploadView,FragmentEvent>(){

    abstract fun uploadImage(listener:UploadProgressListener,path: ArrayList<String>)

    /**上传图片*/
    abstract fun uploadImage(imagePath:String,mUploadListener: UploadProgressListener)

}