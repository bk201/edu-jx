package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.CompanyInfo
import com.jinghan.app.mvp.model.bean.CompanyListInfo
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/11    下午5:35
 * @mail lzr319@163.com
 */
class CompanyListResponse constructor(var values:ArrayList<CompanyListInfo>): BasePageResponse()