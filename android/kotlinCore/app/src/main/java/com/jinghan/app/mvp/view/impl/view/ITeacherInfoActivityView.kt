package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.TeacherListInfo
import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:06
 * @mail lzr319@163.com
 */
interface ITeacherInfoActivityView : BaseView{

    fun onDetail(info:TeacherListInfo)

}