package com.jinghan.app.mvp.presenter.impl

import android.os.CountDownTimer
import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.presenter.IRegisterActivityPresenter
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.app.mvp.view.impl.view.IRegisterActivityView
import com.jinghan.core.R
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/1/22    下午9:52
 * @mail lzr319@163.com
 */
@ActivityScoped
class RegisterActivityPresenter @Inject constructor() : IRegisterActivityPresenter() {
    override fun getForgetPsdVerfiyCode(userPhone: String) {
        mOkHttp.retrofit.builder(UserService::class.java).getVerfiyCode(userPhone,1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object:DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let { it.verfiyCodeResult(false) }
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: BaseResponse) {
                        if(t.isSuccess){
                            mView?.let { it.verfiyCodeResult(true) }
                            mView?.let{it.toast(R.string.verfiy_code_send_success)}
                        }else{
                            mView?.let{it.toast(t.msg)}
                            mView?.let { it.verfiyCodeResult(false) }
                        }
                    }
                })
    }

    override fun forget(userPhone: String, userPwd: String, verfiyCode: String) {
        mOkHttp.retrofit.builder(UserService::class.java).findPassword(userPhone,userPwd,verfiyCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object:DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.toast(R.string.request_failure)}
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: BaseResponse) {
                        if(t.isSuccess) {
                            mView?.let { it.toast(R.string.forget_success) }
                            mView?.let { it.close() }
                        }else{
                            mView?.let { it.toast(t.msg) }
                        }
                    }
                })
    }

    /**倒计时60秒*/
    private var timer:MyCountDownTimer? = null

    private val TIMER = 60*1000L

    class MyCountDownTimer( millisInFuture:Long, countDownInterval:Long,var listener:OnCountDownTimerListenter)
        : CountDownTimer(millisInFuture,countDownInterval){
        override fun onFinish() {
            listener.onFinish()
        }

        override fun onTick(millisUntilFinished: Long) {
            listener.onTick(millisUntilFinished/1000)
        }
    }

    override fun timeDown(listener: OnCountDownTimerListenter) {
        timer = MyCountDownTimer(TIMER,1000,listener)
        timer?.start()
    }


    override fun getVerfiyCode(userPhone:String) {
        mOkHttp.retrofit.builder(UserService::class.java).getVerfiyCode(userPhone,0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object:DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let { it.verfiyCodeResult(false) }
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: BaseResponse) {
                        if(t.isSuccess){
                            mView?.let { it.verfiyCodeResult(true) }
                            mView?.let{it.toast(R.string.verfiy_code_send_success)}
                        }else{
                            mView?.let{it.toast(t.msg)}
                            mView?.let { it.verfiyCodeResult(false) }
                        }
                    }
                })
    }

    override fun register(userPhone: String, userPwd: String, verfiyCode: String) {
        mOkHttp.retrofit.builder(UserService::class.java).register(userPhone,userPwd,verfiyCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object:DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.toast(R.string.request_failure)}
                    }

                    override fun onComplete() {
                    }

                    override fun onNext(t: BaseResponse) {
                        if(t.isSuccess) {
                            mView?.let { it.toast(R.string.register_success) }
                            mView?.let { it.close() }
                        }else{
                            mView?.let { it.toast(t.msg) }
                        }
                    }
                })
    }



    override fun takeView(view: IRegisterActivityView) {
        mView = view
    }

    override fun dropView() {
        mView = null

        timer?.let { it.cancel() }
        timer = null
    }
}