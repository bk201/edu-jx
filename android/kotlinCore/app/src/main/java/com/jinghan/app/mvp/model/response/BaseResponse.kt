package com.jinghan.app.mvp.model.response

/**
 * @author liuzeren
 * @time 2017/11/9    上午11:21
 * @mail lzr319@163.com
 */
open class BaseResponse(var code : Int = 0
                           ,var msg: String = ""){
    val isSuccess: Boolean
        get() = code == STATUS_SUCCESS

    companion object {

        val STATUS_SUCCESS = 200
    }
}