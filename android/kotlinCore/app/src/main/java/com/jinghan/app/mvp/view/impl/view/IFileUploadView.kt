package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/3    下午2:59
 * @mail lzr319@163.com
 */
interface IFileUploadView : BaseView{

    fun onUploadResult(imgList:ArrayList<String>)

    fun onUploadResult(result:Boolean,image:String)

}