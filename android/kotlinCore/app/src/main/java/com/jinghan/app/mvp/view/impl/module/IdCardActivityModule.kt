package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IFileUploadActivityPresenter
import com.jinghan.app.mvp.presenter.impl.FileUploadActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/1/28    下午7:51
 * @mail lzr319@163.com
 */
@Module
abstract class IdCardActivityModule{

    @ActivityScoped
    @Binds
    abstract fun fileUploadActivityPresenter(mFileUploadActivityPresenter: FileUploadActivityPresenter) : IFileUploadActivityPresenter

}