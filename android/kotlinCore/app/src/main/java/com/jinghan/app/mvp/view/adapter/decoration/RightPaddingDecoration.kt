package com.jinghan.app.mvp.view.adapter.decoration

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * @author liuzeren
 * @time 2018/1/27    下午11:38
 * @mail lzr319@163.com
 */
class RightPaddingDecoration(var context: Context, var dividerHeight:Int): RecyclerView.ItemDecoration(){

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect?.right = dividerHeight
        outRect?.bottom = dividerHeight
    }

}