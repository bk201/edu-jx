package com.jinghan.app.mvp.model.response

/**
 * @author liuzeren
 * @time 2018/3/14 13:53
 * @mail lzr319@163.com
 */
class OrderMealResponse constructor(var value:String) : BaseResponse()