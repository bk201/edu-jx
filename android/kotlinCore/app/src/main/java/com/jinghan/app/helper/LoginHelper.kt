package com.jinghan.app.helper

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.text.TextUtils
import com.jinghan.app.mvp.model.bean.UserInfo
import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.model.response.UserInfoResponse
import com.jinghan.app.mvp.view.activity.LoginActivity
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.jinghan.core.dependencies.http.OkHttp
import com.jinghan.core.helper.setting.impl.StringSettingsEntry
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author liuzeren
 * @time 2018/1/24    下午7:35
 * @mail lzr319@163.com
 */
@Singleton
class LoginHelper @Inject constructor(var context:Context){

    private val tokenEntry:StringSettingsEntry = StringSettingsEntry(R.string.login_token,R.string.login_token_value)

    @Inject
    protected lateinit var mOkHttp: OkHttp

    init {
        instance = this
    }

     companion object {
         var instance: LoginHelper? = null

         @Synchronized
         fun getInstance(context: Context): LoginHelper? {
             if (null == instance) {
                 instance = LoginHelper(context)
             }

             return instance
         }
     }

    /**登录用户信息*/
    var userInfo:UserInfo? = null

    fun login(token:String){
        tokenEntry.putValue(token)
    }

    /**用户是否已登录*/
    fun isLogin() : Boolean{
        return !TextUtils.isEmpty(tokenEntry.getValue()) && null != userInfo
    }

    public interface LoginResultListener{

        fun loginResult(isSuccess: Boolean)

    }

    fun getToken() : String{
        return tokenEntry.getValue()
    }

    fun logout(){
        tokenEntry.remove()
        userInfo = null

        var intent = Intent(context,LoginActivity::class.java)
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

    fun login(autoLogin:Boolean,listener: LoginResultListener?){
        if(autoLogin){
            var token = getToken()

            if(!TextUtils.isEmpty(token)){
                //根据token获取用户信息
                mOkHttp.retrofit.builder(UserService::class.java).getUserInfo(token)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object: DefaultObserver<UserInfoResponse>(){
                            override fun onError(e: Throwable) {
                                listener?.loginResult(false)
                            }

                            override fun onComplete() {
                            }

                            override fun onNext(response: UserInfoResponse) {
                                if(response.isSuccess){
                                    userInfo = response.value
                                    listener?.loginResult(true)
                                }else{
                                    listener?.loginResult(false)
                                }
                            }
                        })
            }else{
//                login(false,listener)
                listener?.loginResult(false)
            }
        }else{
            var intent = Intent(context,LoginActivity::class.java)
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }
}