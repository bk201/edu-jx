package com.jinghan.app.mvp.view.impl.module

import dagger.Module

/**
 * @author liuzeren
 * @time 2018/3/17 11:26
 * @mail lzr319@163.com
 */
@Module
abstract class EvaluateListActivityModule