package com.jinghan.app.mvp.presenter.impl

import android.view.View
import com.jinghan.app.mvp.model.response.*
import com.jinghan.app.mvp.presenter.IOrderListFragmentPresenter
import com.jinghan.app.mvp.view.impl.api.OrderService
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/11    下午3:34
 * @mail lzr319@163.com
 */
class OrderListFragmentPresenter @Inject constructor() : IOrderListFragmentPresenter(){

    override fun replayList(status: Int, isRefresh: Boolean, hasData: Boolean) {
        if(isRefresh){
            currentPage = 1
        }else{
            if(hasData) {
                currentPage++
            }else{
                currentPage = 1
            }
        }

        mOkHttp.retrofit.builder(OrderService::class.java).queryAnswer(status,currentPage, BasePageResponse.PageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<ReplayInfoListResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        currentPage--
                        if(hasData){
                            mView?.toast(R.string.request_failure)
                        }else {
                            mView?.error(View.OnClickListener { replayList(status, isRefresh, hasData) })
                        }
                    }

                    override fun onNext(response: ReplayInfoListResponse) {
                        if(response.isSuccess){
                            mView?.replayList(isRefresh,response.values)
                        }else{
                            currentPage--
                            if(hasData){
                                mView?.toast(response.msg)
                            }else {
                                mView?.error(View.OnClickListener { replayList(status, isRefresh, hasData) })
                            }
                        }
                    }
                })
    }

    override fun askList(status: Int, isRefresh: Boolean, hasData: Boolean) {
        if(isRefresh){
            currentPage = 1
        }else{
            if(hasData) {
                currentPage++
            }else{
                currentPage = 1
            }
        }

        mOkHttp.retrofit.builder(UserService::class.java).askList(status,currentPage, BasePageResponse.PageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<AskListResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        currentPage--
                        if(hasData){
                            mView?.toast(R.string.request_failure)
                        }else {
                            mView?.error(View.OnClickListener { askList(status, isRefresh, hasData) })
                        }
                    }

                    override fun onNext(response: AskListResponse) {
                        if(response.isSuccess){
                            mView?.askList(isRefresh,response.values)
                        }else{
                            currentPage--
                            if(hasData){
                                mView?.toast(response.msg)
                            }else {
                                mView?.error(View.OnClickListener { askList(status, isRefresh, hasData) })
                            }
                        }
                    }
                })
    }

    /**当前页*/
    private var currentPage = 1

    override fun orderList(status: Int, isRefresh: Boolean, hasData: Boolean) {
        if(isRefresh){
            currentPage = 1
        }else{
            if(hasData) {
                currentPage++
            }else{
                currentPage = 1
            }
        }

        mOkHttp.retrofit.builder(OrderService::class.java).queryOrder(status,currentPage, BasePageResponse.PageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<OrderShowInfoListResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        currentPage--
                        if(hasData){
                            mView?.toast(R.string.request_failure)
                        }else {
                            mView?.error(View.OnClickListener { orderList(status, isRefresh, hasData) })
                        }
                    }

                    override fun onNext(response: OrderShowInfoListResponse) {
                        if(response.isSuccess){
                            mView?.orderList(isRefresh,response.values)
                        }else{
                            currentPage--
                            if(hasData){
                                mView?.toast(response.msg)
                            }else {
                                mView?.error(View.OnClickListener { orderList(status, isRefresh, hasData) })
                            }
                        }
                    }
                })
    }

}