package com.jinghan.app.mvp.view.activity

import android.content.Intent
import android.view.View
import com.jinghan.app.helper.LoginHelper
import com.jinghan.app.mvp.view.activity.verfiy.CompanyActivity
import com.jinghan.app.mvp.view.activity.verfiy.EducationActivity
import com.jinghan.app.mvp.view.activity.verfiy.IdCardActivity
import com.jinghan.core.databinding.AtyUserInfoBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/1/28    下午7:40
 * @mail lzr319@163.com
 */
class UserInfoActivity constructor(override val layoutId: Int = R.layout.aty_user_info)
    : BaseActivity<AtyUserInfoBinding>(), View.OnClickListener{

    override fun onClick(v: View) {
        when(v.id){
            R.id.viewInfo -> editUserInfo()
            R.id.tvSex -> editSex()
            R.id.tvPhone -> editPhone()
            R.id.tvCity -> editCity()
            R.id.tvAddress -> editAddress()
            R.id.viewToTeacher -> toTeacher()
            R.id.viewToCompany -> toCompany()
        }
    }

    /**升级为机构*/
    private fun toCompany(){
        var intent = Intent(this,CompanyActivity::class.java)
        intent.putExtra(IdCardActivity.VERFIY_TYPE,IdCardActivity.TO_COMPANY)
        startActivity(intent)
    }

    /**成为老师*/
    private fun toTeacher(){
        var intent = Intent(this,IdCardActivity::class.java)
        intent.putExtra(IdCardActivity.VERFIY_TYPE,IdCardActivity.TO_TEACHER)

        startActivity(intent)
    }

    /**编辑地址信息*/
    private fun editAddress(){}

    /**编辑城市信息*/
    private fun editCity(){}

    /**编辑用户信息*/
    private fun editUserInfo(){}

    /**编辑性别*/
    private fun editSex(){}

    /**编辑电话号码*/
    private fun editPhone(){}

    @Inject
    protected lateinit var helper:LoginHelper

    override fun initViewsAndListener() {
        mViewBinding.root.toolBar.tvTitle.text = "个人资料"

        mViewBinding.onClick = this
        if (helper.isLogin()){
            mViewBinding.userInfo = helper.userInfo
        }
    }

    override fun initData() {
    }

    override fun initPresenter() {
    }

}