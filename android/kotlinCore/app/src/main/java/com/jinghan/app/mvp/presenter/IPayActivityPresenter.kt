package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IPayActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/3/16 20:38
 * @mail lzr319@163.com
 */
abstract class IPayActivityPresenter : BaseLifecyclePresenter<IPayActivityView,ActivityEvent>(){

    abstract fun pay(orderNo:String)

}