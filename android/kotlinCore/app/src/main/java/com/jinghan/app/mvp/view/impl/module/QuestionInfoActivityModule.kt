package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IQuestionInfoActivityPresenter
import com.jinghan.app.mvp.presenter.impl.QuestionInfoActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/1/28    下午7:51
 * @mail lzr319@163.com
 */
@Module
abstract class QuestionInfoActivityModule{
    @ActivityScoped
    @Binds
    abstract fun questionInfoActivityPresenter(mQuestionInfoActivityPresenter: QuestionInfoActivityPresenter) : IQuestionInfoActivityPresenter
}