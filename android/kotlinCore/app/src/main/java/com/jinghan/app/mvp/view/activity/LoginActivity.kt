package com.jinghan.app.mvp.view.activity

import android.content.Intent
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.jinghan.app.helper.LoginHelper
import com.jinghan.app.mvp.presenter.ILoginActivityPresenter
import com.jinghan.app.mvp.view.impl.view.ILoginActivityView
import com.jinghan.core.BR
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyLoginBinding
import com.jinghan.core.dependencies.aspectj.annotation.SingleClick
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/1/21    下午9:44
 * @mail lzr319@163.com
 */
class LoginActivity(override val layoutId: Int = R.layout.aty_login) : BaseActivity<AtyLoginBinding>()
        , View.OnClickListener,ILoginActivityView {

    @Inject
    protected lateinit var helper:LoginHelper

    override fun loginResult(result: Boolean, token: String) {
        if(result){
            helper.login(token)

            helper.login(true,object:LoginHelper.LoginResultListener{
                override fun loginResult(isSuccess: Boolean) {
                    if(isSuccess){
                        var intent = Intent(baseContext,HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            })
        }
    }

    @Inject
    protected lateinit var presenter: ILoginActivityPresenter

    @SingleClick
    override fun onClick(v: View) {
        when(v.id){
            R.id.btnLogin->login()
            R.id.tvForgetPwd->{

                var intent = Intent(this,RegisterActivity::class.java)
                intent.putExtra(RegisterActivity.TYPE,RegisterActivity.TYPE_FORGET)

                startActivity(intent)
            }
            else -> register()
        }
    }

    private fun login(){
        presenter.login(mViewBinding.etPhone.text.toString(),mViewBinding.etPwd.text.toString())
    }

    private fun register(){
        var intent = Intent(this,RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun initViewsAndListener() {
        mViewBinding.setVariable(BR.onClick,this)

        mViewBinding.etPhone.addTextChangedListener(object: TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                check()
            }
        })

        mViewBinding.etPwd.addTextChangedListener(object: TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                check()
            }
        })
    }

    private fun check(){
        mViewBinding.btnLogin.isEnabled = !TextUtils.isEmpty(mViewBinding.etPhone.text)
                && !TextUtils.isEmpty(mViewBinding.etPwd.text) && mViewBinding.etPwd.text.length >= 6
    }

    override fun initData() {
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
    }
}
