package com.jinghan.app.mvp.view.impl.api

import com.jinghan.app.mvp.model.response.ImageUploadResponse
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

/**
 * @author liuzeren
 * @time 2018/1/28    上午11:13
 * @mail lzr319@163.com
 */
interface FileUploadService{


    /**
     * 用户发布问题上传图片
     * 图片限制2M以内,且必须为图片格式(png,jpg,bmp,gif)
     * */
    @Multipart
    @POST("question/upload-pic-question")
    fun uploadQuestionPic(@Part part: MultipartBody.Part, @Query("token") token:String) : Observable<ImageUploadResponse>

    /**
     * 老师回答问题上传图片
     * 图片限制2M以内,且必须为图片格式(png,jpg,bmp,gif)
     * */
    @POST("question/upload-pic-answer")
    fun uploadAnswerPic(): Observable<ImageUploadResponse>

}