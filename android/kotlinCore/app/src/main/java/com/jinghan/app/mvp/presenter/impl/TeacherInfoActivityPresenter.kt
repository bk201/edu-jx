package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.CompanyDetailResponse
import com.jinghan.app.mvp.model.response.TeacherDetailResponse
import com.jinghan.app.mvp.presenter.ITeacherInfoActivityPresenter
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:10
 * @mail lzr319@163.com
 */
class TeacherInfoActivityPresenter @Inject constructor() : ITeacherInfoActivityPresenter(){
    override fun requestTeacherInfo(userId: Int) {
        mView?.let { it.showLoading("正在获取老师详情信息~") }

        mOkHttp.retrofit.builder(UserService::class.java).getTeacherDetail(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<TeacherDetailResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let {
                            it.toast(R.string.request_failure)
                            it.close()
                        }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : TeacherDetailResponse) {
                        if(response.isSuccess){
                            mView?.let{it.onDetail(response.value)}
                        }else{
                            mView?.let {
                                it.toast(response.msg)
                                it.close()
                            }
                        }
                    }
                })
    }
}