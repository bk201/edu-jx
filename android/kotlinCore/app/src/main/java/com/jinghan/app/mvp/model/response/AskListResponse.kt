package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.AskInfo
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/11    下午3:37
 * @mail lzr319@163.com
 */
class AskListResponse constructor(var values:ArrayList<AskInfo>) : BasePageResponse()