package com.jinghan.app.mvp.view.activity

import com.jinghan.core.R
import com.jinghan.core.databinding.AtyEvaluateListBinding
import com.jinghan.core.mvp.view.activity.BaseActivity

/**
 * @author liuzeren
 * @time 2018/3/17 11:20
 * @mail lzr319@163.com
 */
class EvaluateListActivity : BaseActivity<AtyEvaluateListBinding>(){
    override val layoutId: Int
        get() = R.layout.aty_evaluate_list

    override fun initViewsAndListener() {
    }

    override fun initData() {
    }

    override fun initPresenter() {
    }
}