package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IVerfiyView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/2/5    下午9:21
 * @mail lzr319@163.com
 */
abstract class IVerfiyPresenter : BaseLifecyclePresenter<IVerfiyView, ActivityEvent>(){

    /**教师验证*/
    abstract fun teacherVerfiy(alipay:String,email:String,idCardBack:String,idCardFront:String,idCardNo:String,name:String,studentIdCardBack:String,studentIdCardFront:String,studentIdCardNo:String)

    /**机构验证*/
    abstract fun companyVerfiy(alipay:String,email:String,institutionBusinessLicenceNo:String,institutionBusinessLicencePic:String,name:String)

}