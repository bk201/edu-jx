package com.jinghan.app.mvp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/1/27    上午11:38
 * @mail lzr319@163.com
 */
class ImageListAdapter constructor(var listener:OnItemClickListener?) : BaseAdapter<BaseViewHolder<String>,String>(){

    interface OnItemClickListener{
        fun onClick(url:String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> {
        var holder:BaseViewHolder<String>

        var view:View = LayoutInflater.from(parent.context).inflate(viewType,parent,false)

        when(viewType){
            R.layout.fg_publish_img_add ->{
                holder = AddImageViewHolder(view,listener)
            }
            else ->{
                holder = ImageViewHolder(view,listener)
            }
        }

        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<String>, position: Int) {
        if(position != itemCount-1){
            super.onBindViewHolder(holder, position)
        }
    }

    override fun getItemViewType(position: Int): Int {

        if(position == itemCount-1){
            return R.layout.fg_publish_img_add
        }

        return R.layout.fg_publish_item_img
    }

    override fun getItemCount(): Int {
        return super.getItemCount()+1
    }

    class AddImageViewHolder(itemView:View,var listener:OnItemClickListener?) : BaseViewHolder<String>(itemView){

        init {
            itemView.setOnClickListener(object : View.OnClickListener{
                override fun onClick(v: View) {
                    listener?.onClick("")
                }
            })
        }

        override fun update(t: String) {

        }
    }

    inner class ImageViewHolder(itemView: View,var listener:OnItemClickListener?) : BaseViewHolder<String>(itemView){

        var ivImage:ImageView?=null
        var ivDel:ImageView?=null

        init {
            ivDel = itemView.findViewById(R.id.ivDel)
            ivImage = itemView.findViewById(R.id.ivImage)
        }

        override fun update(t: String) {
            itemView.setOnClickListener(object : View.OnClickListener{
                override fun onClick(v: View) {
                    listener?.onClick(t)
                }
            })

            ivImage?.setOnClickListener(object : View.OnClickListener{
                override fun onClick(v: View?) {
                    data.remove(t)
                    notifyItemRemoved(position)
                }
            })

            Glide.with(itemView.context).load(t).centerCrop().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(ivImage)
        }
    }

}