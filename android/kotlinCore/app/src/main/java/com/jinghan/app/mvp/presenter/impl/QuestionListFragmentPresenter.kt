package com.jinghan.app.mvp.presenter.impl

import android.view.View
import com.jinghan.app.mvp.model.response.BasePageResponse
import com.jinghan.app.mvp.model.response.PublishQuestionResponse
import com.jinghan.app.mvp.model.response.QuestionListResponse
import com.jinghan.app.mvp.presenter.IQuestionListFragmentPresenter
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/10    下午9:20
 * @mail lzr319@163.com
 */
class QuestionListFragmentPresenter @Inject constructor() : IQuestionListFragmentPresenter(){

    private var currentPage = 1

    override fun worldQuestionList(status: Int, isRefresh: Boolean, hasData: Boolean) {

        if(isRefresh){
            currentPage = 1
        }else{
            if(hasData) {
                currentPage++
            }else{
                currentPage = 1
            }
        }

        mOkHttp.retrofit.builder(QuestionService::class.java).worldQuestionList(status,currentPage,BasePageResponse.PageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<QuestionListResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        currentPage--
                        if(hasData){
                            mView?.toast(R.string.request_failure)
                        }else {
                            mView?.error(View.OnClickListener { worldQuestionList(status, isRefresh, hasData) })
                        }
                    }

                    override fun onNext(response: QuestionListResponse) {
                        if(response.isSuccess){
                            mView?.onResult(isRefresh,response.values)
                        }else{
                            currentPage--
                            if(hasData){
                                mView?.toast(response.msg)
                            }else {
                                mView?.error(View.OnClickListener { worldQuestionList(status, isRefresh, hasData) })
                            }
                        }
                    }
                })
    }

    override fun cityQuestionList(status: Int, isRefresh: Boolean, hasData: Boolean, cityCode: String?, latitude: Double?, longitude: Double?) {
        if(isRefresh){
            currentPage = 1
        }else{
            if(hasData) {
                currentPage++
            }else{
                currentPage = 1
            }
        }

        mOkHttp.retrofit.builder(QuestionService::class.java).cityQuestionList(status,currentPage,BasePageResponse.PageSize,latitude,longitude,cityCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindFragment(lifecycleSubject))
                .subscribe(object : DefaultObserver<QuestionListResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        currentPage--
                        if(hasData){
                            mView?.toast(R.string.request_failure)
                        }else {
                            mView?.error(View.OnClickListener { cityQuestionList(status, isRefresh, hasData, cityCode, latitude, longitude) })
                        }
                    }

                    override fun onNext(response: QuestionListResponse) {
                        if(response.isSuccess){
                            mView?.onResult(isRefresh,response.values)
                        }else{
                            currentPage--
                            if(hasData){
                                mView?.toast(response.msg)
                            }else {
                                mView?.error(View.OnClickListener { cityQuestionList(status, isRefresh, hasData, cityCode, latitude, longitude) })
                            }
                        }
                    }
                })
    }

}