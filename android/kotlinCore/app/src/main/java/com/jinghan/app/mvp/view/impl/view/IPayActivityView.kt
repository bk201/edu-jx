package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/3/16 20:38
 * @mail lzr319@163.com
 */
interface IPayActivityView : BaseView