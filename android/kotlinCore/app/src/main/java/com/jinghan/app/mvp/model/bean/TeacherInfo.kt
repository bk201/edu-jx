package com.jinghan.app.mvp.model.bean

/**
 * 老师信息
 * @author liuzeren
 * @time 2018/2/13    上午11:22
 * @mail lzr319@163.com
 */
data class TeacherInfo(var address:String,
                       var city:String,
                       var cityCode:String,
                       var country:String,
                       var introduce:String,
                       var latitude:Double,
                       var longitude:Double,
                       var mobile:String,
                       var name:String,
                       var phase:String,
                       var region:String,
                       var regionCode:String,
                       var state:String,
                       var teachYears:String,
                       var type:String,
                       var vedioTeachCharge:Int,
                       var visitTeachCharge:Int){
    /*address (string, optional): 发布问题时的地理位置信息,地址 ,
city (string, optional): 发布问题时的地理位置信息,城市 ,
cityCode (string, optional): 发布问题时的地理位置信息,城市编码,如 025 ,
country (string, optional): 发布问题时的地理位置信息,国家 ,
introduce (string, optional): 自我介绍 ,
latitude (number, optional): 发布问题时的地理位置信息,纬度 ,
longitude (number, optional): 发布问题时的地理位置信息,经度 ,
mobile (string, optional): 手机号 ,
name (string, optional): 姓名 ,
phase (string, optional): 阶段 ,
region (string, optional): 发布问题时的地理位置信息,区,如 雨花区 ,
regionCode (string, optional): 发布问题时的地理位置信息, 区域码 如 320114 ,
state (string, optional): 发布问题时的地理位置信息,省 ,
teachYears (string, optional): 教龄,单位为年 ,
type (string, optional): 科目,调用API取 ,
vedioTeachCharge (integer, optional): 视频教学费用,元/小时 ,
visitTeachCharge (integer, optional): 上门教学费用,元/小时*/
}