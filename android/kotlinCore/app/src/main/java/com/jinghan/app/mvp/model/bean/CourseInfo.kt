package com.jinghan.app.mvp.model.bean

/**
 * 套餐信息
 * @author liuzeren
 * @time 2018/3/14 11:52
 * @mail lzr319@163.com
 */
data class CourseInfo(var courseCount:Int,
var courseName:String,
var courseTime:String,
var id:Int,
var introduce:String,
var pics:Array<String>,
var price:Int,
var teacher:String)