package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.presenter.IVerfiyPresenter
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/5    下午9:22
 * @mail lzr319@163.com
 */
class VerfiyPresenter @Inject constructor() : IVerfiyPresenter(){

    data class TeacherVerfiyInfo(var alipay: String, var email: String, var idCardBack: String, var idCardFront: String
                                 , var idCardNo: String, var name: String,var studentIdCardBack: String,var  studentIdCardFront: String,var studentIdCardNo: String)

    data class CompanyVerfiyInfo(var alipay: String, var email: String, var institutionBusinessLicenceNo: String, var institutionBusinessLicencePic: String,var name: String)

    override fun teacherVerfiy(alipay: String, email: String, idCardBack: String, idCardFront: String, idCardNo: String, name: String, studentIdCardBack: String, studentIdCardFront: String, studentIdCardNo: String) {
        var teacher = TeacherVerfiyInfo(alipay, email, idCardBack, idCardFront, idCardNo, name, studentIdCardBack, studentIdCardFront, studentIdCardNo)

        mOkHttp.retrofit.builder(UserService::class.java).verfiyTeacher(teacher)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object : DefaultObserver<BaseResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        mView?.toast(R.string.request_failure)
                    }

                    override fun onNext(response: BaseResponse) {
                        if(response.isSuccess){
                            mView?.onVerfiyResult()
                        }else{
                            mView?.toast(response.msg)
                        }
                    }
                })
    }

    override fun companyVerfiy(alipay: String, email: String, institutionBusinessLicenceNo: String, institutionBusinessLicencePic: String, name: String) {
        var companyInfo = CompanyVerfiyInfo(alipay, email, institutionBusinessLicenceNo, institutionBusinessLicencePic, name)

        mOkHttp.retrofit.builder(UserService::class.java).verfiyCompany(companyInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object : DefaultObserver<BaseResponse>(){
                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        mView?.toast(R.string.request_failure)
                    }

                    override fun onNext(response: BaseResponse) {
                        if(response.isSuccess){
                            mView?.onVerfiyResult()
                        }else{
                            mView?.toast(response.msg)
                        }
                    }
                })
    }

}