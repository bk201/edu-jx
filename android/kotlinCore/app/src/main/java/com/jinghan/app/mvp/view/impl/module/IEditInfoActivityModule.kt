package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IEditInfoActivityPresenter
import com.jinghan.app.mvp.presenter.impl.EditInfoActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/2/12    下午1:55
 * @mail lzr319@163.com
 */
@Module
abstract class IEditInfoActivityModule{
    @ActivityScoped
    @Binds
    abstract fun editInfoActivityPresenter(mEditInfoActivityPresenter: EditInfoActivityPresenter) : IEditInfoActivityPresenter

}