package com.jinghan.app.mvp.model.bean

/**
 * @author liuzeren
 * @time 2018/3/14 13:46
 * @mail lzr319@163.com
 */
data class OrderShowInfo(
        var addTime:String,
var canCancel:Boolean,
var canCancelRefund:Boolean,
var canContact:Boolean,
var canEvaluate:Boolean,
var canFinish:Boolean,
var canPay:Boolean,
var canPlatform:Boolean,
var canRefund:Boolean,
var evaluateStatus:Boolean,
var finishTime:String,
var id:Int,
var items:Array<OrderInfo>,
var orderAmount:Int,
var orderNo:String,
var orderStatus:Int,
var orderType:Int,
var payStatus:Int,
var sellerId:Int,
var sellerName:String,
var userId:Int,
var userMobile:String,
var userName:String
)