package com.jinghan.core.helper

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.jinghan.app.AppContext

/**
 * @author liuzeren
 * @time 2018/1/24    下午8:39
 * @mail lzr319@163.com
 */
object SharedPrefsUtils{

    fun getSharedPrefs() : SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(AppContext.instance)
    }

    fun getString(entry:Entry<String>) : String {
        return getSharedPrefs().getString(entry.getKey(), entry.getDefaultValue())
    }

    fun getStringSet(entry:Entry<Set<String>>) : Set<String>{
        return getSharedPrefs().getStringSet(entry.getKey(), entry.getDefaultValue())
    }

    fun getInt(entry : Entry<Int>) : Int{
        return getSharedPrefs().getInt(entry.getKey(), entry.getDefaultValue())
    }

    fun getLong(entry:Entry<Long>) : Long {
        return getSharedPrefs().getLong(entry.getKey(), entry.getDefaultValue())
    }

    fun getFloat(entry:Entry<Float>) : Float {
        return getSharedPrefs().getFloat(entry.getKey(), entry.getDefaultValue())
    }

    fun getBoolean(entry:Entry<Boolean>) : Boolean{
        return getSharedPrefs().getBoolean(entry.getKey(), entry.getDefaultValue())
    }

    fun getEditor() : SharedPreferences.Editor{
        return getSharedPrefs().edit()
    }

    fun putString(entry:Entry<String>, value:String) {
        getEditor().putString(entry.getKey(), value).apply()
    }

    fun putStringSet(entry:Entry<Set<String>>, value:Set<String>) {
        getEditor().putStringSet(entry.getKey(), value).apply()
    }

    fun putInt(entry:Entry<Integer>, value:Int) {
        getEditor().putInt(entry.getKey(), value).apply()
    }

    fun putLong(entry:Entry<Long>, value:Long) {
        getEditor().putLong(entry.getKey(), value).apply()
    }

    fun putFloat(entry:Entry<Float>, value:Float) {
        getEditor().putFloat(entry.getKey(), value).apply()
    }

    fun putBoolean(entry:Entry<Boolean>, value:Boolean) {
        getEditor().putBoolean(entry.getKey(), value).apply()
    }

    fun <T> remove(entry:Entry<T>) {
        getEditor().remove(entry.getKey()).apply()
    }

    fun clear() {
        getEditor().clear().apply()
    }

    interface Entry<T> {
        fun getKey() : String
        fun getDefaultValue() : T
    }

}