package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.IEvaluateActivityPresenter
import com.jinghan.app.mvp.presenter.IFileUploadActivityPresenter
import com.jinghan.app.mvp.presenter.IFileUploadPresenter
import com.jinghan.app.mvp.presenter.impl.EvaluateActivityPresenter
import com.jinghan.app.mvp.presenter.impl.FileUploadActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/3/16 16:16
 * @mail lzr319@163.com
 */
@Module
abstract class EvaluateActivityModule{
    @ActivityScoped
    @Binds
    abstract fun evaluateActivityPresenter(mEvaluateActivityPresenter: EvaluateActivityPresenter) : IEvaluateActivityPresenter

    @ActivityScoped
    @Binds
    abstract fun fileUploadActivityPresenter(mFileUploadActivityPresenter: FileUploadActivityPresenter) : IFileUploadActivityPresenter
}