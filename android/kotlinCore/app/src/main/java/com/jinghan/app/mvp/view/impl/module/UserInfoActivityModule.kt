package com.jinghan.app.mvp.view.impl.module

import dagger.Module

/**
 * @author liuzeren
 * @time 2018/1/28    下午7:51
 * @mail lzr319@163.com
 */
@Module
abstract class UserInfoActivityModule