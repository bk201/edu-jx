package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.ReplayInfo
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/11    下午3:37
 * @mail lzr319@163.com
 */
class ReplayListResponse constructor(var values:ArrayList<ReplayInfo>) : BasePageResponse()