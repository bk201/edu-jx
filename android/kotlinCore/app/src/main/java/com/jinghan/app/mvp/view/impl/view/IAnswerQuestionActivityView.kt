package com.jinghan.app.mvp.view.impl.view

import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/3/11    下午9:38
 * @mail lzr319@163.com
 */
interface IAnswerQuestionActivityView : BaseView{

    fun submitAnswer()

}