package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IOrderListActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/1/25    下午10:11
 * @mail lzr319@163.com
 */
abstract class IOrderListActivityPresenter : BaseLifecyclePresenter<IOrderListActivityView,ActivityEvent>(){}