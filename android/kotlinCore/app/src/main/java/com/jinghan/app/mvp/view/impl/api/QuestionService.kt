package com.jinghan.app.mvp.view.impl.api

import com.jinghan.app.mvp.model.response.*
import com.jinghan.app.mvp.presenter.impl.AnswerQuestionActivityPresenter
import com.jinghan.app.mvp.presenter.impl.PublishFragmentPresenter
import com.jinghan.app.mvp.presenter.impl.RefundActivityPresenter
import io.reactivex.Observable
import retrofit2.http.*

/**
 * @author liuzeren
 * @time 2018/1/26    下午7:29
 * @mail lzr319@163.com
 */
interface QuestionService{

    /**问题列表*/
    @GET("question/query-question")
    fun questionList(@Query("pageNo") pageNo:Int,@Query("pageSize") pageSize:Int) : Observable<QuestionListResponse>

    /**发布问题*/
    @Headers("Content-Type:application/json")
    @POST("question/submit-question")
    fun publishQuestion(@Body question: PublishFragmentPresenter.PublishQuestion) : Observable<PublishQuestionResponse>

    /**问题详情*/
    @GET("look-question")
    fun questionInfo(@Query("questionNo") questionNo:String):Observable<QuestionInfoResponse>

    /**问题价格信息*/
    @GET("config/min_question_money")
    fun questionMoney() : Observable<QuestionMoneyResponse>

    /**科目列表*/
    @GET("config/question-type")
    fun subjectList() : Observable<SubjectListResponse>

    /**全球问题列表*/
    @GET("query-global-question")
    fun worldQuestionList(@Query("status") status:Int
                          ,@Query("pageNo") pageNo:Int
                          ,@Query("pageSize") pageSize:Int):Observable<QuestionListResponse>

    /**同城问题列表*/
    @GET("query-local-question")
    fun cityQuestionList(@Query("status") status:Int,@Query("pageNo") pageNo:Int
                         ,@Query("pageSize") pageSize: Int
                         ,@Query("latitude") latitude:Double?=0.0
                         ,@Query("longitude") longitude:Double?=0.0
                         ,@Query("cityCode") cityCode:String?=""):Observable<QuestionListResponse>


    /**查询本地机构列表*/
    @GET("query-local-institution")
    fun queryLocalInstitution(@Query("keywords") keywords:String,@Query("pageNo") pageNo:Int
                              ,@Query("pageSize") pageSize: Int
                              ,@Query("latitude") latitude:Double?=0.0
                              ,@Query("longitude") longitude:Double?=0.0
                              ,@Query("cityCode") cityCode:String?=""):Observable<CompanyListResponse>

    /**查询全球机构列表*/
    @GET("query-global-institution")
    fun queryGlobalInstitution(@Query("keywords") keywords:String,@Query("pageNo") pageNo:Int
                               ,@Query("pageSize") pageSize: Int):Observable<CompanyListResponse>

    /**查询本地老师列表*/
    @GET("query-local-teacher")
    fun queryLocalTeacher(@Query("keywords") keywords:String,@Query("pageNo") pageNo:Int
                              ,@Query("pageSize") pageSize: Int
                              ,@Query("latitude") latitude:Double?=0.0
                              ,@Query("longitude") longitude:Double?=0.0
                              ,@Query("cityCode") cityCode:String?=""):Observable<TeacherListResponse>

    /**查询全球老师列表*/
    @GET("query-global-teacher")
    fun queryGlobalTeacher(@Query("keywords") keywords:String,@Query("pageNo") pageNo:Int
                               ,@Query("pageSize") pageSize: Int):Observable<TeacherListResponse>

    /**申请退款*/
    @POST("question/user/refund")
    fun refund(@Body refundInfo:RefundActivityPresenter.RefundInfo):Observable<BaseResponse>

    /**接题抢单*/
    @POST("question/catch-question")
    fun catchQuestion(@Query("questionNo") questionNo:String):Observable<BaseResponse>

    /**解答问题*/
    @POST("question/submit-answer")
    fun submitAnswer(@Body answerInfo: AnswerQuestionActivityPresenter.AnswerInfo):Observable<BaseResponse>

}