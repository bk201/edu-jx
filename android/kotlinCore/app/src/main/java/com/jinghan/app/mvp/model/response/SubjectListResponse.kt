package com.jinghan.app.mvp.model.response

import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/1/31    下午8:42
 * @mail lzr319@163.com
 */
class SubjectListResponse(var values:ArrayList<String>) : BaseResponse()