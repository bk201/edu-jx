package com.jinghan.app.mvp.model.request

/**
 * @author liuzeren
 * @time 2017/11/9    上午11:18
 * @mail lzr319@163.com
 */
data class BaseUserRequestData(var userId: Long = 0) : BaseRequestData()
