package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IMyAccountActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author xueliang
 * @time
 * @mail
 */
abstract class IMyAccountActivityPresenter : BaseLifecyclePresenter<IMyAccountActivityView, ActivityEvent>() {


}