package com.jinghan.app.mvp.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jinghan.app.mvp.model.bean.CompanyInfo
import com.jinghan.app.mvp.model.bean.CompanyListInfo
import com.jinghan.app.mvp.view.activity.CompanyInfoActivity
import com.jinghan.core.R
import com.jinghan.core.mvp.view.BaseAdapter
import com.jinghan.core.mvp.view.BaseViewHolder

/**
 * @author liuzeren
 * @time 2018/2/13    下午1:44
 * @mail lzr319@163.com
 */
class CompanyListFragmentAdapter : BaseAdapter<CompanyListFragmentAdapter.CompanyListFragmentViewHolder,CompanyListInfo>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyListFragmentViewHolder {
        return CompanyListFragmentViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.frg_company_list_item,parent,false))
    }

    inner class CompanyListFragmentViewHolder(itemView: View) : BaseViewHolder<CompanyListInfo>(itemView){

        private var ivCover:ImageView
        private var tvName:TextView
        private var tvDistance:TextView
        private var tvSubjects:TextView
        private var tvMoney:TextView
        private var tvAddress:TextView

        init {
            ivCover = itemView.findViewById(R.id.ivCover)
            tvName = itemView.findViewById(R.id.tvName)
            tvDistance = itemView.findViewById(R.id.tvDistance)
            tvSubjects = itemView.findViewById(R.id.tvSubjects)
            tvMoney = itemView.findViewById(R.id.tvMoney)
            tvAddress = itemView.findViewById(R.id.tvAddress)
        }

        override fun update(t: CompanyListInfo) {
            Glide.with(itemView.context).load(t.avatar).into(ivCover)
            tvName.text = t.institutionName
            tvDistance.text = t.distance
            tvSubjects.text = t.type
            tvMoney.text = itemView.context.getString(R.string.money_limit,t.price)
            tvAddress.text = t.address

            itemView.setOnClickListener {
                var intent = Intent(itemView.context, CompanyInfoActivity::class.java)
                intent.putExtra(CompanyInfoActivity.ID,t.userId)
                itemView.context.startActivity(intent)
            }
        }
    }
}