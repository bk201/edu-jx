package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.CompanyInfo
import com.jinghan.app.mvp.model.bean.StudentInfo
import com.jinghan.app.mvp.model.bean.TeacherInfo
import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/2/12    下午1:54
 * @mail lzr319@163.com
 */
interface IEditInfoActivityView : BaseView{

    fun onStudentInfo(info: StudentInfo)

    fun onTeacherInfo(info:TeacherInfo)

    fun onCompanyInfo(info:CompanyInfo)

}