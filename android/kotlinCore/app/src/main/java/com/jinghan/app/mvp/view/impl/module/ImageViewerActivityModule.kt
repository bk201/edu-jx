package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.view.fragment.ImageFragment
import com.jinghan.core.dependencies.dragger2.scope.FragmentScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author liuzeren
 * @time 2018/3/1    下午2:50
 * @mail lzr319@163.com
 */
@Module
abstract class ImageViewerActivityModule{
    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun imageFragment() : ImageFragment
}