package com.jinghan.app.mvp.view.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.jinghan.app.mvp.view.activity.ImageViewerActivity;
import com.jinghan.core.R;
import com.jinghan.core.databinding.FrgImageBinding;
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped;
import com.jinghan.core.helper.ImageUtil;
import com.jinghan.core.mvp.view.fragment.BaseFragment;
import com.jinghan.core.mvp.widget.scaleimageview.ImageSource;
import com.jinghan.core.mvp.widget.scaleimageview.ScaleImageView;

import javax.inject.Inject;

/**
 * @author liuzeren
 * @time 2018/2/13 16:33
 * @mail lzr319@163.com
 */
@ActivityScoped
public class ImageFragment extends BaseFragment<FrgImageBinding> {

    private String path = "";

    @Inject
    public ImageFragment(){}

    @Override
    public void initData() {

        Bundle bundle = getArguments();
        if(null == bundle || !bundle.containsKey(ImageViewerActivity.PATH)){
            return;
        }

        path = bundle.getString(ImageViewerActivity.PATH);

        Glide.with(this).load(path).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                Bitmap bitmap = ImageUtil.drawable2Bitmap(resource);
                if(null != bitmap) {
                    binding.ivImage.setImage(ImageSource.bitmap(bitmap));
                }
            }

            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                super.onLoadFailed(e, errorDrawable);
            }
        });
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_image;
    }
}