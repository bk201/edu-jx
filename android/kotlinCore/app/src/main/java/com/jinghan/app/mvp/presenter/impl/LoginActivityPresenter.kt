package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.model.response.LoginResponse
import com.jinghan.app.mvp.presenter.ILoginActivityPresenter
import com.jinghan.app.mvp.view.impl.api.UserService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/1/24    下午7:20
 * @mail lzr319@163.com
 */
class LoginActivityPresenter @Inject constructor() : ILoginActivityPresenter(){


    override fun login(phone: String, pwd: String) {

        mView?.let{it.showLoading("登录中...")}

        mOkHttp.retrofit.builder(UserService::class.java).login(phone,pwd)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<LoginResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let { it.toast(R.string.request_failure) }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : LoginResponse) {
                        if(response.isSuccess){
                            mView?.let{it.loginResult(true,response.value)}
                        }else{
                            mView?.let { it.toast(response.msg) }
                        }
                    }
                })
    }

}
