package com.jinghan.core.mvp.view

import android.support.v7.widget.RecyclerView

/**
 * @author liuzeren
 * @time 2018/1/27    上午11:40
 * @mail lzr319@163.com
 */
abstract class BaseAdapter<T : BaseViewHolder<F>,F> constructor(var data:ArrayList<F> = ArrayList()) : RecyclerView.Adapter<T>(){

    fun setDataSource(data:ArrayList<F>){
        this.data = data
        notifyDataSetChanged()
    }

    fun appendData(item:ArrayList<F>){
        data.addAll(item)
        notifyDataSetChanged()
    }

    fun appendData(item:F){
        data.add(item)
        notifyDataSetChanged()
    }

    fun clear(){
        data.clear()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: T, position: Int) {
        holder.update(data.get(position))
    }

    override fun getItemCount(): Int {
        if(null == data){
            return 0
        }

        return data.size
    }
}