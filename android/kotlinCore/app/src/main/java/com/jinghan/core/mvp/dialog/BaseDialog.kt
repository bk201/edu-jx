package com.jinghan.core.mvp.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import com.jinghan.core.R
import com.jinghan.core.helper.PermissionUtils

/**
 * @author liuzeren
 * @time 2018/1/27    下午1:12
 * @mail lzr319@163.com
 */
open class BaseDialog(mContext:Context,cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener?=null,protected var layoutParams: WindowManager.LayoutParams? = null)
    : Dialog(mContext,cancelable,cancelListener) , PermissionUtils.OnPermissionListener{
    override fun onPermissionGranted(reqCode:Int) {

    }

    override fun onPermissionDenied() {
    }

    init {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window!!.setBackgroundDrawableResource(R.drawable.transparent_bg)
        val window = this.window
        layoutParams = window!!.attributes
        layoutParams!!.alpha = 1f
        window.attributes = layoutParams
        if (layoutParams != null) {
            layoutParams!!.height = android.view.ViewGroup.LayoutParams.MATCH_PARENT
            layoutParams!!.gravity = Gravity.CENTER
        }
    }

    /**
     * 隐藏头部导航栏状态栏
     */
    fun skipTools() {
        if (Build.VERSION.SDK_INT < 19) {
            return
        }
        window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    /**
     * 设置全屏显示
     */
    fun setFullScreen() {
        val window = window
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = WindowManager.LayoutParams.FILL_PARENT
        lp.height = WindowManager.LayoutParams.FILL_PARENT
        window.attributes = lp
    }

    /**
     * 设置宽度match_parent
     */
    fun setFullScreenWidth() {
        val window = window
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = WindowManager.LayoutParams.FILL_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
    }

    /**
     * 设置高度为match_parent
     */
    fun setFullScreenHeight() {
        val window = window
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.FILL_PARENT
        window.attributes = lp
    }

    fun setOnWhole() {
        window!!.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
    }
}