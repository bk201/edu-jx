package com.jinghan.app.mvp.view.activity

import android.content.Context
import android.content.Intent
import android.view.View
import com.jinghan.app.mvp.presenter.IPayActivityPresenter
import com.jinghan.app.mvp.view.impl.view.IPayActivityView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyPayBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * 支付
 * @author liuzeren
 * @time 2018/3/16 20:13
 * @mail lzr319@163.com
 */
class PayActivity : BaseActivity<AtyPayBinding>(),IPayActivityView,View.OnClickListener{
    override fun onClick(v: View) {
        presenter.pay(orderNo)
    }

    private var orderNo:String=""

    override val layoutId: Int
        get() = R.layout.aty_pay

    @Inject
    protected lateinit var presenter:IPayActivityPresenter

    companion object {
        val QUESTION_NO ="QUESTION_NO"
        val PRICE = "PRICE"

        fun pay(context:Context,questionNo:String,price:Int){
            var intent = Intent(context,PayActivity::class.java)
            intent.putExtra(QUESTION_NO,questionNo)
            intent.putExtra(PRICE,price)

            context.startActivity(intent)
        }
    }

    override fun initViewsAndListener() {

        if(intent.hasExtra(QUESTION_NO)){
            orderNo = intent.getStringExtra(QUESTION_NO)
            mViewBinding.tvOrderNo.text = getString(R.string.service_order_no,orderNo)
        }

        var price = 0

        if(intent.hasExtra(PRICE)){
            price = intent.getIntExtra(PRICE,0)
        }

        mViewBinding.tvMoney.text = getString(R.string.money,price)
        mViewBinding.onClick = this

        mViewBinding.root.toolBar.tvTitle.text = "支付"
    }

    override fun initData() {
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}