package com.jinghan.core.helper.setting.impl

import android.support.annotation.StringRes
import com.jinghan.app.AppContext
import com.jinghan.core.helper.SharedPrefsUtils
import com.jinghan.core.helper.setting.SettingsEntry

/**
 * @author liuzeren
 * @time 2018/1/24    下午8:57
 * @mail lzr319@163.com
 */
class StringSettingsEntry constructor(@StringRes keyResId:Int, defaultValueResId:Int ) : SettingsEntry<String>(keyResId,defaultValueResId) {
    override fun getValue(): String {
        return SharedPrefsUtils.getString(this)
    }

    override fun putValue(value: String) {
        SharedPrefsUtils.putString(this, value)
    }

    override fun getDefaultValue(): String {
        return AppContext.instance.getString(defaultValueResId)
    }
}