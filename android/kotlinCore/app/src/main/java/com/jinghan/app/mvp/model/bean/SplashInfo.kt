package com.jinghan.app.mvp.model.bean

/**
 * 欢迎页信息体
 * @author liuzeren
 * @time 2017/11/9    上午11:06
 * @mail lzr319@163.com
 */
data class SplashInfo(var initialImageUrl: String,var linkUrl: String,var countDown: Int)