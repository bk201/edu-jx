package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.CompanyListInfo

/**
 * @author liuzeren
 * @time 2018/3/11    上午10:13
 * @mail lzr319@163.com
 */
data class CompanyDetailResponse(var value:CompanyListInfo) : BaseResponse()