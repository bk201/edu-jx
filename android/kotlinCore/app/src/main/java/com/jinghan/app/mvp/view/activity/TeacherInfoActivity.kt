package com.jinghan.app.mvp.view.activity

import android.text.Html
import android.view.View
import com.jinghan.app.mvp.model.bean.TeacherListInfo
import com.jinghan.app.mvp.presenter.ITeacherInfoActivityPresenter
import com.jinghan.app.mvp.view.impl.view.ITeacherInfoActivityView
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyTeacherInfoBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * 老师详情
 * @author liuzeren
 * @time 2018/3/10    下午9:47
 * @mail lzr319@163.com
 */
class TeacherInfoActivity : BaseActivity<AtyTeacherInfoBinding>(),View.OnClickListener,ITeacherInfoActivityView{

    companion object {
        val ID = "id"
    }

    /**老师id*/
    private var teacherId:Int = 0

    @Inject
    protected lateinit var presenter:ITeacherInfoActivityPresenter


    override fun onDetail(info: TeacherListInfo) {
        mViewBinding.onClick = this
        mViewBinding.info = info

        mViewBinding.tvCount.text = getString(R.string.count_of,0)
        mViewBinding.tvFaceToFace.text = Html.fromHtml(getString(R.string.red_count_of,info.visitTeachCharge))
        mViewBinding.tvVideo.text = Html.fromHtml(getString(R.string.red_count_of,info.vedioTeachCharge))

        var times:String = "0"
        if(info.evaluateTime > 99){
            times = "99+"
        }else{
            times = ""+info.evaluateTime
        }

        mViewBinding.tvComment.text = times
    }

    override fun onClick(v: View) {

        when(v.id){
            R.id.tvComment->{
                //评论
            }
            R.id.btnOrderFace->{
                //上门教学，立即下单
            }
            R.id.btnAskTeacherFace->{
                //上门教学、咨询教师
            }
            R.id.btnOrderVideo->{
                //视频教学，立即下单
            }
            R.id.btnAskTeacherVideo->{
                //视频教学，咨询教师
            }
        }
    }

    override val layoutId: Int
        get() = R.layout.aty_teacher_info

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
    }

    override fun initData() {
        if(intent.hasExtra(ID)){
            teacherId = intent.getIntExtra(ID,0)
        }

        presenter.requestTeacherInfo(teacherId)
    }

    override fun initToolbar() {
        super.initToolbar()
        mViewBinding.root.toolBar.tvTitle.text = "老师详情"
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}