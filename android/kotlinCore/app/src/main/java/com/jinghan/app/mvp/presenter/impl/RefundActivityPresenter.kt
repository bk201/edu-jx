package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.presenter.IRefundActivityPresenter
import com.jinghan.app.mvp.view.impl.api.QuestionService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/3/11    下午8:59
 * @mail lzr319@163.com
 */
class RefundActivityPresenter @Inject constructor() : IRefundActivityPresenter(){
    override fun refund(pics: ArrayList<String>?, questionNo: String, reason: String) {
        mView?.let { it.showLoading("正在提交退款申请~") }



        mOkHttp.retrofit.builder(QuestionService::class.java).refund(RefundInfo(pics,questionNo,reason))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let {
                            it.toast(R.string.request_failure)
                        }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{it.toast("退款申请已提交")}
                            mView?.let{it.close()}
                        }else{
                            mView?.let {
                                it.toast(response.msg)
                            }
                        }
                    }
                })
    }


    /**退款实体类*/
    data class RefundInfo(
            var pics:ArrayList<String>?,
            var questionNo:String,
            var reason:String
    )

}