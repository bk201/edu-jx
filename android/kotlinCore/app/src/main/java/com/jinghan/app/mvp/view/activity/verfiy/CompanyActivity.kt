package com.jinghan.app.mvp.view.activity.verfiy

import android.content.Intent
import android.text.TextUtils
import android.view.View
import com.bumptech.glide.Glide
import com.jinghan.app.mvp.view.activity.BaseChooseImageActivity
import com.jinghan.core.databinding.AtyCompanyBinding
import com.jinghan.core.R
import com.jinghan.core.dependencies.http.model.UploadProgressListener
import kotlinx.android.synthetic.main.view_toolbar.view.*
import java.util.ArrayList

/**
 * 机构认证
 * @author liuzeren
 * @time 2018/1/28    下午9:04
 * @mail lzr319@163.com
 */
class CompanyActivity constructor() : BaseChooseImageActivity<AtyCompanyBinding>(), View.OnClickListener{

    /**本地图片*/
    private var imagePath = ""

    /**上传后的图片*/
    private var imageUrl = ""

    override fun onUploadResult(imgList: ArrayList<String>) {
    }

    override fun onUploadResult(result: Boolean, image: String) {
        if(result){
            imageUrl = image

            toNextVerfiy()
        }
    }

    override fun initPath(path: String) {
        imagePath = path
        imageUrl = ""

        Glide.with(this).load(path).centerCrop().into(mViewBinding.ivImage)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnNext -> next()
            R.id.viewAdd -> addImage()
        }
    }

    /**添加营业执照*/
    private fun addImage(){
        dialog?.show()
    }

    /**下一步*/
    private fun next(){
        if(TextUtils.isEmpty(mViewBinding.etCredit.text.toString())){
            toast(mViewBinding.etCredit.hint.toString())
            return
        }

        if(TextUtils.isEmpty(imageUrl)){
            if(TextUtils.isEmpty(imagePath)){
                toast("请先上传企业营业执照")
                return
            }else{
                presenter.uploadImage(imagePath, object : UploadProgressListener {
                    override fun onProgress(bytesWritten: Long, contentLength: Long) {

                    }
                })
            }
        }else{
            toNextVerfiy()
        }
    }

    companion object {
        val COMPANY_IMAGE = "company_image"
        val COMPANY_CODE = "company_code"
    }

    private fun toNextVerfiy(){
        var it : Intent? = intent

        it?.putExtra(COMPANY_IMAGE,imageUrl)
        it?.putExtra(COMPANY_CODE,mViewBinding.etCredit.text.toString())
        it?.setClass(this,AlipayActiviity::class.java)

        startActivity(intent)
    }

    override val layoutId: Int
        get() = R.layout.aty_company

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
        mViewBinding.root.toolBar.tvTitle.text = "机构认证"
    }

}