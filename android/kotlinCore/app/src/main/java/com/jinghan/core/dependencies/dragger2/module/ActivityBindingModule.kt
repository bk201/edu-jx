package com.jinghan.core.dependencies.dragger2.module

import com.jinghan.app.mvp.view.activity.*
import com.jinghan.app.mvp.view.activity.verfiy.*
import com.jinghan.app.mvp.view.impl.module.*
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author liuzeren
 * @time 2017/11/2    下午3:14
 * @mail lzr319@163.com
 */
@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(SplashActivityModule::class))
    internal abstract fun splashActivity(): SplashActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(HomeActivityModule::class))
    internal abstract fun homeActivity(): HomeActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(LoginActivityModule::class))
    internal abstract fun loginActivity(): LoginActivity


    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(RegisterActivityModule::class))
    internal abstract fun registerActivity(): RegisterActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(UserInfoActivityModule::class))
    internal abstract fun userInfoActivity(): UserInfoActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(QuestionInfoActivityModule::class))
    internal abstract fun questionInfoActivity(): QuestionInfoActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(RefundActivityModule::class))
    internal abstract fun refundActivity(): RefundActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(OrderListActivityModule::class))
    internal abstract fun orderListActivity(): OrderListActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(CompanyActivityModule::class))
    internal abstract fun companyActivity(): CompanyActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(IdCardActivityModule::class))
    internal abstract fun idCardActivity(): IdCardActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(EducationActivityModule::class))
    internal abstract fun educationActivity(): EducationActivity

    @ActivityScoped
    @ContributesAndroidInjector()
    internal abstract fun verfiySuccessActivity(): VerfiySuccessActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(EMailActivityModule::class))
    internal abstract fun eMailActiviity(): EMailActiviity

    @ActivityScoped
    @ContributesAndroidInjector()
    internal abstract fun alipayActiviity(): AlipayActiviity

    @ActivityScoped
    @ContributesAndroidInjector()
    internal abstract fun subjectListActivity(): SubjectListActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(IEditInfoActivityModule::class))
    internal abstract fun editInfoActivity(): EditInfoActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(ImageViewerActivityModule::class))
    internal abstract fun imageViewerActivity(): ImageViewerActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(TeacherInfoActivityModule::class))
    internal abstract fun teacherInfoActivity(): TeacherInfoActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(CompanyInfoActivityModule::class))
    internal abstract fun companyInfoActivity(): CompanyInfoActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(AnswerQuestionActivityModule::class))
    internal abstract fun answerQuestionActivity(): AnswerQuestionActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(EvaluateActivityModule::class))
    internal abstract fun evaluateActivity(): EvaluateActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(PayActivityModule::class))
    internal abstract fun payActivity(): PayActivity


    @ActivityScoped
    @ContributesAndroidInjector(modules = arrayOf(MyAccountActivityModule::class))
    internal abstract fun myAccountActivity(): MyAccountActivity
}