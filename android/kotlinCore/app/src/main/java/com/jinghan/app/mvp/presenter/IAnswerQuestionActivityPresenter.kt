package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IAnswerQuestionActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/3/11    下午9:38
 * @mail lzr319@163.com
 */
abstract class IAnswerQuestionActivityPresenter : BaseLifecyclePresenter<IAnswerQuestionActivityView,ActivityEvent>(){

    /**解答*/
    abstract fun answer(description:String,
                        pics :ArrayList<String>,
                        questionNo:String)

}