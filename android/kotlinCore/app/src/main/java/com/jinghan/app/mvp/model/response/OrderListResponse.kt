package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.OrderInfo
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/11    下午3:37
 * @mail lzr319@163.com
 */
class OrderListResponse constructor(var values:ArrayList<OrderInfo>) : BasePageResponse()