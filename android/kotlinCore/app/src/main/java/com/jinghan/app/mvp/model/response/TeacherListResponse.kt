package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.TeacherInfo
import com.jinghan.app.mvp.model.bean.TeacherListInfo
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/13    上午10:40
 * @mail lzr319@163.com
 */
class TeacherListResponse constructor(var values: ArrayList<TeacherListInfo>) : BaseResponse()