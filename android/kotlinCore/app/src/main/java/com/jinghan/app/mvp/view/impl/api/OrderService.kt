package com.jinghan.app.mvp.view.impl.api

import com.jinghan.app.mvp.model.bean.CourseDetail
import com.jinghan.app.mvp.model.bean.CourseInfo
import com.jinghan.app.mvp.model.bean.EvaluateInfo
import com.jinghan.app.mvp.model.bean.PlatformInfo
import com.jinghan.app.mvp.model.response.*
import io.reactivex.Observable
import retrofit2.http.*

/**
 * @author liuzeren
 * @time 2018/3/14 11:23
 * @mail lzr319@163.com
 */
interface OrderService {

    /**取消订单*/
    @POST("question/user/cancel-order")
    fun cancelOrder(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**取消退款*/
    @POST("question/user/cancel-refund")
    fun cancelRefund(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**联系老师*/
    @GET("question/user/contact")
    fun contactTeacher(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**立即评价*/
    @POST("question/user/evaluate")
    fun evaluate(@Body evaluate:EvaluateInfo): Observable<BaseResponse>

    /**完成订单*/
    @POST("question/user/finish")
    fun finishOrder(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**立即付款*/
    @GET("question/user/pay")
    fun pay(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**用户回答列表*/
    @GET("question/query-answer")
    fun queryAnswer(@Query("status") status :Int,
                    @Query("pageNo") pageNo :Int,
                    @Query("pageSize") pageSize  :Int):Observable<ReplayInfoListResponse>

    /**平台介入*/
    @POST("question/user/platform")
    fun platform(@Body info: PlatformInfo): Observable<BaseResponse>

    /**申请退款*/
    @POST("question/user/refund")
    fun refund(@Body info: PlatformInfo): Observable<BaseResponse>

    /**同意退款*/
    @POST("question/teacher/approve-refund")
    fun approveRefund(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**联系客户*/
    @GET("question/teacher/contact")
    fun contactStudent(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**查看评价*/
    @GET("question/teacher/look-evaluate")
    fun lookEvaluate(@Query("questionNo ") questionNo :String): Observable<BaseResponse>

    /**拒绝退款*/
    @POST("question/teacher/refuse-refund")
    fun refuseRefund(@Body info:PlatformInfo): Observable<BaseResponse>

    /**添加套餐*/
    @PUT("institution/add-course")
    fun addCourse(@Body info: CourseInfo): Observable<AddCourseInfoResponse>

    /**删除套餐*/
    @DELETE("institution/del-course")
    fun delCourse(@Query("mealId") mealId:Int): Observable<BaseResponse>

    /**获取套餐列表*/
    @GET("institution/find-course")
    fun findCourse(@Query("pageNo") pageNo :Int,@Query("pageSize") pageSize  :Int): Observable<CourseInfoListResponse>

    /**获取套餐明细*/
    @GET("institution/get-course")
    fun findCourse(@Query("mealId ") mealId  :Int): Observable<CourseDetailResponse>

    /**更新套餐*/
    @POST("institution/update-course")
    fun updateCourse(@Body info:CourseDetail): Observable<BaseResponse>

    /**支付订单*/
    @POST("user/pay-order")
    fun payOrder(@Query("orderNo") orderNo:String): Observable<BaseResponse>

    /**支付提问订单*/
    @POST("user/pay-question")
    fun payQuestion(@Query("questionNo") questionNo :String): Observable<BaseResponse>

    /**
     * 用户查看自己订单列表
     * 状态 status 字段说明, 状态,0:待消费 1:已消费,2:待评价,3:退款, -1 代表所有订单
     * */
    @GET("user/query-order")
    fun queryOrder(@Query("status") status  :Int,@Query("pageNo") pageNo :Int,@Query("pageSize") pageSize  :Int): Observable<OrderShowInfoListResponse>

    /**用户对机构套餐下单*/
    @POST("user/submit-order-meal")
    fun submitOrderMeal(@Query("mealId") mealId  :Int,@Query("goodsNum") goodsNum:Int): Observable<OrderMealResponse>

    /**
     * type教学类型,0:视频教学,1:上门教学
     * 用户对老师教学下单*/
    @POST("user/submit-order-teacher")
    fun submitOrderTeacher(@Query("teacherId") teacherId   :Int,@Query("goodsNum") goodsNum:Int,@Query("type") type :Int): Observable<OrderTeacherResponse>

    /**
     * 老师或机构查看自己订单列表
     * */
    @GET("seller/query-order")
    fun teacherQueryOrder(@Query("status") status  :Int,@Query("pageNo") pageNo :Int,@Query("pageSize") pageSize  :Int): Observable<OrderShowInfoListResponse>

}
