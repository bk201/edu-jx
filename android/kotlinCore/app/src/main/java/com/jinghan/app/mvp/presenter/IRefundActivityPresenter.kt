package com.jinghan.app.mvp.presenter

import com.jinghan.app.mvp.view.impl.view.IRefundActivityView
import com.jinghan.core.mvp.preseneter.BaseLifecyclePresenter
import com.trello.rxlifecycle2.android.ActivityEvent

/**
 * @author liuzeren
 * @time 2018/3/11    下午8:55
 * @mail lzr319@163.com
 */
abstract class IRefundActivityPresenter : BaseLifecyclePresenter<IRefundActivityView,ActivityEvent>(){

    public abstract fun refund(pics:ArrayList<String>?,
                            questionNo:String,
                            reason:String)

}