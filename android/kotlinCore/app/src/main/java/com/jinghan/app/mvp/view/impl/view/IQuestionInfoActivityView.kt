package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.QuestionListInfo
import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/2/28    下午2:46
 * @mail lzr319@163.com
 */
interface IQuestionInfoActivityView : BaseView{

    fun onDetail(info:QuestionListInfo)

    /**抢单结果*/
    fun catchResult(isSuccess:Boolean)

}