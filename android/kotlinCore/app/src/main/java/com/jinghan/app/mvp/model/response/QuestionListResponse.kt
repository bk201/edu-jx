package com.jinghan.app.mvp.model.response

import com.jinghan.app.mvp.model.bean.QuestionListInfo
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/10    下午9:30
 * @mail lzr319@163.com
 */
class QuestionListResponse constructor(var values:ArrayList<QuestionListInfo>) : BasePageResponse()