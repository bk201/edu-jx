package com.jinghan.app.mvp.model.bean

/**
 * 机构信息
 * @author liuzeren
 * @time 2018/2/11    下午5:34
 * @mail lzr319@163.com
 */
data class CompanyInfo(var address:String,
                       var city:String,
                       var cityCode:String,
                       var country:String,
                       var introduce:String,
                       var latitude:Double,
                       var longitude:Double,
                       var mobile:String,
                       var name:String,
                       var region:String,
                       var regionCode:String,
                       var state:String){




    /*{
address (string, optional): 发布问题时的地理位置信息,地址 ,
city (string, optional): 发布问题时的地理位置信息,城市 ,
cityCode (string, optional): 发布问题时的地理位置信息,城市编码,如 025 ,
country (string, optional): 发布问题时的地理位置信息,国家 ,
introduce (string, optional): 机构介绍 ,
latitude (number, optional): 发布问题时的地理位置信息,纬度 ,
longitude (number, optional): 发布问题时的地理位置信息,经度 ,
mobile (string, optional): 机构电话 ,
name (string, optional): 机构名称 ,
region (string, optional): 发布问题时的地理位置信息,区,如 雨花区 ,
regionCode (string, optional): 发布问题时的地理位置信息, 区域码 如 320114 ,
state (string, optional): 发布问题时的地理位置信息,省
}*/
}