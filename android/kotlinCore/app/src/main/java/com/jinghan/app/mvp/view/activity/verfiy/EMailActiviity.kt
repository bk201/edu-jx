package com.jinghan.app.mvp.view.activity.verfiy

import android.content.Intent
import android.text.TextUtils
import android.view.View
import com.jinghan.app.mvp.presenter.IVerfiyPresenter
import com.jinghan.app.mvp.view.impl.view.IVerfiyView
import com.jinghan.core.databinding.AtyEmailBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import kotlinx.android.synthetic.main.view_toolbar.view.*
import javax.inject.Inject

/**
 * 邮箱验证
 * @author liuzeren
 * @time 2018/1/29    下午8:38
 * @mail lzr319@163.com
 */
class EMailActiviity constructor(override val layoutId: Int = R.layout.aty_email) : BaseActivity<AtyEmailBinding>()
        , View.OnClickListener,IVerfiyView{
    override fun onVerfiyResult() {
        startActivity(Intent(this,VerfiySuccessActivity::class.java))
    }

    companion object {
        val EMAIL = "eMail"
    }

    @Inject
    protected lateinit var presnter:IVerfiyPresenter

    override fun onClick(v: View?) {
        //next
        if(TextUtils.isEmpty(mViewBinding.etEMail.text.toString())){
            toast(mViewBinding.etEMail.hint.toString())
            return
        }

        var it = intent

        var type = it.getIntExtra(IdCardActivity.VERFIY_TYPE,IdCardActivity.TO_TEACHER)

        when(type){
            IdCardActivity.TO_TEACHER->toTeacher(it)
            IdCardActivity.TO_COMPANY->toCompany(it)
        }
    }

    private fun toTeacher(intent:Intent){

        //身份证信息
        var idCard = intent.getStringExtra(IdCardActivity.ID_CODE)
        var idFrontImage = intent.getStringExtra(IdCardActivity.FRONT_IMAGE)
        var idBackImage = intent.getStringExtra(IdCardActivity.BACK_IMAGE)

        //学位信息
        var educationId = intent.getStringExtra(EducationActivity.EDUCATION_CODE)
        var educationFrontImage = intent.getStringExtra(EducationActivity.EDUCATION_FRONT)
        var educationBackImage = intent.getStringExtra(EducationActivity.EDUCATION_BACK)

        //支付宝信息
        var alipayCode = intent.getStringExtra(AlipayActiviity.ALIPAY_CODE)
        var alipayName = intent.getStringExtra(AlipayActiviity.ALIPAY_NAME)

        //邮箱信息
        var email = mViewBinding.etEMail.text.toString()

        presnter.teacherVerfiy(alipayCode,email,idBackImage,idFrontImage,idCard,alipayName,educationBackImage,educationFrontImage,educationId)
    }

    private fun toCompany(intent:Intent){

        //支付宝信息
        var alipayCode = intent.getStringExtra(AlipayActiviity.ALIPAY_CODE)
        var alipayName = intent.getStringExtra(AlipayActiviity.ALIPAY_NAME)

        //机构信息
        var companyId = intent.getStringExtra(CompanyActivity.COMPANY_CODE)
        var companyImage = intent.getStringExtra(CompanyActivity.COMPANY_IMAGE)

        //邮箱信息
        var email = mViewBinding.etEMail.text.toString()

        presnter.companyVerfiy(alipayCode,email,companyId,companyImage,alipayName)
    }

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
        mViewBinding.root.toolBar.tvTitle.text = "电子邮箱认证"
    }

    override fun initData() {
    }

    override fun initPresenter() {
        presnter.takeView(this)
        presnter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()
        presnter.dropView()
    }
}