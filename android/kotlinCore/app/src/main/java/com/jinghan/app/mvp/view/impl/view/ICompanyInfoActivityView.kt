package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.CompanyListInfo
import com.jinghan.core.mvp.view.impl.view.BaseView

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:48
 * @mail lzr319@163.com
 */
interface ICompanyInfoActivityView : BaseView{

    fun onDetail(info:CompanyListInfo)

}