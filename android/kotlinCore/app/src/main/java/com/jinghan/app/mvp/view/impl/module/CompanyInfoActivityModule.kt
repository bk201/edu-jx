package com.jinghan.app.mvp.view.impl.module

import com.jinghan.app.mvp.presenter.ICompanyInfoActivityPresenter
import com.jinghan.app.mvp.presenter.impl.CompanyInfoActivityPresenter
import com.jinghan.core.dependencies.dragger2.scope.ActivityScoped
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/3/11    上午9:51
 * @mail lzr319@163.com
 */
@Module
abstract class CompanyInfoActivityModule{
    @ActivityScoped
    @Binds
    internal abstract fun companyInfoActivityPresenter(presenter: CompanyInfoActivityPresenter): ICompanyInfoActivityPresenter

}