package com.jinghan.app.mvp.view.impl.view

import com.jinghan.app.mvp.model.bean.CompanyListInfo
import com.jinghan.core.mvp.view.impl.view.BaseView
import java.util.ArrayList

/**
 * @author liuzeren
 * @time 2018/2/11    下午5:23
 * @mail lzr319@163.com
 */
interface ICompanyListFragmentView : BaseView{

    fun stopRefresh()

    fun onResult(isRefresh:Boolean,data:ArrayList<CompanyListInfo>)

}