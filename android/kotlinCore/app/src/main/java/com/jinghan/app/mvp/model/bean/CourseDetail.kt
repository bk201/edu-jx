package com.jinghan.app.mvp.model.bean

/**
 * 套餐明细
 * @author liuzeren
 * @time 2018/3/14 13:34
 * @mail lzr319@163.com
 */
data class CourseDetail(var courseCount:Int,
var courseName:String,
var courseTime:String,
var id:Int,
var introduce:String,
var pics:Array<String>,
var price:Int,
var teacher:String
)