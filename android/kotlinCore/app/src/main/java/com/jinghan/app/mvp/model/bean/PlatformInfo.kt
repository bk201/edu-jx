package com.jinghan.app.mvp.model.bean

/**
 * 平台介入
 * 原因,图片列表,问题单号
 * @author liuzeren
 * @time 2018/3/14 11:29
 * @mail lzr319@163.com
 */
data class PlatformInfo(var reason:String, var pics:Array<String>, var questionNo:String)