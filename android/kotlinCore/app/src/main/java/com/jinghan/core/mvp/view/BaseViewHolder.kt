package com.jinghan.core.mvp.view

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * @author liuzeren
 * @time 2018/1/27    上午11:40
 * @mail lzr319@163.com
 */
abstract class BaseViewHolder<T> constructor(itemView: View) : RecyclerView.ViewHolder(itemView){

    abstract fun update(t:T)

}