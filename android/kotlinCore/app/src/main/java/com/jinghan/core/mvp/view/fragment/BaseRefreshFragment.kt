package com.jinghan.core.mvp.view.fragment

import android.databinding.ViewDataBinding
import com.aspsine.swipetoloadlayout.OnLoadMoreListener
import com.aspsine.swipetoloadlayout.OnRefreshListener
import com.aspsine.swipetoloadlayout.SwipeToLoadLayout

/**
 * @author liuzeren
 * @time 2018/2/3    下午2:51
 * @mail lzr319@163.com
 */
abstract class BaseRefreshFragment<B: ViewDataBinding> : BaseFragment<B>(), OnRefreshListener, OnLoadMoreListener {
    protected var swipeToLoadLayout : SwipeToLoadLayout?=null

    fun stopRefresh(){
        if(swipeToLoadLayout?.isRefreshing==true){
            swipeToLoadLayout?.isRefreshing = false
        }

        if(swipeToLoadLayout?.isLoadingMore==true){
            swipeToLoadLayout?.isLoadingMore = false
        }
    }

    override fun onLoadMore() {

    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)

        if(hidden) stopRefresh()
    }

    override fun onDestroy() {
        super.onDestroy()

        stopRefresh()
    }
}