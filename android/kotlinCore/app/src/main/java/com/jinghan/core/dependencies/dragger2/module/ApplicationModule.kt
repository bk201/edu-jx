package com.jinghan.core.dependencies.dragger2.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

/**
 * @author liuzeren
 * @time 2018/2/8    下午9:53
 * @mail lzr319@163.com
 */
@Module
abstract class ApplicationModule{

    @Binds
    abstract fun bindContext(app: Application): Context
}