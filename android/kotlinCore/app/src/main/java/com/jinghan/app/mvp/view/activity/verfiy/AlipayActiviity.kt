package com.jinghan.app.mvp.view.activity.verfiy

import android.text.TextUtils
import android.view.View
import com.jinghan.core.databinding.AtyEmailBinding
import com.jinghan.core.mvp.view.activity.BaseActivity
import com.jinghan.core.R
import com.jinghan.core.databinding.AtyAlipayBinding
import kotlinx.android.synthetic.main.aty_alipay.view.*
import kotlinx.android.synthetic.main.view_toolbar.view.*

/**
 * 支付宝验证
 * @author liuzeren
 * @time 2018/1/29    下午8:38
 * @mail lzr319@163.com
 */
class AlipayActiviity constructor(override val layoutId: Int = R.layout.aty_alipay) : BaseActivity<AtyAlipayBinding>(), View.OnClickListener{

    companion object {

        /**姓名*/
        val ALIPAY_NAME = "alipay_name"

        /**账号*/
        val ALIPAY_CODE = "alipay_code"
    }

    override fun onClick(v: View) {
        //next
        if(TextUtils.isEmpty(mViewBinding.etName.text.toString())){
            toast(mViewBinding.etName.hint.toString())
            return
        }

        if(TextUtils.isEmpty(mViewBinding.etCode.text.toString())){
            toast(mViewBinding.etCode.hint.toString())
            return
        }

        var it = intent

        it.putExtra(ALIPAY_NAME,mViewBinding.etName.text.toString())
        it.putExtra(ALIPAY_CODE,mViewBinding.etCode.text.toString())
        it.setClass(this,EMailActiviity::class.java)

        startActivity(it)
    }

    override fun initViewsAndListener() {
        mViewBinding.onClick = this
        mViewBinding.root.toolBar.tvTitle.text = "支付宝认证"
    }

    override fun initData() {
    }

    override fun initPresenter() {
    }
}