package com.jinghan.core.helper;

import android.content.Context;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.jinghan.app.AppContext;
import com.orhanobut.logger.Logger;

import javax.inject.Singleton;

/**
 * @author liuzeren
 * @time 2018/2/10    上午9:41
 * @mail lzr319@163.com
 */
@Singleton
public class LocationHelper implements AMapLocationListener {

    private static LocationHelper instance;

    private Context context;

    private AMapLocationListener listener;

    public void setListener(AMapLocationListener listener) {
        this.listener = listener;
    }

    private LocationHelper(Context context){
        this.context = context;
        init();
    }

    public static LocationHelper getInstance(Context context){
        if(null == instance){
            synchronized (LocationHelper.class){
                if(null == instance){
                    instance = new LocationHelper(context);
                }
            }
        }

        return instance;
    }

    /**声明AMapLocationClient类对象*/
    private AMapLocationClient mLocationClient;

    /**声明AMapLocationClientOption对象*/
    private AMapLocationClientOption mLocationOption;

    private AMapLocationClientOption option = new AMapLocationClientOption();

    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (amapLocation != null) {
            if (amapLocation.getErrorCode() == 0) {
                //可在其中解析amapLocation获取相应内容。
                listener.onLocationChanged(amapLocation);
                AppContext.instance.setLocation(amapLocation);

                StringBuilder sb = new StringBuilder();
                //获取当前定位结果来源，如网络定位结果，详见定位类型表
                sb.append("LocationType:").append(amapLocation.getLocationType()).append("\n");
                //获取纬度
                sb.append("Latitude:").append(amapLocation.getLatitude()).append("\n");
                //获取经度
                sb.append("Longitude:").append(amapLocation.getLongitude()).append("\n");
                //获取精度信息
                sb.append("Accuracy:").append(amapLocation.getAccuracy()).append("\n");
                //地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
                sb.append("Address:").append(amapLocation.getAddress()).append("\n");
                //国家信息
                sb.append("Country:").append(amapLocation.getCountry()).append("\n");
                //省信息
                sb.append("Province:").append(amapLocation.getProvince()).append("\n");
                //城市信息
                sb.append("City:").append(amapLocation.getCity()).append("\n");
                //城区信息
                sb.append("District:").append(amapLocation.getDistrict()).append("\n");
                //街道信息
                sb.append("Street:").append(amapLocation.getStreet()).append("\n");
                //街道门牌号信息
                sb.append("StreetNum:").append(amapLocation.getStreetNum()).append("\n");
                //城市编码
                sb.append("CityCode:").append(amapLocation.getCityCode()).append("\n");
                //地区编码
                sb.append("AdCode:").append(amapLocation.getAdCode()).append("\n");
                //获取当前定位点的AOI信息
                sb.append("AoiName:").append(amapLocation.getAoiName()).append("\n");
                //获取当前室内定位的建筑物Id
                sb.append("BuildingId:").append(amapLocation.getBuildingId()).append("\n");
                //获取当前室内定位的楼层
                sb.append("Floor:").append(amapLocation.getFloor()).append("\n");
                //获取GPS的当前状态
                sb.append("GpsAccuracyStatus:").append(amapLocation.getGpsAccuracyStatus()).append("\n");

                Logger.d(sb.toString());
            } else {
                //定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
                Logger.d("location Error, ErrCode:"
                        + amapLocation.getErrorCode() + ", errInfo:"
                        + amapLocation.getErrorInfo());
            }
        }
    }

    private void init() {
        option.setLocationPurpose(AMapLocationClientOption.AMapLocationPurpose.SignIn);

        //初始化定位
        mLocationClient = new AMapLocationClient(AppContext.instance);
        // 设置定位监听
        mLocationClient.setLocationListener(this);

        //初始化AMapLocationClientOption对象
        mLocationOption = new AMapLocationClientOption();
        //设置定位模式为AMapLocationMode.Hight_Accuracy，高精度模式。
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //获取一次定位结果,该方法默认为false。
        mLocationOption.setOnceLocation(true);
        //获取最近3s内精度最高的一次定位结果：
        //设置setOnceLocationLatest(boolean b)接口为true，启动定位时SDK会返回最近3s内精度最高的一次定位结果。如果设置其为true，setOnceLocation(boolean b)接口也会被设置为true，反之不会，默认为false。
        mLocationOption.setOnceLocationLatest(true);
        //设置是否返回地址信息（默认返回地址信息）
        mLocationOption.setNeedAddress(true);

        //设置是否允许模拟位置,默认为true，允许模拟位置
        mLocationOption.setMockEnable(false);

        //单位是毫秒，默认30000毫秒，建议超时时间不要低于8000毫秒。
        mLocationOption.setHttpTimeOut(20000);

        //关闭缓存机制
        mLocationOption.setLocationCacheEnable(false);

        //给定位客户端对象设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
    }

    /**开始定位*/
    public void start() {
        mLocationClient.setLocationOption(option);
        //设置场景模式后最好调用一次stop，再调用start以保证场景模式生效
        mLocationClient.stopLocation();

        //启动定位
        mLocationClient.startLocation();
    }

    /**结束定位*/
    public void stop() {
        mLocationClient.stopLocation();//停止定位后，本地定位服务并不会被销毁

    }

    /**销毁对象*/
    public void destory(){
        stop();
        mLocationClient.onDestroy();//销毁定位客户端，同时销毁本地定位服务。
    }
}
