package com.jinghan.app.mvp.view.activity

import android.databinding.DataBindingUtil
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.amap.api.location.AMapLocation
import com.jinghan.app.helper.LoginHelper
import com.jinghan.app.mvp.model.bean.CompanyInfo
import com.jinghan.app.mvp.model.bean.StudentInfo
import com.jinghan.app.mvp.model.bean.TeacherInfo
import com.jinghan.app.mvp.model.bean.UserInfo
import com.jinghan.app.mvp.presenter.IEditInfoActivityPresenter
import com.jinghan.app.mvp.view.impl.view.IEditInfoActivityView
import com.jinghan.core.R
import com.jinghan.core.databinding.*
import com.jinghan.core.helper.LocationHelper
import com.jinghan.core.mvp.dialog.ValueSelectDialog
import com.jinghan.core.mvp.view.activity.BaseActivity
import kotlinx.android.synthetic.main.aty_edit_info_student.view.*
import kotlinx.android.synthetic.main.aty_user_info.view.*
import java.util.ArrayList
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/2/12    上午11:30
 * @mail lzr319@163.com
 */
class EditInfoActivity : BaseActivity<AtyEditInfoBinding>(), View.OnClickListener,IEditInfoActivityView{

    /**教师*/
    private var mAtyEditInfoTeacherBinding:AtyEditInfoTeacherBinding?=null
    private var teacherInfo:TeacherInfo?=null

    override fun onTeacherInfo(info: TeacherInfo) {
        teacherInfo = info
        mAtyEditInfoTeacherBinding?.teacherInfo = info
    }

    /**机构*/
    private var mAtyEditInfoCompanyBinding:AtyEditInfoCompanyBinding?=null
    private var companyInfo:CompanyInfo?=null

    override fun onCompanyInfo(info: CompanyInfo) {
        companyInfo = info
        mAtyEditInfoCompanyBinding?.companyInfo = info
    }

    /**学生*/
    private var mStudentBinding: AtyEditInfoStudentBinding? = null
    private var studentInfo:StudentInfo?=null

    override fun onStudentInfo(info: StudentInfo) {
        studentInfo = info
        mStudentBinding?.studentInfo = info
        mStudentBinding?.root?.etAge?.setText(info.age.toString())
        mStudentBinding?.root?.etAge?.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(!TextUtils.isEmpty(s.toString())) {
                    try {
                        studentInfo?.age = s.toString() as Int
                    }catch (e:Exception){}
                }else{
                    studentInfo?.age = 0
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }

    @Inject
    protected lateinit var presenter:IEditInfoActivityPresenter

    @Inject
    protected lateinit var helper:LoginHelper

    /**定位所处的位置*/
    private var location : AMapLocation? = null
    private var mLocationHelper:LocationHelper = LocationHelper.getInstance(this)

    override fun onClick(v: View) {
        when(v.id){
            R.id.tvStudentSex ->updateSex()
            R.id.tvCancel->finish()
            R.id.tvSave->save()
        }
    }

    private fun save(){

        when(helper.userInfo?.type){
            UserInfo.student->presenter.editUserInfo(studentInfo)
            UserInfo.teacher->presenter.updateTeacherInfo(teacherInfo)
            UserInfo.company->presenter.updateCompanyInfo(companyInfo)
        }
    }

    private fun updateSex(){
        var sexs = ArrayList<String>()
        sexs.add("保密")
        sexs.add("男")
        sexs.add("女")

        var dialog = ValueSelectDialog(this,sexs,object : ValueSelectDialog.OnItemClickListener{
            override fun onItemClick(value: String) {
                when(helper.userInfo?.type){
                    UserInfo.student->{
                        mViewBinding.viewStudent.root.tvStudentSex.text = value

                        when(value){
                            "保密"->studentInfo?.sex = 0
                            "男"->studentInfo?.sex = 1
                            "女"->studentInfo?.sex = 2
                        }
                    }
                    UserInfo.teacher->{
                    }
                    UserInfo.company->{}
                }
            }
        })
        dialog.show()
    }

    override val layoutId: Int
        get() = R.layout.aty_edit_info

    override fun initViewsAndListener() {
        mViewBinding.onClick = this

        when(helper.userInfo?.type){
            UserInfo.student->{
                mViewBinding.viewStudent.setOnInflateListener { stub, inflated ->
                    mStudentBinding = DataBindingUtil.bind<AtyEditInfoStudentBinding>(inflated)
                    mStudentBinding?.onClick = this@EditInfoActivity
                }
                mViewBinding.viewStudent.viewStub.inflate()
            }
            UserInfo.teacher->{
                mViewBinding.viewTeacher.setOnInflateListener { stub, inflated ->
                    mAtyEditInfoTeacherBinding = DataBindingUtil.bind<AtyEditInfoTeacherBinding>(inflated)
                    mAtyEditInfoTeacherBinding?.onClick = this@EditInfoActivity
                }
                mViewBinding.viewTeacher.viewStub.inflate()
            }
            UserInfo.company->{
                mViewBinding.viewCompany.setOnInflateListener { stub, inflated ->
                    mAtyEditInfoCompanyBinding = DataBindingUtil.bind<AtyEditInfoCompanyBinding>(inflated)
                    mAtyEditInfoCompanyBinding?.onClick = this@EditInfoActivity
                }
                mViewBinding.viewCompany.viewStub.inflate()
            }
        }

        mLocationHelper.setListener { p0 ->
            location = p0

            when(helper.userInfo?.type){
                UserInfo.student->{
                    if(TextUtils.isEmpty(mViewBinding.viewStudent.root.tvCity.text.toString())){
                        mViewBinding.viewStudent.root.tvCity.text = location?.city
                    }
                }
                UserInfo.teacher->{
                }
                UserInfo.company->{}
            }
        }
        mLocationHelper.start()
    }

    override fun initToolbar(){
        super.initToolbar()

        when(helper.userInfo?.type){
            UserInfo.student->mViewBinding.tvTitle.text = "编辑个人资料"
            UserInfo.teacher->mViewBinding.tvTitle.text = "编辑个人资料"
            UserInfo.company->mViewBinding.tvTitle.text = "编辑机构资料"
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setHomeButtonEnabled(false)
    }

    override fun initData() {
        when(helper.userInfo?.type){
            UserInfo.student->presenter.getEditUserInfo()
            UserInfo.teacher->presenter.getTeacherInfo()
            UserInfo.company->presenter.getCompanyInfo()
        }
    }

    override fun initPresenter() {
        presenter.takeView(this)
        presenter.lifecycle(lifecycleSubject)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.dropView()
    }

}