package com.jinghan.app.mvp.presenter.impl

import com.jinghan.app.mvp.model.response.BaseResponse
import com.jinghan.app.mvp.presenter.IPayActivityPresenter
import com.jinghan.app.mvp.view.impl.api.OrderService
import com.jinghan.core.R
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DefaultObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author liuzeren
 * @time 2018/3/17 9:16
 * @mail lzr319@163.com
 */
class PayActivityPresenter @Inject constructor(): IPayActivityPresenter() {
    override fun pay(orderNo: String) {
        mView?.let { it.showLoading("正在付款~") }



        mOkHttp.retrofit.builder(OrderService::class.java).payOrder(orderNo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).compose(RxLifecycleAndroid.bindActivity(lifecycleSubject))
                .subscribe(object: DefaultObserver<BaseResponse>(){
                    override fun onError(e: Throwable) {
                        mView?.let{it.hideLoading()}
                        mView?.let {
                            it.toast(R.string.request_failure)
                        }
                    }

                    override fun onComplete() {
                        mView?.let{it.hideLoading()}
                    }

                    override fun onNext(response : BaseResponse) {
                        if(response.isSuccess){
                            mView?.let{it.toast("付款成功")}
                            mView?.let{it.close()}
                        }else{
                            mView?.let {
                                it.toast(response.msg)
                            }
                        }
                    }
                })
    }
}