package com.jinghan.core.helper;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by liuzeren on 2017/9/13.
 */

public class FileUtil {
    private static final String TAG = FileUtil.class.getSimpleName();

    static public String getFileExt(String fileName) {
        String ext = "";
        int idx = fileName.lastIndexOf(".");
        if (idx >= 0) {
            ext = fileName.substring(idx + 1);
        }
        return ext;
    }

    /*public static String rndFilePath(String ext) {
        if (!ext.equals("") && !ext.startsWith(".")) {
            ext = "." + ext;
        }
        String to0 = System.currentTimeMillis() + "";
        File rndFile = null;
        String to = to0 + ext;
        String rndPath = MagicGlobals.getAppDataPath(to);
        rndFile = new File(rndPath);
        int i = 0;
        while (rndFile.exists()) {
            to = to0 + "-" + i + ext;
            rndPath = MagicGlobals.getAppDataPath(to);
            rndFile = new File(rndPath);
            i++;
        }
        return to;
    }

    static public String moveToRndFile(String from) {
        String ext = getFileExt(from);
        String to = rndFilePath(ext);

        String realToPath = MagicGlobals.getAppDataPath(to);
        File toFile = new File(realToPath);

        File fromFile = new File(from);
        if (fromFile.renameTo(toFile)) {
            return to;
        }

        return null;
    }

    static public String fixVideoThumbnail(String videoFilePath, String unfixedThumbnailPath) {
        File imgFile = new File(unfixedThumbnailPath);
        if (imgFile.exists() && imgFile.isFile() && imgFile.length() >= 1024) {
            return FileUtil.moveToRndFile(unfixedThumbnailPath);
        }

        BufferedOutputStream bos = null;
        Log.w(TAG, "to fix the video thumbnail file, videoFilePath: " + videoFilePath);
        try {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(videoFilePath);
            Bitmap bmp = mmr.getFrameAtTime(1000L * 1000L,
                    MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            if (bmp == null) {
                Log.e(TAG, "failed in mmr.getFrameAtTime()");
                return null;
            }
            String fixedPath = FileUtil.rndFilePath("jpg");
            File fixedFile = new File(MagicGlobals.getAppDataPath(fixedPath));
            bos = new BufferedOutputStream(new FileOutputStream(fixedFile));
            bmp.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bos.flush();
            if (imgFile.exists() && imgFile.isFile()) {
                imgFile.delete();
            }
            Log.i(TAG, "fix the video thumbnail file ok, file size:" + fixedFile.length());
            return fixedPath;
        } catch (Exception exp) {
            Log.e(TAG, "fix the video thumbnail file failed, e:" + exp.toString());
            exp.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.e(TAG, "fix the video thumbnail file failed");
        return null;
    }

    static public Bitmap getVideoThumbnail(String videoFileName, int[] duration) {
        String fullPath = MagicGlobals.getAppDataPath(videoFileName);
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(fullPath);
            bitmap = retriever
                    .getFrameAtTime(MagicConstants.VideoThumnailPos * 1000000L);
            if (duration != null) {
                String strDuration = retriever
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                if (strDuration != null) {
                    try {
                        long d = Long.parseLong(strDuration);
                        duration[0] = (int) (d / 1000L);
                    } catch (Exception exp) {
                    }
                }
            }
        } catch (IllegalArgumentException ex) {
            // Assume this is a corrupt video file
        } catch (RuntimeException ex) {
            // Assume this is a corrupt video file.
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
                // Ignore failures while cleaning up.
            }
        }

        return bitmap;
    }*/

    public static void safeClose(Closeable c){
        if(null != c){
            try{
                c.close();
            }catch (IOException e){}finally {
                c = null;
            }
        }
    }
}
