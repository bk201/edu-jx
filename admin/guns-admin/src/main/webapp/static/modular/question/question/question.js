/**
 * 问题管理管理初始化
 */
var Question = {
    id: "QuestionTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Question.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '问题单号', field: 'questionNo', visible: true, align: 'center', valign: 'middle'},
            {title: '科目,', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '用户ID', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '发布问题的用户手机号', field: 'userMobile', visible: true, align: 'center', valign: 'middle'},
            {title: '发布问题用户名', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '问题图片列表', field: 'pictures', visible: true, align: 'center', valign: 'middle'},
            {title: '问题钱数', field: 'money', visible: true, align: 'center', valign: 'middle'},
            {title: '问题描述', field: 'description', visible: true, align: 'center', valign: 'middle'},
            {title: '状态,0:绿灯,待解答 1:黄灯,有人在解答 2:红灯 ,已经解答', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '支付状态,0:未支付,1:已支付', field: 'payStatus', visible: true, align: 'center', valign: 'middle'},
            {title: '国家', field: 'country', visible: true, align: 'center', valign: 'middle'},
            {title: '省', field: 'state', visible: true, align: 'center', valign: 'middle'},
            {title: '城市', field: 'city', visible: true, align: 'center', valign: 'middle'},
            {title: '城市编码', field: 'cityCode', visible: true, align: 'center', valign: 'middle'},
            {title: '区', field: 'region', visible: true, align: 'center', valign: 'middle'},
            {title: '区', field: 'regionCode', visible: true, align: 'center', valign: 'middle'},
            {title: '用户地址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '纬度', field: 'latitude', visible: true, align: 'center', valign: 'middle'},
            {title: '经度', field: 'longitude', visible: true, align: 'center', valign: 'middle'},
            {title: '添加时间,整数据10位形', field: 'addTimeLong', visible: true, align: 'center', valign: 'middle'},
            {title: '添加时间,时间型', field: 'addTime', visible: true, align: 'center', valign: 'middle'},
            {title: '解题用户', field: 'answerUserId', visible: true, align: 'center', valign: 'middle'},
            {title: '回答问题的用户名', field: 'answerUserName', visible: true, align: 'center', valign: 'middle'},
            {title: '回答问题人的手机号', field: 'answerMobile', visible: true, align: 'center', valign: 'middle'},
            {title: '0:正常状态; 1: 订单完成 ; 2:申请退款 3:退款完成 4:平台介入', field: 'orderStatus', visible: true, align: 'center', valign: 'middle'},
            {title: '回答问题的图片列表', field: 'answerPictures', visible: true, align: 'center', valign: 'middle'},
            {title: '解答文字', field: 'answerDescription', visible: true, align: 'center', valign: 'middle'},
            {title: '接题时间', field: 'answerAddTime', visible: true, align: 'center', valign: 'middle'},
            {title: '提交答案时间', field: 'answerUploadTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Question.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Question.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加问题管理
 */
Question.openAddQuestion = function () {
    var index = layer.open({
        type: 2,
        title: '添加问题管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/question/question_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看问题管理详情
 */
Question.openQuestionDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '问题管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/question/question_update/' + Question.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除问题管理
 */
Question.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/question/delete", function (data) {
            Feng.success("删除成功!");
            Question.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("questionId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询问题管理列表
 */
Question.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Question.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Question.initColumn();
    var table = new BSTable(Question.id, "/question/list", defaultColunms);
    table.setPaginationType("client");
    Question.table = table.init();
});
