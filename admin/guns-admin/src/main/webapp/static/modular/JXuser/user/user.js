/**
 * 用户管理管理初始化
 */
var User = {
    id: "UserTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
User.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '用户ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '密码', field: 'password', visible: true, align: 'center', valign: 'middle'},
            {title: '昵称', field: 'nickName', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'mobile', visible: true, align: 'center', valign: 'middle'},
            {title: '账户余额', field: 'money', visible: true, align: 'center', valign: 'middle'},
            {title: '性别,0:保密,1:男,2:女', field: 'sex', visible: true, align: 'center', valign: 'middle'},
            {title: '生日', field: 'birthday', visible: true, align: 'center', valign: 'middle'},
            {title: '身份,0:学生,1:老师,2:机构', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '如果身份是老师或机构,是否经过审核的,0:未审核,1:已经审核', field: 'typeQualified', visible: true, align: 'center', valign: 'middle'},
            {title: '省', field: 'province', visible: true, align: 'center', valign: 'middle'},
            {title: '市', field: 'city', visible: true, align: 'center', valign: 'middle'},
            {title: '住址', field: 'address', visible: true, align: 'center', valign: 'middle'},
            {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
            {title: '是否绑定邮箱', field: 'emailBind', visible: true, align: 'center', valign: 'middle'},
            {title: '头像', field: 'avatar', visible: true, align: 'center', valign: 'middle'},
            {title: '注册时间', field: 'addTime', visible: true, align: 'center', valign: 'middle'},
            {title: '支付宝账号', field: 'alipay', visible: true, align: 'center', valign: 'middle'},
            {title: '微信号', field: 'wechat', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'idCard', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照号', field: 'businessLicense', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
User.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        User.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户管理
 */
User.openAddUser = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/user/user_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户管理详情
 */
User.openUserDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/user/user_update/' + User.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户管理
 */
User.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/user/delete", function (data) {
            Feng.success("删除成功!");
            User.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("userId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户管理列表
 */
User.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    User.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = User.initColumn();
    var table = new BSTable(User.id, "/user/list", defaultColunms);
    table.setPaginationType("client");
    User.table = table.init();
});
