/**
 * 升级管理管理初始化
 */
var Apply = {
    id: "ApplyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Apply.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '申请老师ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '类型,1:老师,2:机构', field: 'type', visible: true, align: 'center', valign: 'middle',
            formatter:function (value,row,index) {
                return value===1?'老师':'机构';
            }},
            {title: '申请用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'mobile', visible: true, align: 'center', valign: 'middle'},
            {title: '支付宝姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '支付宝', field: 'alipay', visible: true, align: 'center', valign: 'middle'},
            {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'idCardNo', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证正面照片', field: 'idCardFront', visible: true, align: 'center', valign: 'middle',
                formatter: function(value,row,index){
                return '<img  src="'+value+'" class="img-rounded" alt="'+value+'" onclick="showImg(event)">';
            }},
            {title: '身份证反面照片', field: 'idCardBack', visible: true, align: 'center', valign: 'middle',
                formatter: function(value,row,index){
                return '<img  src="'+value+'" class="img-rounded" alt="'+value+'" onclick="showImg(event)">';
            }},
            {title: '学生证或学位证号照片', field: 'studentIdCardNo', visible: true, align: 'center', valign: 'middle',
                },
            {title: '学生证或学位证正面', field: 'studentIdCardFront', visible: true, align: 'center', valign: 'middle',
                formatter: function(value,row,index){
                    return '<img  src="'+value+'" class="img-rounded" alt="'+value+'" onclick="showImg(event)">';
                }},
            {title: '学生证或学位证反面照片', field: 'studentIdCardBack', visible: true, align: 'center', valign: 'middle',
                formatter: function(value,row,index){
                    return '<img  src="'+value+'" class="img-rounded" alt="'+value+'" onclick="showImg(event)">';
                }},
            {title: '社会信用码', field: 'institutionBusinessLicenceNo', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照照片', field: 'institutionBusinessLicencePic', visible: true, align: 'center', valign: 'middle',
                formatter: function(value,row,index){
                    return '<img  src="'+value+'" class="img-rounded" alt="'+value+'" onclick="showImg(event)">';
                }},
            {title: '申请状态,0:申请中,1:通过,2:未通过', field: 'status', visible: true, align: 'center', valign: 'middle',
            formatter:function (value,row,index) {
                return value===0?'<span style="color: blue">申请中</span>':(value===1?'<span style="color: green">通过</span>':'<span style="color: red">未通过</span>');
            }},
            {title: '未通过时原因', field: 'reason', visible: true, align: 'center', valign: 'middle'},
            {title: '添加时间', field: 'addTime', visible: true, align: 'center', valign: 'middle'},
            {title: '更新时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'},
            {title:'审核',field:'check',visible:true,align:'center',valign:'middle',
            formatter: function(value,row,index){
                if (row.status===0){
                    return "<div style='white-space: nowrap'><button type='button' class='btn btn-primary lw-mr' data-toggle='modal' data-target='#agreeModal' data-apply-id="+row.id+">同意</button>" +
                        "<button type='button' class='btn btn-danger' data-toggle='modal' data-target='#refuseModal' data-apply-id="+row.id+">拒绝</button></div>";
                }else{
                    return "";
                }
            }}
    ];
};

/**
 * 检查是否选中
 */
Apply.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Apply.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加升级管理
 */
Apply.openAddApply = function () {
    var index = layer.open({
        type: 2,
        title: '添加升级管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/apply/apply_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看升级管理详情
 */
Apply.openApplyDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '升级管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/apply/apply_update/' + Apply.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除升级管理
 */
Apply.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/apply/delete", function (data) {
            Feng.success("删除成功!");
            Apply.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("applyId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询升级管理列表
 */
Apply.search = function () {
    var queryData = {};
    var obj={};
    obj['applyType'] = $("#applyType").val();
    obj['name']=$("#condition").val();
    queryData['condition'] = JSON.stringify(obj);
    Apply.table.refresh({query: queryData});
};

/**
 * 审核
 * */
Apply.audit = function (applyId,status,reason) {
    var ajax = new $ax(Feng.ctxPath + "/apply/approve", function (data) {
        //这里先这样吧，接口页面我暂时打不开。
        if(data.code===200){
            Feng.success("成功!");
            Apply.table.refresh();
        }else{
            Feng.error("失败!ERR:"+data.code);
        }
    }, function (data) {
        Feng.error("失败!" + data.responseJSON.message + "!");
    });
    ajax.set("applyId",applyId);
    ajax.set("status",status);
    ajax.set("reason",reason);
    ajax.start();
}


$(function () {
    var defaultColunms = Apply.initColumn();
    var table = new BSTable(Apply.id, "/apply/list", defaultColunms);
    table.setPaginationType("client");
    Apply.table = table.init();
});


