/**
 * 初始化用户管理详情对话框
 */
var UserLoginInfoDlg = {
    userLoginInfoData : {}
};

/**
 * 清除数据
 */
UserLoginInfoDlg.clearData = function() {
    this.userLoginInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserLoginInfoDlg.set = function(key, val) {
    this.userLoginInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserLoginInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UserLoginInfoDlg.close = function() {
    parent.layer.close(window.parent.UserLogin.layerIndex);
}

/**
 * 收集数据
 */
UserLoginInfoDlg.collectData = function() {
    this
    .set('token')
    .set('userId')
    .set('mobile')
    .set('loginIp')
    .set('loginTime')
    .set('lastLoginTime');
}

/**
 * 提交添加
 */
UserLoginInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/userLogin/add", function(data){
        Feng.success("添加成功!");
        window.parent.UserLogin.table.refresh();
        UserLoginInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.userLoginInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
UserLoginInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/userLogin/update", function(data){
        Feng.success("修改成功!");
        window.parent.UserLogin.table.refresh();
        UserLoginInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.userLoginInfoData);
    ajax.start();
}

$(function() {

});
