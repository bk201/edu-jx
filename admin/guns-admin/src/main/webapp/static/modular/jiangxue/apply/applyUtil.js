/**
 * applyUtil.js
 * 功能：提供一些基础方法
 *
 * */

/**
 * 查看图片缩略图
 * */
function showImg(e) {
    var MAX_HEIGHT = 500;
    var src = $(e.target).attr("src");
    var $img =$("#imgCheck").find("img");
    $img.attr("src",src);

    //等比缩放
    var n_height = $img.get(0).naturalHeight;
    var n_width = $img.get(0).naturalWidth;
    var height=n_height>MAX_HEIGHT?MAX_HEIGHT:n_height + 'px';
    var width =n_width*height/n_height +'px';

    layer.open({
        type: 1,
        title: false,
        closeBtn: 0,
        area: [width, height], //宽高
        fix: false, //不固定
        skin: 'layui-layer-nobg',
        shadeClose: true,
        content: $('#imgCheck')
    });
}

/**
 *给同意审核按钮绑定方法
 *
 * */
$('#agreeModal').on('show.bs.modal', function (event) {
    var agreeButton = $(event.relatedTarget) // Button that triggered the modal
    var applyId = agreeButton.data('applyId') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    $("#agreeBtn").on("click",function () {
        Apply.audit(applyId,1,"恭喜通过审核");
    })
})

/**
 * 解绑同意审核按钮的方法
 * */
$('#agreeModal').on('hidden.bs.modal', function (event) {
    $("#agreeBtn").off("click");
})


/**
 *给拒绝审核按钮绑定方法
 *
 * */
$('#refuseModal').on('show.bs.modal', function (event) {
    var refuseButton = $(event.relatedTarget) // Button that triggered the modal
    var applyId = refuseButton.data('applyId') // Extract info from data-* attributes
    var $reasonArea =$(this).find("#reasonArea");
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    $("#refuseBtn").on("click",function () {
        var reason = $reasonArea.val();
        Apply.audit(applyId,2,reason);
    })
})

/**
 * 解绑拒绝审核按钮的方法
 * */
$('#refuseModal').on('hidden.bs.modal', function (event) {
    $("#refuseBtn").off("click");
})