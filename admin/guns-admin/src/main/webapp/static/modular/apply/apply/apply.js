/**
 * 用户升级申请管理初始化
 */
var Apply = {
    id: "ApplyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Apply.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '申请老师ID', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '类型,0:老师,1:机构', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '申请用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'mobile', visible: true, align: 'center', valign: 'middle'},
            {title: '支付宝姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '支付宝', field: 'alipay', visible: true, align: 'center', valign: 'middle'},
            {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证号', field: 'idCardNo', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证正面照片', field: 'idCardFront', visible: true, align: 'center', valign: 'middle'},
            {title: '身份证反面照片', field: 'idCardBack', visible: true, align: 'center', valign: 'middle'},
            {title: '学生证或学位证号照片', field: 'studentIdCardNo', visible: true, align: 'center', valign: 'middle'},
            {title: '学生证或学位证正面', field: 'studentIdCardFront', visible: true, align: 'center', valign: 'middle'},
            {title: '学生证或学位证反面照片', field: 'studentIdCardBack', visible: true, align: 'center', valign: 'middle'},
            {title: '社会信用码', field: 'institutionBusinessLicenceNo', visible: true, align: 'center', valign: 'middle'},
            {title: '营业执照照片', field: 'institutionBusinessLicencePic', visible: true, align: 'center', valign: 'middle'},
            {title: '申请状态,0:申请中,1:通过,2:未通过', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '未通过时原因', field: 'reason', visible: true, align: 'center', valign: 'middle'},
            {title: '添加时间', field: 'addTime', visible: true, align: 'center', valign: 'middle'},
            {title: '更新时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Apply.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Apply.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户升级申请
 */
Apply.openAddApply = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户升级申请',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/apply/apply_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户升级申请详情
 */
Apply.openApplyDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户升级申请详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/apply/apply_update/' + Apply.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户升级申请
 */
Apply.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/apply/delete", function (data) {
            Feng.success("删除成功!");
            Apply.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("applyId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户升级申请列表
 */
Apply.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Apply.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Apply.initColumn();
    var table = new BSTable(Apply.id, "/apply/list", defaultColunms);
    table.setPaginationType("client");
    Apply.table = table.init();
});
