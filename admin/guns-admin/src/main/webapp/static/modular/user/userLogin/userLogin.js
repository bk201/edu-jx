/**
 * 用户管理管理初始化
 */
var UserLogin = {
    id: "UserLoginTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
UserLogin.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '登陆表', field: 'token', visible: true, align: 'center', valign: 'middle'},
            {title: '用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'mobile', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'loginIp', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'loginTime', visible: true, align: 'center', valign: 'middle'},
            {title: '最近一次使用token时间', field: 'lastLoginTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
UserLogin.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        UserLogin.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加用户管理
 */
UserLogin.openAddUserLogin = function () {
    var index = layer.open({
        type: 2,
        title: '添加用户管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/userLogin/userLogin_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看用户管理详情
 */
UserLogin.openUserLoginDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '用户管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/userLogin/userLogin_update/' + UserLogin.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户管理
 */
UserLogin.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/userLogin/delete", function (data) {
            Feng.success("删除成功!");
            UserLogin.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("userLoginId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询用户管理列表
 */
UserLogin.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    UserLogin.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = UserLogin.initColumn();
    var table = new BSTable(UserLogin.id, "/userLogin/list", defaultColunms);
    table.setPaginationType("client");
    UserLogin.table = table.init();
});
