package com.rbl.admin.modular.jiangxue.service;

import com.rbl.admin.common.persistence.model.Apply;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 申请表 服务类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface IApplyService extends IService<Apply> {

  public List<Apply> findList(String condition);

  /**
   * 处理审核
   * @param applyId
   * @param status
   * @param reason
   */
  public void approve(Integer applyId,Integer status,String reason);

}
