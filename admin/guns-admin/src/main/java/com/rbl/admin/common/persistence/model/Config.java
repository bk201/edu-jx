package com.rbl.admin.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 配置内容
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@TableName("jx_config")
public class Config extends Model<Config> {

    private static final long serialVersionUID = 1L;

    /**
     * 配置名称
     */
    private String name;
    /**
     * 配置内容
     */
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    protected Serializable pkVal() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Config{" +
        "name=" + name +
        ", value=" + value +
        "}";
    }
}
