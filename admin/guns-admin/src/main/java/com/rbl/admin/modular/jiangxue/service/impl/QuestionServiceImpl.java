package com.rbl.admin.modular.jiangxue.service.impl;

import com.rbl.admin.common.persistence.model.Question;
import com.rbl.admin.common.persistence.dao.QuestionMapper;
import com.rbl.admin.modular.jiangxue.service.IQuestionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 问题列表 服务实现类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IQuestionService {

}
