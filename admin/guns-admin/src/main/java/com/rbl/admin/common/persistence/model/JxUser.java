package com.rbl.admin.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;

/**
 * <p>
 * 用户注册表
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@TableName("jx_user")
public class JxUser extends Model<JxUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 账户余额
     */
    private BigDecimal money;
    /**
     * 性别,0:保密,1:男,2:女
     */
    private Integer sex;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 身份,0:学生,1:老师,2:机构
     */
    private Integer type;
    /**
     * 如果身份是老师或机构,是否经过审核的,0:未审核,1:已经审核
     */
    @TableField("type_qualified")
    private Integer typeQualified;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 住址
     */
    private String address;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 是否绑定邮箱
     */
    @TableField("email_bind")
    private Integer emailBind;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 注册时间
     */
    @TableField("add_time")
    private Date addTime;
    /**
     * 支付宝账号
     */
    private String alipay;
    /**
     * 微信号
     */
    private String wechat;
    /**
     * 身份证号
     */
    @TableField("id_card")
    private String idCard;
    /**
     * 营业执照号
     */
    @TableField("business_license")
    private String businessLicense;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTypeQualified() {
        return typeQualified;
    }

    public void setTypeQualified(Integer typeQualified) {
        this.typeQualified = typeQualified;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEmailBind() {
        return emailBind;
    }

    public void setEmailBind(Integer emailBind) {
        this.emailBind = emailBind;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBusinessLicense() {
        return businessLicense;
    }

    public void setBusinessLicense(String businessLicense) {
        this.businessLicense = businessLicense;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "User{" +
        "id=" + id +
        ", name=" + name +
        ", password=" + password +
        ", nickName=" + nickName +
        ", mobile=" + mobile +
        ", money=" + money +
        ", sex=" + sex +
        ", birthday=" + birthday +
        ", type=" + type +
        ", typeQualified=" + typeQualified +
        ", province=" + province +
        ", city=" + city +
        ", address=" + address +
        ", email=" + email +
        ", emailBind=" + emailBind +
        ", avatar=" + avatar +
        ", addTime=" + addTime +
        ", alipay=" + alipay +
        ", wechat=" + wechat +
        ", idCard=" + idCard +
        ", businessLicense=" + businessLicense +
        "}";
    }
}
