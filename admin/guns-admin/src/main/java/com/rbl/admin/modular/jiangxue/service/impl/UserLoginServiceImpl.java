package com.rbl.admin.modular.jiangxue.service.impl;

import com.rbl.admin.common.persistence.model.UserLogin;
import com.rbl.admin.common.persistence.dao.UserLoginMapper;
import com.rbl.admin.modular.jiangxue.service.IUserLoginService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户登陆表 服务实现类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@Service
public class UserLoginServiceImpl extends ServiceImpl<UserLoginMapper, UserLogin> implements IUserLoginService {

}
