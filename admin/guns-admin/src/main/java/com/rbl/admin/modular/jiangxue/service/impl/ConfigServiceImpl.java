package com.rbl.admin.modular.jiangxue.service.impl;

import com.rbl.admin.common.persistence.model.Config;
import com.rbl.admin.common.persistence.dao.ConfigMapper;
import com.rbl.admin.modular.jiangxue.service.IConfigService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配置内容 服务实现类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements IConfigService {

}
