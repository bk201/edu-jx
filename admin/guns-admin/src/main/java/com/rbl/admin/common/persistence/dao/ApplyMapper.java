package com.rbl.admin.common.persistence.dao;

import com.rbl.admin.common.persistence.model.Apply;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 申请表 Mapper 接口
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface ApplyMapper extends BaseMapper<Apply> {

}
