package com.rbl.admin.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 问题列表
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@TableName("jx_question")
public class Question extends Model<Question> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 问题单号
     */
    @TableField("question_no")
    private String questionNo;
    /**
     * 科目,
     */
    private String type;
    /**
     * 用户ID
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 发布问题的用户手机号
     */
    @TableField("user_mobile")
    private String userMobile;
    /**
     * 发布问题用户名
     */
    @TableField("user_name")
    private String userName;
    /**
     * 问题图片列表
     */
    private String pictures;
    /**
     * 问题钱数
     */
    private Double money;
    /**
     * 问题描述
     */
    private String description;
    /**
     * 状态,0:绿灯,待解答 1:黄灯,有人在解答 2:红灯 ,已经解答
     */
    private Integer status;
    /**
     * 支付状态,0:未支付,1:已支付
     */
    @TableField("pay_status")
    private Integer payStatus;
    /**
     * 国家
     */
    private String country;
    /**
     * 省
     */
    private String state;
    /**
     * 城市
     */
    private String city;
    /**
     * 城市编码
     */
    @TableField("city_code")
    private String cityCode;
    /**
     * 区
     */
    private String region;
    /**
     * 区
     */
    @TableField("region_code")
    private String regionCode;
    /**
     * 用户地址
     */
    private String address;
    /**
     * 纬度
     */
    private Double latitude;
    /**
     * 经度
     */
    private Double longitude;
    /**
     * 添加时间,整数据10位形
     */
    @TableField("add_time_long")
    private Integer addTimeLong;
    /**
     * 添加时间,时间型
     */
    @TableField("add_time")
    private Date addTime;
    /**
     * 解题用户
     */
    @TableField("answer_user_id")
    private Integer answerUserId;
    /**
     * 回答问题的用户名
     */
    @TableField("answer_user_name")
    private String answerUserName;
    /**
     * 回答问题人的手机号
     */
    @TableField("answer_mobile")
    private String answerMobile;
    /**
     * 0:正常状态; 1: 订单完成 ; 2:申请退款 3:退款完成 4:平台介入
     */
    @TableField("order_status")
    private Integer orderStatus;
    /**
     * 回答问题的图片列表
     */
    @TableField("answer_pictures")
    private String answerPictures;
    /**
     * 解答文字
     */
    @TableField("answer_description")
    private String answerDescription;
    /**
     * 接题时间
     */
    @TableField("answer_add_time")
    private Date answerAddTime;
    /**
     * 提交答案时间
     */
    @TableField("answer_upload_time")
    private Date answerUploadTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getAddTimeLong() {
        return addTimeLong;
    }

    public void setAddTimeLong(Integer addTimeLong) {
        this.addTimeLong = addTimeLong;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getAnswerUserId() {
        return answerUserId;
    }

    public void setAnswerUserId(Integer answerUserId) {
        this.answerUserId = answerUserId;
    }

    public String getAnswerUserName() {
        return answerUserName;
    }

    public void setAnswerUserName(String answerUserName) {
        this.answerUserName = answerUserName;
    }

    public String getAnswerMobile() {
        return answerMobile;
    }

    public void setAnswerMobile(String answerMobile) {
        this.answerMobile = answerMobile;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getAnswerPictures() {
        return answerPictures;
    }

    public void setAnswerPictures(String answerPictures) {
        this.answerPictures = answerPictures;
    }

    public String getAnswerDescription() {
        return answerDescription;
    }

    public void setAnswerDescription(String answerDescription) {
        this.answerDescription = answerDescription;
    }

    public Date getAnswerAddTime() {
        return answerAddTime;
    }

    public void setAnswerAddTime(Date answerAddTime) {
        this.answerAddTime = answerAddTime;
    }

    public Date getAnswerUploadTime() {
        return answerUploadTime;
    }

    public void setAnswerUploadTime(Date answerUploadTime) {
        this.answerUploadTime = answerUploadTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Question{" +
        "id=" + id +
        ", questionNo=" + questionNo +
        ", type=" + type +
        ", userId=" + userId +
        ", userMobile=" + userMobile +
        ", userName=" + userName +
        ", pictures=" + pictures +
        ", money=" + money +
        ", description=" + description +
        ", status=" + status +
        ", payStatus=" + payStatus +
        ", country=" + country +
        ", state=" + state +
        ", city=" + city +
        ", cityCode=" + cityCode +
        ", region=" + region +
        ", regionCode=" + regionCode +
        ", address=" + address +
        ", latitude=" + latitude +
        ", longitude=" + longitude +
        ", addTimeLong=" + addTimeLong +
        ", addTime=" + addTime +
        ", answerUserId=" + answerUserId +
        ", answerUserName=" + answerUserName +
        ", answerMobile=" + answerMobile +
        ", orderStatus=" + orderStatus +
        ", answerPictures=" + answerPictures +
        ", answerDescription=" + answerDescription +
        ", answerAddTime=" + answerAddTime +
        ", answerUploadTime=" + answerUploadTime +
        "}";
    }
}
