package com.rbl.admin.modular.jiangxue.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.rbl.admin.common.persistence.model.Question;
import com.rbl.admin.modular.jiangxue.service.IQuestionService;

/**
 * 问题管理控制器
 *
 * @author fengshuonan
 * @Date 2018-02-10 10:08:02
 */
@Controller
@RequestMapping("/question")
public class QuestionController extends BaseController {

    private String PREFIX = "/jiangxue/question/";

    @Autowired
    private IQuestionService questionService;

    /**
     * 跳转到问题管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "question.html";
    }

    /**
     * 跳转到添加问题管理
     */
    @RequestMapping("/question_add")
    public String questionAdd() {
        return PREFIX + "question_add.html";
    }

    /**
     * 跳转到修改问题管理
     */
    @RequestMapping("/question_update/{questionId}")
    public String questionUpdate(@PathVariable Integer questionId, Model model) {
        Question question = questionService.selectById(questionId);
        model.addAttribute("item",question);
        LogObjectHolder.me().set(question);
        return PREFIX + "question_edit.html";
    }

    /**
     * 获取问题管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return questionService.selectList(null);
    }

    /**
     * 新增问题管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Question question) {
        questionService.insert(question);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除问题管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer questionId) {
        questionService.deleteById(questionId);
        return SUCCESS_TIP;
    }

    /**
     * 修改问题管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Question question) {
        questionService.updateById(question);
        return super.SUCCESS_TIP;
    }

    /**
     * 问题管理详情
     */
    @RequestMapping(value = "/detail/{questionId}")
    @ResponseBody
    public Object detail(@PathVariable("questionId") Integer questionId) {
        return questionService.selectById(questionId);
    }
}
