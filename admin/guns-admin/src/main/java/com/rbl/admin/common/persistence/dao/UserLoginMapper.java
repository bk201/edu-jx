package com.rbl.admin.common.persistence.dao;

import com.rbl.admin.common.persistence.model.UserLogin;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户登陆表 Mapper 接口
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface UserLoginMapper extends BaseMapper<UserLogin> {

}
