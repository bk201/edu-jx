package com.rbl.admin.modular.jiangxue.service;

import com.rbl.admin.common.persistence.model.Config;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 配置内容 服务类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface IConfigService extends IService<Config> {

}
