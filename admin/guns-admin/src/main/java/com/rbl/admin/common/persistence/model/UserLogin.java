package com.rbl.admin.common.persistence.model;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户登陆表
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@TableName("jx_user_login")
public class UserLogin extends Model<UserLogin> {

    private static final long serialVersionUID = 1L;

    /**
     * 登陆表
     */
    private String token;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 手机号
     */
    private String mobile;
    @TableField("login_ip")
    private String loginIp;
    @TableField("login_time")
    private Date loginTime;
    /**
     * 最近一次使用token时间
     */
    @TableField("last_login_time")
    private Date lastLoginTime;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.token;
    }

    @Override
    public String toString() {
        return "UserLogin{" +
        "token=" + token +
        ", userId=" + userId +
        ", mobile=" + mobile +
        ", loginIp=" + loginIp +
        ", loginTime=" + loginTime +
        ", lastLoginTime=" + lastLoginTime +
        "}";
    }
}
