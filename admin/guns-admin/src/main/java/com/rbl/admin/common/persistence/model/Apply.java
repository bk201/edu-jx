package com.rbl.admin.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 申请表
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@TableName("jx_apply")
public class Apply extends Model<Apply> {

    private static final long serialVersionUID = 1L;

    /**
     * 申请老师ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 类型,0:老师,1:机构
     */
    private Integer type;
    /**
     * 申请用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 支付宝姓名
     */
    private String name;
    /**
     * 支付宝
     */
    private String alipay;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 身份证号
     */
    @TableField("id_card_no")
    private String idCardNo;
    /**
     * 身份证正面照片
     */
    @TableField("id_card_front")
    private String idCardFront;
    /**
     * 身份证反面照片
     */
    @TableField("id_card_back")
    private String idCardBack;
    /**
     * 学生证或学位证号照片
     */
    @TableField("student_id_card_no")
    private String studentIdCardNo;
    /**
     * 学生证或学位证正面
     */
    @TableField("student_id_card_front")
    private String studentIdCardFront;
    /**
     * 学生证或学位证反面照片
     */
    @TableField("student_id_card_back")
    private String studentIdCardBack;
    /**
     * 社会信用码
     */
    @TableField("institution_business_licence_no")
    private String institutionBusinessLicenceNo;
    /**
     * 营业执照照片
     */
    @TableField("institution_business_licence_pic")
    private String institutionBusinessLicencePic;
    /**
     * 申请状态,0:申请中,1:通过,2:未通过
     */
    private Integer status;
    /**
     * 未通过时原因
     */
    private String reason;
    /**
     * 添加时间
     */
    @TableField("add_time")
    private Date addTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getIdCardFront() {
        return idCardFront;
    }

    public void setIdCardFront(String idCardFront) {
        this.idCardFront = idCardFront;
    }

    public String getIdCardBack() {
        return idCardBack;
    }

    public void setIdCardBack(String idCardBack) {
        this.idCardBack = idCardBack;
    }

    public String getStudentIdCardNo() {
        return studentIdCardNo;
    }

    public void setStudentIdCardNo(String studentIdCardNo) {
        this.studentIdCardNo = studentIdCardNo;
    }

    public String getStudentIdCardFront() {
        return studentIdCardFront;
    }

    public void setStudentIdCardFront(String studentIdCardFront) {
        this.studentIdCardFront = studentIdCardFront;
    }

    public String getStudentIdCardBack() {
        return studentIdCardBack;
    }

    public void setStudentIdCardBack(String studentIdCardBack) {
        this.studentIdCardBack = studentIdCardBack;
    }

    public String getInstitutionBusinessLicenceNo() {
        return institutionBusinessLicenceNo;
    }

    public void setInstitutionBusinessLicenceNo(String institutionBusinessLicenceNo) {
        this.institutionBusinessLicenceNo = institutionBusinessLicenceNo;
    }

    public String getInstitutionBusinessLicencePic() {
        return institutionBusinessLicencePic;
    }

    public void setInstitutionBusinessLicencePic(String institutionBusinessLicencePic) {
        this.institutionBusinessLicencePic = institutionBusinessLicencePic;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Apply{" +
        "id=" + id +
        ", type=" + type +
        ", userId=" + userId +
        ", mobile=" + mobile +
        ", name=" + name +
        ", alipay=" + alipay +
        ", email=" + email +
        ", idCardNo=" + idCardNo +
        ", idCardFront=" + idCardFront +
        ", idCardBack=" + idCardBack +
        ", studentIdCardNo=" + studentIdCardNo +
        ", studentIdCardFront=" + studentIdCardFront +
        ", studentIdCardBack=" + studentIdCardBack +
        ", institutionBusinessLicenceNo=" + institutionBusinessLicenceNo +
        ", institutionBusinessLicencePic=" + institutionBusinessLicencePic +
        ", status=" + status +
        ", reason=" + reason +
        ", addTime=" + addTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
