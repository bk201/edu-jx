package com.rbl.admin.modular.jiangxue.service.impl;

import com.rbl.admin.common.persistence.model.JxUser;
import com.rbl.admin.common.persistence.dao.JxUserMapper;
import com.rbl.admin.modular.jiangxue.service.IJxUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户注册表 服务实现类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@Service
public class JxUserServiceImpl extends ServiceImpl<JxUserMapper, JxUser> implements IJxUserService
{

}
