package com.rbl.admin.modular.jiangxue.service;

import com.rbl.admin.common.persistence.model.Question;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 问题列表 服务类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface IQuestionService extends IService<Question> {

}
