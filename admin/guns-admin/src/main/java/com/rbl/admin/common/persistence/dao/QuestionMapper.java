package com.rbl.admin.common.persistence.dao;

import com.rbl.admin.common.persistence.model.Question;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 问题列表 Mapper 接口
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface QuestionMapper extends BaseMapper<Question> {

}
