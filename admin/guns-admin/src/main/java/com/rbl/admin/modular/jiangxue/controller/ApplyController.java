package com.rbl.admin.modular.jiangxue.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.SuccessTip;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.rbl.admin.common.persistence.model.Apply;
import com.rbl.admin.modular.jiangxue.service.IApplyService;

import java.util.List;

/**
 * 升级管理控制器
 *
 * @author fengshuonan
 * @Date 2018-02-10 10:08:54
 */
@Api(tags = { "用户角色升级" })
@Controller
@RequestMapping("/apply")
public class ApplyController extends BaseController {

    private String PREFIX = "/jiangxue/apply/";

    @Autowired
    private IApplyService applyService;

    @Value("${jiangxu.card-url}")
    private String cardUrl;

    /**
     * 跳转到升级管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "apply.html";
    }

    /**
     * 跳转到添加升级管理
     */
    @RequestMapping("/apply_add")
    public String applyAdd() {
        return PREFIX + "apply_add.html";
    }

    /**
     * 跳转到修改升级管理
     */
    @RequestMapping("/apply_update/{applyId}")
    public String applyUpdate(@PathVariable Integer applyId, Model model) {
        Apply apply = applyService.selectById(applyId);
        model.addAttribute("item",apply);
        LogObjectHolder.me().set(apply);
        return PREFIX + "apply_edit.html";
    }

    /**
     * 获取升级管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        System.out.println(condition);
        List<Apply> list = applyService.findList(condition);
        return list;
    }

    /**
     * 新增升级管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Apply apply) {
        applyService.insert(apply);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除升级管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer applyId) {
        applyService.deleteById(applyId);
        return SUCCESS_TIP;
    }

    /**
     * 修改升级管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Apply apply) {
        applyService.updateById(apply);
        return super.SUCCESS_TIP;
    }

    /**
     * 升级管理详情
     */
    @RequestMapping(value = "/detail/{applyId}")
    @ResponseBody
    public Object detail(@PathVariable("applyId") Integer applyId) {
        return applyService.selectById(applyId);
    }

    //通过审核
    @RequestMapping(value = "/approve",method = RequestMethod.POST,produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    @ApiOperation(value = "审核用户升级申请",notes = "审核用户升级是否通过,status字段说明: 1:通过,2:未通过<br>"
        + "如果未通过,可以提交原因说明")
    public SuccessTip doApply(@ApiParam(value = "申请ID",  required = true) @RequestParam(value = "applyId") Integer applyId,
        @ApiParam(value = "状态,1:通过,2:未通过",  required = true,defaultValue = "1",allowableValues = "1,2") @RequestParam(value = "status",required = true) Integer status,
        @ApiParam(value = "原因",  required = true,defaultValue = "恭喜通过") @RequestParam(value = "reason",required = true) String reason)
    {
        applyService.approve(applyId,status,reason);
        return super.SUCCESS_TIP;
    }
}
