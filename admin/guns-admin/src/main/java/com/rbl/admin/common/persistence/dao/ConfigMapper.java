package com.rbl.admin.common.persistence.dao;

import com.rbl.admin.common.persistence.model.Config;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 配置内容 Mapper 接口
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
