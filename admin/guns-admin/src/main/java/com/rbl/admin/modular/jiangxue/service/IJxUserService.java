package com.rbl.admin.modular.jiangxue.service;

import com.rbl.admin.common.persistence.model.JxUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户注册表 服务类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
public interface IJxUserService extends IService<JxUser> {

}
