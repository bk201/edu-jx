package com.rbl.admin.modular.jiangxue.service.impl;

import com.rbl.admin.common.persistence.model.Apply;
import com.rbl.admin.common.persistence.dao.ApplyMapper;
import com.rbl.admin.common.persistence.model.JxUser;
import com.rbl.admin.modular.jiangxue.service.IApplyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.rbl.admin.modular.jiangxue.service.IJxUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 申请表 服务实现类
 * </p>
 *
 * @author tkong123
 * @since 2018-02-10
 */
@Service
public class ApplyServiceImpl extends ServiceImpl<ApplyMapper, Apply> implements IApplyService
{

  @Autowired IJxUserService jxUserService;

  //TODO 需要添加条件过滤
  @Override public List<Apply> findList(String condition)
  {
    return this.selectList(null);
  }

  @Override public void approve(Integer applyId, Integer status, String reason)
  {
    //获取数据
    Apply apply = this.selectById(applyId);
    if(apply==null)
      return;

    JxUser user = jxUserService.selectById(apply.getUserId());

    //如果通过
    if(status == 1)
    {
      //更新用户数据
      approveUser(apply,user);
    }
    else if(status == 2)
    {
      degradeUser(user);
    }
    apply.setStatus(status);
    apply.setReason(reason);
    apply.setUpdateTime(new Date());

    this.updateById(apply);
  }

  /**
   * 更新用户数据
   * @param apply
   * @param user
   */
  private void approveUser(Apply apply,JxUser user)
  {
    //修改用户身份
    user.setType(apply.getType());
    user.setTypeQualified(1);

    //修改用户支付数据,邮箱数据
    user.setName(apply.getName());
    user.setAlipay(apply.getAlipay());
    user.setEmail(apply.getEmail());

    jxUserService.updateById(user);
  }

  /**
   * 降级用户
   * @param user
   */
  private void degradeUser(JxUser user)
  {
    //修改用户身份
    user.setType(0);
    user.setTypeQualified(0);
    jxUserService.updateById(user);
  }
}
