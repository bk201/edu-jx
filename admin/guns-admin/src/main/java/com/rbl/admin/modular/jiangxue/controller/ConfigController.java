package com.rbl.admin.modular.jiangxue.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.rbl.admin.common.persistence.model.Config;
import com.rbl.admin.modular.jiangxue.service.IConfigService;

/**
 * 配置管理控制器
 *
 * @author fengshuonan
 * @Date 2018-02-10 10:24:47
 */
@Controller
@RequestMapping("/config")
public class ConfigController extends BaseController {

    private String PREFIX = "/jiangxue/config/";

    @Autowired
    private IConfigService configService;

    /**
     * 跳转到配置管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "config.html";
    }

    /**
     * 跳转到添加配置管理
     */
    @RequestMapping("/config_add")
    public String configAdd() {
        return PREFIX + "config_add.html";
    }

    /**
     * 跳转到修改配置管理
     */
    @RequestMapping("/config_update/{configId}")
    public String configUpdate(@PathVariable Integer configId, Model model) {
        Config config = configService.selectById(configId);
        model.addAttribute("item",config);
        LogObjectHolder.me().set(config);
        return PREFIX + "config_edit.html";
    }

    /**
     * 获取配置管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return configService.selectList(null);
    }

    /**
     * 新增配置管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Config config) {
        configService.insert(config);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除配置管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer configId) {
        configService.deleteById(configId);
        return SUCCESS_TIP;
    }

    /**
     * 修改配置管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Config config) {
        configService.updateById(config);
        return super.SUCCESS_TIP;
    }

    /**
     * 配置管理详情
     */
    @RequestMapping(value = "/detail/{configId}")
    @ResponseBody
    public Object detail(@PathVariable("configId") Integer configId) {
        return configService.selectById(configId);
    }
}
