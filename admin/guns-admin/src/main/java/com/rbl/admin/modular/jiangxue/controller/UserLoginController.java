package com.rbl.admin.modular.jiangxue.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.rbl.admin.common.persistence.model.UserLogin;
import com.rbl.admin.modular.jiangxue.service.IUserLoginService;

/**
 * 用户管理控制器
 *
 * @author fengshuonan
 * @Date 2018-02-10 10:06:41
 */
@Controller
@RequestMapping("/userLogin")
public class UserLoginController extends BaseController {

    private String PREFIX = "/jiangxue/userLogin/";

    @Autowired
    private IUserLoginService userLoginService;

    /**
     * 跳转到用户管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "userLogin.html";
    }

    /**
     * 跳转到添加用户管理
     */
    @RequestMapping("/userLogin_add")
    public String userLoginAdd() {
        return PREFIX + "userLogin_add.html";
    }

    /**
     * 跳转到修改用户管理
     */
    @RequestMapping("/userLogin_update/{userLoginId}")
    public String userLoginUpdate(@PathVariable Integer userLoginId, Model model) {
        UserLogin userLogin = userLoginService.selectById(userLoginId);
        model.addAttribute("item",userLogin);
        LogObjectHolder.me().set(userLogin);
        return PREFIX + "userLogin_edit.html";
    }

    /**
     * 获取用户管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return userLoginService.selectList(null);
    }

    /**
     * 新增用户管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(UserLogin userLogin) {
        userLoginService.insert(userLogin);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除用户管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer userLoginId) {
        userLoginService.deleteById(userLoginId);
        return SUCCESS_TIP;
    }

    /**
     * 修改用户管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(UserLogin userLogin) {
        userLoginService.updateById(userLogin);
        return super.SUCCESS_TIP;
    }

    /**
     * 用户管理详情
     */
    @RequestMapping(value = "/detail/{userLoginId}")
    @ResponseBody
    public Object detail(@PathVariable("userLoginId") Integer userLoginId) {
        return userLoginService.selectById(userLoginId);
    }
}
